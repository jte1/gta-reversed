#include "StdInc.h"

CVector CZoneDef::FindCenter()
{
	return CVector((this->xMax + this->xMin) / 2.0f,
		(this->yMax + this->yMin) / 2.0f,
		(this->zMax + this->zMin) / 2.0f);
}

bool CZoneDef::IsPointWithin(CVector vecPoint)
{
	if (this->zMax >= vecPoint.z)
		return false;

	if (this->zMin <= vecPoint.z)
		return false;

	float dx = (vecPoint.x - this->x);
	float dy = (vecPoint.y - this->y);


	float minDistSq = this->xMin * dx + this->yMin * dy;

	if (minDistSq <= 0.0)
		return false;

	if (minDistSq >= this->yMin * this->yMin + this->xMin * this->xMin)
		return false;

	float maxDistSq = this->xMax * dx + this->yMax * dy;

	if (maxDistSq <= 0)
		return false;

	float maxSq = this->yMax * this->yMax + this->xMax * this->xMax;

	if (maxDistSq >= maxSq)
		return false;

	return true;
}