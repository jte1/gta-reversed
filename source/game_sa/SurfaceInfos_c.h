/**
 * nick7 @ 2014/11/21 22:20
 */

# pragma once

#include "CColPoint.h"

enum EFrictionEffect
{
	EFrictionEffectNone,
	EFrictionEffectSparks
};

enum EColSurfaceValue
{
	DEFAULT,
	TARMAC,
	TARMAC_FUCKED,
	TARMAC_REALLYFUCKED,
	PAVEMENT,
	PAVEMENT_FUCKED,
	GRAVEL,
	FUCKED_CONCRETE,
	PAINTED_GROUND,
	GRASS_SHORT_LUSH,
	GRASS_MEDIUM_LUSH,
	GRASS_LONG_LUSH,
	GRASS_SHORT_DRY,
	GRASS_MEDIUM_DRY,
	GRASS_LONG_DRY,
	GOLFGRASS_ROUGH,
	GOLFGRASS_SMOOTH,
	STEEP_SLIDYGRASS,
	STEEP_CLIFF,
	FLOWERBED,
	MEADOW,
	WASTEGROUND,
	WOODLANDGROUND,
	VEGETATION,
	MUD_WET,
	MUD_DRY,
	DIRT,
	DIRTTRACK,
	SAND_DEEP,
	SAND_MEDIUM,
	SAND_COMPACT,
	SAND_ARID,
	SAND_MORE,
	SAND_BEACH,
	CONCRETE_BEACH,
	ROCK_DRY,
	ROCK_WET,
	ROCK_CLIFF,
	WATER_RIVERBED,
	WATER_SHALLOW,
	CORNFIELD,
	HEDGE,
	WOOD_CRATES,
	WOOD_SOLID,
	WOOD_THIN,
	GLASS,
	GLASS_WINDOWS_LARGE,
	GLASS_WINDOWS_SMALL,
	EMPTY1,
	EMPTY2,
	GARAGE_DOOR,
	THICK_METAL_PLATE,
	SCAFFOLD_POLE,
	LAMP_POST,
	METAL_GATE,
	METAL_CHAIN_FENCE,
	GIRDER,
	FIRE_HYDRANT,
	CONTAINER,
	NEWS_VENDOR,
	WHEELBASE,
	CARDBOARDBOX,
	PED,
	CAR,
	CAR_PANEL,
	CAR_MOVINGCOMPONENT,
	TRANSPARENT_CLOTH,
	RUBBER,
	PLASTIC,
	TRANSPARENT_STONE,
	WOOD_BENCH,
	CARPET,
	FLOORBOARD,
	STAIRSWOOD,
	P_SAND,
	P_SAND_DENSE,
	P_SAND_ARID,
	P_SAND_COMPACT,
	P_SAND_ROCKY,
	P_SANDBEACH,
	P_GRASS_SHORT,
	P_GRASS_MEADOW,
	P_GRASS_DRY,
	P_WOODLAND,
	P_WOODDENSE,
	P_ROADSIDE,
	P_ROADSIDEDES,
	P_FLOWERBED,
	P_WASTEGROUND,
	P_CONCRETE,
	P_OFFICEDESK,
	P_711SHELF1,
	P_711SHELF2,
	P_711SHELF3,
	P_RESTUARANTTABLE,
	P_BARTABLE,
	P_UNDERWATERLUSH,
	P_UNDERWATERBARREN,
	P_UNDERWATERCORAL,
	P_UNDERWATERDEEP,
	P_RIVERBED,
	P_RUBBLE,
	P_BEDROOMFLOOR,
	P_KIRCHENFLOOR,
	P_LIVINGRMFLOOR,
	P_CORRIDORFLOOR,
	P_711FLOOR,
	P_FASTFOODFLOOR,
	P_SKANKYFLOOR,
	P_MOUNTAIN,
	P_MARSH,
	P_BUSHY,
	P_BUSHYMIX,
	P_BUSHYDRY,
	P_BUSHYMID,
	P_GRASSWEEFLOWERS,
	P_GRASSDRYTALL,
	P_GRASSLUSHTALL,
	P_GRASSGRNMIX,
	P_GRASSBRNMIX,
	P_GRASSLOW,
	P_GRASSROCKY,
	P_GRASSSMALLTREES,
	P_DIRTROCKY,
	P_DIRTWEEDS,
	P_GRASSWEEDS,
	P_RIVEREDGE,
	P_POOLSIDE,
	P_FORESTSTUMPS,
	P_FORESTSTICKS,
	P_FORRESTLEAVES,
	P_DESERTROCKS,
	P_FORRESTDRY,
	P_SPARSEFLOWERS,
	P_BUILDINGSITE,
	P_DOCKLANDS,
	P_INDUSTRIAL,
	P_INDUSTJETTY,
	P_CONCRETELITTER,
	P_ALLEYRUBISH,
	P_JUNKYARDPILES,
	P_JUNKYARDGRND,
	P_DUMP,
	P_CACTUSDENSE,
	P_AIRPORTGRND,
	P_CORNFIELD,
	P_GRASSLIGHT,
	P_GRASSLIGHTER,
	P_GRASSLIGHTER2,
	P_GRASSMID1,
	P_GRASSMID2,
	P_GRASSDARK,
	P_GRASSDARK2,
	P_GRASSDIRTMIX,
	P_RIVERBEDSTONE,
	P_RIVERBEDSHALLOW,
	P_RIVERBEDWEEDS,
	P_SEAWEED,
	DOOR,
	PLASTICBARRIER,
	PARKGRASS,
	STAIRSSTONE,
	STAIRSMETAL,
	STAIRSCARPET,
	FLOORMETAL,
	FLOORCONCRETE,
	BIN_BAG,
	THIN_METAL_SHEET,
	METAL_BARREL,
	PLASTIC_CONE,
	PLASTIC_DUMPSTER,
	METAL_DUMPSTER,
	WOOD_PICKET_FENCE,
	WOOD_SLATTED_FENCE,
	WOOD_RANCH_FENCE,
	UNBREAKABLE_GLASS,
	HAY_BALE,
	GORE,
	RAILTRACK,
};

struct SurfaceInfo
{
	unsigned char tyreGrip;
	unsigned char wetGrip;
	unsigned char __align[2];
	unsigned int flags1;
	unsigned int flags2;
};

class SurfaceInfos_c {
public:
	float adhesiveLimits[6 * 6];
	SurfaceInfo surfType[179];
public:
	SurfaceInfos_c(void);
	~SurfaceInfos_c(void);

	void Init(void);
	int GetSurfaceIdFromName(char const * pszName);
	bool CanClimb(unsigned int);
	bool CantSprintOn(unsigned int);
	bool CreatesObjects(unsigned int);
	bool CreatesPlants(unsigned int);
	bool CreatesSparks(unsigned int);
	bool CreatesWheelDust(unsigned int);
	bool CreatesWheelGrass(unsigned int);
	bool CreatesWheelGravel(unsigned int);
	bool CreatesWheelMud(unsigned int);
	bool CreatesWheelSand(unsigned int);
	bool CreatesWheelSpray(unsigned int);
	bool GetAdhesionGroup(unsigned int);
	bool GetAdhesiveLimit(CColPoint & colPoint);
	bool GetBulletFx(unsigned int);
	bool GetFlammability(unsigned int);
	bool GetFrictionEffect(unsigned int);
	bool GetRoughness(unsigned int);
	bool GetSkidmarkType(unsigned int);
	bool GetTyreGrip(unsigned int);
	bool GetWetMultiplier(unsigned int);
	bool IsAudioConcrete(unsigned int);
	bool IsAudioGrass(unsigned int);
	bool IsAudioGravel(unsigned int);
	bool IsAudioLongGrass(unsigned int);
	bool IsAudioMetal(unsigned int);
	bool IsAudioSand(unsigned int);
	bool IsAudioTile(unsigned int);
	bool IsAudioWater(unsigned int);
	bool IsAudioWood(unsigned int);
	bool IsBeach(unsigned int);
	bool IsGlass(unsigned int);
	bool IsPavement(unsigned int);
	bool IsSand(unsigned int);
	bool IsSeeThrough(unsigned int surfaceId);
	bool IsShallowWater(unsigned int);
	bool IsShootThrough(unsigned int surfaceId);
	bool IsSkateable(unsigned int);
	bool IsSoftLanding(unsigned int);
	bool IsStairs(unsigned int);
	bool IsSteepSlope(unsigned int);
	bool IsWater(unsigned int);
	bool LeavesFootsteps(unsigned int);
	bool MakesCarClean(unsigned int);
	bool MakesCarDirty(unsigned int);
	bool ProducesFootDust(unsigned int);
protected:
	/**
	 * Load surface.dat
	 */
	void LoadAdhesiveLimits(void);

	/**
	 * Load surfinfo.dat
	 */
	void LoadSurfaceInfos(void);

	/**
	 * Load surfaud.dat
	 */
	void LoadSurfaceAudioInfos(void);
};

extern SurfaceInfos_c &g_surfaceInfos;
