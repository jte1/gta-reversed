#pragma once

#include "common.h"

class CZoneDef
{
public:
	short x;
	short y;
	short xMin;
	short yMin;
	short xMax;
	short yMax;
	short zMax;
	short zMin;

	CVector FindBoundingBox(CVector*, CVector*);
	CVector FindCenter();
	bool IsPointWithin(CVector);
};

VALIDATE_SIZE(CZoneDef, 0x10);

class CCullZone : public CZoneDef
{
public:
	unsigned short flags;
};

VALIDATE_SIZE(CCullZone, 0x12);