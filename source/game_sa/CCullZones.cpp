#include "StdInc.h"

int &CCullZones::CurrentFlags_Camera = *reinterpret_cast<int*>(0xC87ABC);
int &CCullZones::CurrentFlags_Player = *reinterpret_cast<int*>(0xC87AB8);
int &CCullZones::NumAttributeZones = *reinterpret_cast<int*>(0xC87AC8);
int &CCullZones::NumMirrorAttributeZones = *reinterpret_cast<int*>(0xC87AC4);
int &CCullZones::NumTunnelAttributeZones = *reinterpret_cast<int*>(0xC87AC0);
bool &CCullZones::bMilitaryZonesDisabled = *reinterpret_cast<bool*>(0xC87ACD);
CCullZone * CCullZones::aAttributeZones = reinterpret_cast<CCullZone *>(0xC81F50);


void CCullZones::Init()
{
	CCullZones::NumAttributeZones = 0;
	CCullZones::CurrentFlags_Player = 0;
	CCullZones::CurrentFlags_Camera = 0;
}

bool CCullZones::CamCloseInForPlayer()
{
	return CCullZones::CurrentFlags_Player & CullFlag_CamCloseInForPlayer;
}

bool CCullZones::CamStairsForPlayer()
{
	return CCullZones::CurrentFlags_Player & CullFlag_CamStairsForPlayer;
}

bool CCullZones::Cam1stPersonForPlayer()
{
	return CCullZones::CurrentFlags_Player & CullFlag_Cam1stPersonForPlayer;
}

bool CCullZones::NoPolice()
{
	return CCullZones::CurrentFlags_Player & CullFlag_NoPolice;
}

bool CCullZones::PoliceAbandonCars()
{
	return CCullZones::CurrentFlags_Player & CullFlag_PoliceAbandonCars;
}

bool CCullZones::InRoomForAudio()
{
	return CCullZones::CurrentFlags_Camera & CullFlag_InRoomForAudio;
}

bool CCullZones::FewerCars()
{
	return CCullZones::CurrentFlags_Player & CullFlag_FewerCars;
}

bool CCullZones::FewerPeds()
{
	return CCullZones::CurrentFlags_Player & CullFlag_FewerPeds;
}

bool CCullZones::DoINeedToLoadCollision()
{
	return CCullZones::CurrentFlags_Player & CullFlag_DoINeedToLoadCollision;
}

bool CCullZones::CamNoRain()
{
	return CCullZones::CurrentFlags_Camera & CullFlag_NoRain;
}

bool CCullZones::PlayerNoRain()
{
	return CCullZones::CurrentFlags_Player & CullFlag_NoRain;
}

bool CCullZones::DoExtraAirResistanceForPlayer()
{
	return CCullZones::CurrentFlags_Player & CullFlag_DoExtraAirResistance;
}

void CCullZones::Update()
{
	if ((CTimer::m_FrameCounter & 7) == 2)
	{
		CCullZones::CurrentFlags_Camera = CCullZones::FindAttributesForCoors(*TheCamera.GetGameCamPosition());
	}
	else if ((CTimer::m_FrameCounter & 7) == 6)
	{
		CVector vecPlayerCoors = FindPlayerCoors(-1);
		CCullZones::CurrentFlags_Player = CCullZones::FindAttributesForCoors(vecPlayerCoors);
		if (!CCullZones::bMilitaryZonesDisabled)
		{
			if (CCullZones::CurrentFlags_Player & CullFlag_MilitaryZone)
			{
				if (FindPlayerPed(-1)->IsAlive())
				{
					FindPlayerPed(-1)->SetWantedLevelNoDrop(5);
				}
			}
		}
	}
}

unsigned int CCullZones::FindAttributesForCoors(CVector vecPos)
{
	unsigned int Flags = 0;

	for (int i = 0; i < CCullZones::NumAttributeZones; ++i)
	{
		CCullZone * pZone = &CCullZones::aAttributeZones[i];
		if (pZone->IsPointWithin(vecPos))
			Flags |= pZone->flags;
	}

	return Flags;
}