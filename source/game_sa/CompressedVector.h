/*
    Plugin-SDK (Grand Theft Auto San Andreas) header file
    Authors: GTA Community. See more here
    https://github.com/DK22Pac/plugin-sdk
    Do not delete this comment block. Respect others' work!
*/
#pragma once

#include "PluginBase.h"

class  CompressedVector {
public:
    signed short x;
    signed short y;
    signed short z;
};

template <int F>
CVector _DecompressVector(CompressedVector const & cvecComp)
{
	CVector full;
	full.x = cvecComp.x / F;
	full.y = cvecComp.y / F;
	full.z = cvecComp.z / F;
	return full;
}

VALIDATE_SIZE(CompressedVector, 6);
