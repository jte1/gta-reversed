#include "StdInc.h"

bool &gbFirstPersonRunThisFrame = *reinterpret_cast<bool *>(0xB6EC20);
float &gfPedNearClipLimit = *reinterpret_cast<float *>(0x8CC38C);//0.5f
float &fTranslateCamUp = *reinterpret_cast<float *>(0x8CC7D0);
float &fMulPedAngle = *reinterpret_cast<float *>(0x8CC7CC);
unsigned short &nFadeControlThreshhold = *reinterpret_cast<unsigned short *>(0x8CC7D4);
float &fDefaultAlphaOrient = *reinterpret_cast<float *>(0x8CC7D8);
float &fPedVertAngleMul = *reinterpret_cast<float *>(0x8CC7C8);
float &fPedBaseMultAngle = *reinterpret_cast<float *>(0x8CC7C4);
float &fPedBaseAngle = *reinterpret_cast<float *>(0x8CC7C0);
float &flt_8CCEA0 = *reinterpret_cast<float *>(0x8CCEA0);
float &flt_8CCE74 = *reinterpret_cast<float *>(0x8CCE74);
float &flt_8CCE70 = *reinterpret_cast<float *>(0x8CCE70);
float &flt_8CCE68 = *reinterpret_cast<float *>(0x8CCE68);
float &flt_8CCE6C = *reinterpret_cast<float *>(0x8CCE6C);
float &AIMWEAPON_STICK_SENS = *reinterpret_cast<float *>(0x8cc4a0);
float &flt_8CCE90 = *reinterpret_cast<float *>(0x8CCE90);
bool &byte_B6EC54 = *reinterpret_cast<bool *>(0xB6EC54);
float &flt_8CCE9C = *reinterpret_cast<float *>(0x8CCE9C);
float &flt_8CCE98 = *reinterpret_cast<float *>(0x8CCE98);
float &flt_8CCE94 = *reinterpret_cast<float *>(0x8CCE94);
float &flt_8CCE8C = *reinterpret_cast<float *>(0x8CCE8C);
float &flt_8CCE88 = *reinterpret_cast<float *>(0x8CCE88);
float &flt_8CCE84 = *reinterpret_cast<float *>(0x8CCE84);
float &flt_8CCE80 = *reinterpret_cast<float *>(0x8CCE80);
float &flt_8CCE7C = *reinterpret_cast<float *>(0x8CCE7C);
float &flt_8CCE78 = *reinterpret_cast<float *>(0x8CCE78);
int &gCameraDirection = *reinterpret_cast<int *>(0x8CC384);
int &gDWLastModeForCineyCam = *reinterpret_cast<int *>(0x8CC488);
int &gLastCamMode = *reinterpret_cast<int *>(0x8CC388);
bool &gbForceFollowPedMouse = *reinterpret_cast<bool *>(0x8CCF00);
bool *gbExitCam = reinterpret_cast<bool*>(0xB6EC70); // bool gbExitCam[9]
unsigned int &gDWCineyCamEndTime = *reinterpret_cast<unsigned int*>(0x8CCBA4);
float * gMovieCamMinDist = reinterpret_cast<float*>(0x8CCBCC); // float gMovieCamMinDist[9]
float * gMovieCamMaxDist = reinterpret_cast<float*>(0x8CCBF0); // float gMovieCamMaxDist[9]

struct
{
	float fOffsetZ;//field_0
	float field_4;
	float fTargetZoom;//field_8
	float field_C;
	float field_10;
	float field_14;
	float field_18;
	unsigned char field_1C;
	unsigned char field_1D;
	unsigned char field_1E;
	unsigned char field_1F;
	float field_20;
	float field_24;
	float field_28;
	float field_2C;
	float field_30;
	float field_34;
	float field_38;
} PEDCAM_SET[3] =
{
	{0.6f, 2.0, 0.15f, 2.0, 4.0, 0.8f, 0.1, 0, 0, 0, 63, 
	0.8f, 0.1, 0.1, 0.02, 1.0, DEG_TO_RAD(45.0f), DEG_TO_RAD(85.0f)},
	{0.6f, 2.0, 0.15f, 2.0, 3.0, 0.9f, 0.1, 0, 0, 128, 63, 0.8f,
	 0.1, 0.3f, 0.05f, 1.0, DEG_TO_RAD(45.0f), DEG_TO_RAD(45.0f)},
	{0.6f, 2.0, 0.15f, 2.0, 4.0, 0.8f, 0.1, 0, 0, 0, 63, 0.8f, 0.1,
	0.1, 0.02, 1.0, DEG_TO_RAD(45.0f), DEG_TO_RAD(85.0f)}
};

float NormalizeRadianAngle(float fAngle)
{
	if (fAngle > rwPI)
	{
		return fAngle - 2 * rwPI;
	}
	if (fAngle < -rwPI)
	{
		return fAngle + 2 * rwPI;
	}
	return fAngle;
}

void CCam::InjectHooks()
{
	InjectHook(0x50F970, &CCam::Process_FollowPedWithMouse, PATCH_JUMP);
	InjectHook(0x522D40, &CCam::Process_FollowPed_SA, PATCH_JUMP);
}

void CCam::NormalizeHorizontalAngle()
{
	if (this->m_fHorizontalAngle > rwPI) // CHECK
	{
		this->m_fHorizontalAngle -= 2 * rwPI;
	}
	else if (this->m_fHorizontalAngle < -rwPI)
	{
		this->m_fHorizontalAngle += 2 * rwPI;
	}
}

void CCam::Process_FollowPedWithMouse(CVector const & vecPos, float fAngle, float fSpeed, float)
{
	this->m_fFOV = 70.0f;
	
	if (this->m_pCamTargetEntity->_GetEntityType() != ENTITY_TYPE_PED)
		return;

	if (this->m_bResetStatics)
	{
		this->m_bRotating = false;
		this->m_bCollisionChecksOn = true;
		CPad::GetPad(0)->ClearMouseHistory();
		this->m_bResetStatics = false;
	}

	bool bIsTaxi = false;//v94
	if (FindPlayerVehicle(-1, 0) && FindPlayerVehicle(-1, 0)->m_nVehicleClass == CLASS_TAXI)
		bIsTaxi = true;

	CVector vecStart = vecPos;
	vecStart.z += fTranslateCamUp;
	float fDeltaHorizontal;//v91
	float fDeltaVertical;//v83
	if (CPad::GetPad(0)->bPlayerSafe)
	{
		CVector camToTarget = this->m_vecSource - vecStart;
		camToTarget.Normalise();

		float fHeadingHorizontal;//v9
		if (camToTarget.z >= -0.9f)
			fHeadingHorizontal = atan2(camToTarget.y, camToTarget.x);
		else
			fHeadingHorizontal = fAngle + rwPI;
		fDeltaVertical = 0.0f;
		fDeltaHorizontal = fHeadingHorizontal - this->m_fHorizontalAngle;
	}
	else
	{
		if (CPad::GetPad(0)->NewMouseControllerState.X == 0.0
			&& CPad::GetPad(0)->NewMouseControllerState.Y == 0.0
			|| CPad::GetPad(0)->DisablePlayerControls != 0)
		{
			float fLookLeftRight = -CPad::GetPad(0)->LookAroundLeftRight();
			float fLookUpDown = CPad::GetPad(0)->LookAroundUpDown();
			float fFOV = this->m_fFOV / 80.0f;
			fDeltaHorizontal = fFOV / 14.0f * CTimer::ms_fTimeStep * fMulPedAngle * fLookLeftRight;
			fDeltaVertical = fFOV * 0.042857144 * CTimer::ms_fTimeStep * fMulPedAngle * fLookUpDown;
		}
		else
		{
			float fFOV = this->m_fFOV / 80.0f;
			fDeltaHorizontal = CCamera::m_fMouseAccelHorzntl * fFOV * (CPad::GetPad(0)->NewMouseControllerState.X * -2.5f);
			fDeltaVertical = CCamera::m_fMouseAccelVertical * fFOV * (CPad::GetPad(0)->NewMouseControllerState.Y * 4.0f);
		}
	}

	float fVerticalAngleAdd; //v16
	if (TheCamera.m_bFading
		&& TheCamera.m_nFadeInOutFlag == 1
		&& CDraw::FadeValue > nFadeControlThreshhold
		|| CDraw::FadeValue > 200
		|| CPad::GetPad(0)->bPlayerSafe)
	{
		if (fDefaultAlphaOrient - 0.05f <= this->m_fVerticalAngle)
		{
			if (this->m_fVerticalAngle >= fDefaultAlphaOrient) // CHECK
			{
				fVerticalAngleAdd = fDefaultAlphaOrient - this->m_fVerticalAngle;
			}
			else if (fDefaultAlphaOrient + 0.05f >= this->m_fVerticalAngle)
			{
				if (this->m_fVerticalAngle <= fDefaultAlphaOrient)
					fVerticalAngleAdd = 0.0;
				else
					fVerticalAngleAdd = fDefaultAlphaOrient - this->m_fVerticalAngle;
			}
			else
			{
				fVerticalAngleAdd = -0.05f;
			}
		}
		else
		{
			fVerticalAngleAdd = 0.05f;
		}
	}
	else
	{
		fVerticalAngleAdd = fDeltaVertical;
	}

	this->m_fHorizontalAngle += fDeltaHorizontal;
	this->m_fVerticalAngle += fVerticalAngleAdd;

	this->NormalizeHorizontalAngle();

	this->m_fVerticalAngle = Clamp(-DEG_TO_RAD(89.5f), this->m_fVerticalAngle, DEG_TO_RAD(45.0f));

	float v23;
	if (this->m_fVerticalAngle <= 0.0)
	{
		v23 = cos(this->m_fVerticalAngle);
	}
	else
	{
		v23 = cos(ClampMax(fPedVertAngleMul * this->m_fVerticalAngle, DEG_TO_RAD(90.0f)));
	}

	float v84 = v23 * fPedBaseMultAngle + fPedBaseAngle;

	if (TheCamera.m_bUseScriptZoomValuePed)
		this->m_fHorizontalAngle = this->m_fTransitionBeta;

	if (TheCamera.m_bCamDirectlyInFront)
		this->m_fHorizontalAngle = TheCamera.m_fStartingAlphaForInterPol + rwPI;

	if (TheCamera.m_bCameraJustRestored)
		this->m_fHorizontalAngle = TheCamera.m_fStartingAlphaForInterPol;

	if (bIsTaxi)
		this->m_fHorizontalAngle = fAngle;

	this->m_vecFront.x = -(cos(this->m_fHorizontalAngle) * cos(this->m_fVerticalAngle));
	this->m_vecFront.y = -(sin(this->m_fHorizontalAngle) * cos(this->m_fVerticalAngle));
	this->m_vecFront.z = sin(this->m_fVerticalAngle);

	this->m_vecSource = vecStart - v84 * this->m_vecFront;

	this->m_vecTargetCoorsForFudgeInter = vecStart;

	CWorld::pIgnoreEntity = this->m_pCamTargetEntity;
	CColPoint hitPoint;
	CEntity * pHitEntity;
	if (CWorld::ProcessLineOfSight(
		vecStart,
		this->m_vecSource,
		hitPoint,
		pHitEntity,
		1,
		1,
		1,
		1,
		0,
		0,
		1,
		0))
	{
		CVector hitRay = vecStart - hitPoint.m_vecPoint;
		float fDistanceToHit = hitRay.Magnitude();//v92
		float v85 = v84 - fDistanceToHit;
		if (pHitEntity->_GetEntityType() != ENTITY_TYPE_PED || v85 <= 0.4f)
		{
			this->m_vecSource = hitPoint.m_vecPoint;

			if (fDistanceToHit < 0.6f)
			{
				float fNearClip = ClampMin(fDistanceToHit - 0.3f, 0.05f);
				RwCameraSetNearClipPlane(Scene.m_pRwCamera, fNearClip);
			}
		}
		else
		{
			if (!CWorld::ProcessLineOfSight(
				hitPoint.m_vecPoint,
				this->m_vecSource,
				hitPoint,
				pHitEntity,
				1,
				1,
				1,
				1,
				0,
				0,
				1,
				0))
			{
				float fNearClip = ClampMax(v85 - 0.35f, 0.9f);
				RwCameraSetNearClipPlane(Scene.m_pRwCamera, fNearClip);
			}
			else
			{
				CVector posToHit = vecStart - hitPoint.m_vecPoint;
				float rayLen = posToHit.Magnitude();

				this->m_vecSource = hitPoint.m_vecPoint;
				
				if (rayLen < 0.6f)
				{
					float fNearClip = ClampMin(rayLen - 0.3f, 0.05f);
					RwCameraSetNearClipPlane(Scene.m_pRwCamera, fNearClip);
				}
			}
		}
	}
	
	CWorld::pIgnoreEntity = 0;
	float v87 = CDraw::ms_fAspectRatio * tan(this->m_fFOV * DEG_TO_RAD(1) * 0.5) * 1.1;
	float fNearPlane = Scene.m_pRwCamera->nearPlane;//v93

	CVector vecSphereCenter = this->m_vecSource + fNearPlane * this->m_vecFront;
	float fSphereRadius = fNearPlane * v87;

	bool v61 = CWorld::TestSphereAgainstWorld(vecSphereCenter, fSphereRadius, nullptr, 1, 1, 0, 1, 0, 0);

	for (int v47 = 0; v47 <= 5 && v61; ++v47)
	{
		CVector tempToCam = gaTempSphereColPoints[0].m_vecPoint - this->m_vecSource;
		float fDot = DotProduct(tempToCam, this->m_vecFront);
		CVector front = fDot * this->m_vecFront;
		CVector df = tempToCam - front;
		float v88 = df.Magnitude() / v87;


		v88 = ClampMax(v88, fNearPlane);
		v88 = Clamp(0.1f, v88, fNearPlane);

		if (v88 < fNearPlane)
			RwCameraSetNearClipPlane(Scene.m_pRwCamera, v88);

		if (v88 == 0.1f)
		{
			CVector vecPosToCam = vecStart - this->m_vecSource;
			this->m_vecSource += vecPosToCam * 0.3f;
		}

		fNearPlane = Scene.m_pRwCamera->nearPlane;
		v87 = tan(this->m_fFOV * 0.017453292 * 0.5) * CDraw::ms_fAspectRatio * 1.1;
		float fSphereRadius = fNearPlane * v87;
		vecSphereCenter = this->m_vecSource + fNearPlane * this->m_vecFront;
		v61 = CWorld::TestSphereAgainstWorld(vecSphereCenter, fSphereRadius, nullptr, 1, 1, 0, 1, 0, 0);
	}

	CVector posToCamera = vecStart - this->m_vecSource;//,v66,v67

	float fDistToCamera = posToCamera.Magnitude();//v89,v68

	if (fDistToCamera >= this->m_fDistance)
	{
		float fSmoothFactor = pow(0.92f, CTimer::ms_fTimeStep);

		this->m_fDistance = Lerp(fDistToCamera, this->m_fDistance, fSmoothFactor);// v69

		if (fDistToCamera > 0.05f)
		{
			CVector vecCamToPos = this->m_vecSource - vecStart;

			this->m_vecSource = vecStart + vecCamToPos * (this->m_fDistance / fDistToCamera);
		}
		float fNearClip = this->m_fDistance - gfPedNearClipLimit;
		if (fNearClip < Scene.m_pRwCamera->nearPlane)
		{
			fNearClip = ClampMin(fNearClip, 0.1f);
				
			RwCameraSetNearClipPlane(Scene.m_pRwCamera, fNearClip);
		}
	}
	else
	{
		this->m_fDistance = fDistToCamera;
	}

	TheCamera.m_bCamDirectlyInFront = false;
	TheCamera.m_bCameraJustRestored = false;

	this->GetVectorsReadyForRW();

	if (TheCamera.m_bFading && TheCamera.m_nFadeInOutFlag == 1 && CDraw::FadeValue > 128)
	{
		float fAngle = atan2(-this->m_vecFront.x, this->m_vecFront.y);//v90
	
		CPed * pPed = (CPed *)TheCamera.m_pTargetEntity;

		pPed->m_fCurrentRotation = fAngle;
		pPed->m_fAimingRotation = fAngle;
		pPed->SetHeading(fAngle);

		RpClump * pClump = TheCamera.m_pTargetEntity->m_pRwClump;

		if (pClump)
		{
			RwFrame * pFrame = (RwFrame*)pClump->object.parent;//RpClumpGetFrame(pClump);
			TheCamera.m_pTargetEntity->_UpdateRwMatrix(RwFrameGetMatrix(pFrame));
		}
	}
}

void CCam::GetVectorsReadyForRW()
{
	CVector up(0, 0, 1);

	this->m_vecUp = up;

	this->m_vecFront.Normalise();
	if (this->m_vecFront.x == 0.0 && this->m_vecFront.y == 0.0)
	{
		this->m_vecFront.x = 0.0001;
		this->m_vecFront.y = 0.0001;
	}
	up = CrossProduct(this->m_vecFront, this->m_vecUp);
	up.Normalise();
	this->m_vecUp = CrossProduct(up, this->m_vecFront);
}


void CCam::Process_FollowPed_SA(CVector const & vecTargetPos, float fTargetAngle, float fSpeed, float, bool a6)
{
	if (this->m_pCamTargetEntity->_GetEntityType() != ENTITY_TYPE_PED)
		return;

	CPed * pPed = (CPed *)this->m_pCamTargetEntity; // v8, v6
	if (pPed->IsPlayer() == false)
		return;

	CPad * pPad = CPad::GetPad(0);
	if (pPed->m_nPedType == 1)
		pPad = CPad::GetPad(1);

	CVector targPosn = vecTargetPos;//,v9,v10

	int iPedCamSet = 0; // pedtype ,v11, v13, v170

	if (CGame::currArea)
	{
		iPedCamSet = 1;
	}

	float v171 = PEDCAM_SET[iPedCamSet].field_4;

	if (false == pPed->bIsStanding)
	{
		if (TheCamera.m_nPedZoom == 3)
		{
			if (pPed->m_pIntelligence->GetUsingParachute())
				v171 = v171 + v171;
		}
	}

	float v165 = PEDCAM_SET[iPedCamSet].field_C;
	v171 = PEDCAM_SET[iPedCamSet].field_34;
	float num = PEDCAM_SET[iPedCamSet].field_10;//v17
	float v158 = TheCamera.m_fPedZoomSmoothed + v171;//v16
	float v172 = PEDCAM_SET[iPedCamSet].field_38;
	
	// CHECK
	//if (!(v19 | v20))
	//	v165 = v158;

	//flt_B6EC50 = v158;

	float v162 = PEDCAM_SET[iPedCamSet].fTargetZoom;//field_8 -> fTargetZoom

	switch (TheCamera.m_nPedZoom)
	{
	case 1:
		v162 += this->m_fTargetZoomOneZExtra;
		break;
	case 2:
		if (iPedCamSet == 1)
			v162 += this->m_fTargetZoomTwoInteriorZExtra;
		else
			v162 += this->m_fTargetZoomTwoZExtra;
		break;
	case 3:
		v162 += this->m_fTargetZoomThreeZExtra;
		break;
	default:
		break;
	}
	float v169 = 0.0;
	float v166 = 0.0;
	CPedIntelligence * pIntelligence = pPed->m_pIntelligence;//v25
	CTaskSimpleSwim * pTaskSwim = pIntelligence->GetTaskSwim();//v24
	if (pIntelligence->GetTaskSwim())
	{
		v166 = 1.0;
		if (pIntelligence->GetTaskSwim()->m_nSwimState != eSwimState::SWIM_UNDERWATER_SPRINTING)
			v169 = 0.5;
	}
	else if (pIntelligence->GetTaskJetPack())
	{
		v166 = 0.5;
		if (pPed->bIsStanding == false)
			v169 = 3.0;
	}
	if (0 == TheCamera.m_bTransitionState)
	{
		if (this->m_bResetStatics)
			this->m_fFOV = 70.0;
		else
		{
			float v27 = CTimer::ms_fTimeStep * 1.0f;
			float v28 = v27 + this->m_fFOV;
			if (v28 < 70.0)
			{
				this->m_fFOV = v28;
			}
			else
			{
				this->m_fFOV = ClampMin(this->m_fFOV - v27, 70.0f);
			}
		}
	}

	targPosn.z = targPosn.z + PEDCAM_SET[iPedCamSet].fOffsetZ;//field_0 -> offset z

	float v168 = v158 <= num ? num : v158;

	bool flag2 = true;
	bool flag1 = a6;
	if (this->m_bResetStatics || TheCamera.m_bCamDirectlyBehind || TheCamera.m_bCamDirectlyInFront)
	{
		if (!a6)
		{
			flag2 = false;
		}
		flag1 = true;
	}
	if (flag1)
	{
		CVector in, outVec;
		if (flag2)
		{
			in = pPed->GetPosition();

			outVec = CVector(0, 0, 0);
			/**
			inVec = 0;
			dword_B6EC80 = 0;
			dword_B6EC84 = 0;
			*/
			targPosn = pPed->GetPosition();
			targPosn.z += PEDCAM_SET[iPedCamSet].fOffsetZ;
		}
		TheCamera.ResetDuckingSystem(pPed);

		this->m_bRotating = false;
		//byte_B6EC54 = 0;
		this->m_bCollisionChecksOn = true;
		
		if (!TheCamera.m_bJustCameOutOfGarage && !a6)
		{
			this->m_fHorizontalAngle = pPed->_GetHeading() - DEG_TO_RAD(90.0f);
			if (TheCamera.m_bCamDirectlyInFront)
				this->m_fHorizontalAngle += rwPI;
		}
		this->m_fBetaSpeed = 0.0;
		this->m_fAlphaSpeed = 0.0;
		this->m_fDistance = 1000.0;
		
		if (v158 == TheCamera.m_fPedZoomBase)
		{
			TheCamera.m_fPedZoomSmoothed = TheCamera.m_fPedZoomTotal;
			v158 = TheCamera.m_fPedZoomTotal;
		}

		if (!TheCamera.m_bJustCameOutOfGarage && !a6)
		{
			this->m_fVerticalAngle = 0.0f;
			if (pPed->bIsStanding)
			{
				//CHECK
				this->m_fVerticalAngle = -asin(Clamp(-1.0f, DotProduct(pPed->field_578, pPed->m_matrix->up), 1.0f));
			}
		}

		this->m_vecFront.x = -(cos(this->m_fHorizontalAngle) * cos(this->m_fVerticalAngle));
		this->m_vecFront.y = -(sin(this->m_fHorizontalAngle) * cos(this->m_fVerticalAngle));
		this->m_vecFront.z = sin(this->m_fVerticalAngle);

		this->m_avecTargetHistoryPos[0] = targPosn - v168 * this->m_vecFront;
		this->m_anTargetHistoryTime[0] = CTimer::m_snTimeInMilliseconds;
		this->m_avecTargetHistoryPos[1] = targPosn - v158 * this->m_vecFront;

		this->m_nCurrentHistoryPoints = 0;

		if (!TheCamera.m_bJustCameOutOfGarage && !a6)
			this->m_fVerticalAngle = -v162;

		if (pPed->m_pIntelligence->GetTaskSwim()
			&& pPed->m_pIntelligence->GetTaskSwim()->m_nSwimState != SWIM_UNDERWATER_SPRINTING)
		{
			this->m_fVerticalAngle -= DEG_TO_RAD(15.0f);
			
		}
		else
		{
			if (pPed->m_pIntelligence->GetTaskJetPack())
			{
				this->m_fVerticalAngle -= DEG_TO_RAD(20.0f);
			}
		}
		CPad::GetPad(0)->ClearMouseHistory();
	}
	else
	{
		CEntity * pContactEntity = pPed->m_pContactEntity;//v31
		if (pContactEntity)
		{
			CVehicle * pVehicle = (CVehicle *)pContactEntity;
			CPhysical * pAttachedEntity = pVehicle->m_pAttachedTo;
			CVehicle * pAttachedVehicle = (CVehicle *)pAttachedEntity;
			// CHECK
#if 0
			if (pContactEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE && pVehicle->m_nVehicleSubClass == VEHICLE_TRAIN
				|| (pAttachedEntity != nullptr
					&& pAttachedEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE
					&& pAttachedVehicle->m_nVehicleSubClass == VEHICLE_TRAIN))
			{
				float fVelocity = pVehicle->m_vecMoveSpeed.Magnitude();
				float v36 = max(0.0, fVelocity - flt_8CCEA0);
				float v37 = max(flt_8CCEA0, fVelocity);
				this->m_avecTargetHistoryPos[0] += (v36 / v37) * pVehicle->m_vecMoveSpeed * CTimer::ms_fTimeStep;
				this->m_avecTargetHistoryPos[1] += pVehicle->m_vecMoveSpeed * CTimer::ms_fTimeStep;
			}
#endif
		}
	}

	this->m_vecFront = targPosn - this->m_avecTargetHistoryPos[0];
	this->m_vecFront.Normalise();

	CVector vecHistory = targPosn - this->m_avecTargetHistoryPos[1];//v66,v67,v68
	float fHistoryLen = vecHistory.Magnitude();

	if (fHistoryLen < v158 && v158 > PEDCAM_SET[iPedCamSet].field_C)
	{
		v158 = ClampMin(v165, fHistoryLen);
	}

	num = 0.0;
	float v151 = 0.0;
	float v147 = 0.0;
	
	float v159 = atan2(-this->m_vecFront.x, this->m_vecFront.y) - DEG_TO_RAD(90.0f);
	if (v159 < -rwPI)
		v159 += 2 * rwPI;

	float v73 = pPed->_GetHeading();//atan2(-x,y)
	float v74 = v73 - DEG_TO_RAD(90.0f);//v165
	v165 = v74;
	float v75 = v74 - v159;
	if (v75 <= rwPI)
	{
		if (v75 < -rwPI)
			v165 += 2 * rwPI;
	}
	else
	{
		v165 -= 2 * rwPI;
	}

	if (pPad->GetForceCameraBehindPlayer())
	{
		byte_B6EC54 = 1;
	}
	else if (byte_B6EC54
		&& (//!(v78 | v79)||
			 fabs(v165 - v159) < 0.01f
			|| pPad->AimWeaponLeftRight(pPed)
			|| pPad->AimWeaponUpDown(pPed)))
	{
		byte_B6EC54 = 0;
	}
	float v80 = v165 - v159;
	float v154 = v80;
	float v86;
	if (fabs(v80) < flt_8CCE9C || byte_B6EC54 || v166 != 0.0)
	{
		float v152 = CTimer::ms_fTimeStep * PEDCAM_SET[iPedCamSet].field_28;
		float v147 = CTimer::ms_fTimeStep * PEDCAM_SET[iPedCamSet].field_2C;
		if (byte_B6EC54 || v166 != 0.0)
		{
			v151 = ClampMax(v152 * flt_8CCE98, 1.0f);
			v147 *= flt_8CCE94;
			if (v166 != 0.0)
			{
				v151 *= v166;
				v147 *= v166;
			}
		}
		else
		{
			CPhysical * pContactEntity = (CPhysical *)pPed->m_pContactEntity;
			if (pContactEntity)
			{
				if ((pPed->m_vecMoveSpeed - pContactEntity->m_vecMoveSpeed).Magnitude() * v152 <= 1.0)
				{
					v151 = (pPed->m_vecMoveSpeed - pContactEntity->m_vecMoveSpeed).Magnitude() * v152;
				}
				else
				{
					v151 = 1.0;
				}
			}
			else if (pPed->m_vecMoveSpeed.Magnitude() * v152 <= 1.0)
			{
				v151 = pPed->m_vecMoveSpeed.Magnitude() * v152;
			}
			else
			{
				v151 = 1.0;
			}
		}
		v86 = Clamp(-v147, v154 * v151, v147);
	}
	else
	{
		v86 = num;
	}
	float v160 = v159 + v86;
	if (this->m_fHorizontalAngle + rwPI >= v160)
	{
		if (this->m_fHorizontalAngle - rwPI > v160)
			v160 += 2 * rwPI;
	}
	else
	{
		v160 -= 2 * rwPI;
	}
	float v89 = ClampMin(CTimer::ms_fTimeStep, 1.0f);

	float v174 = (v160 - this->m_fHorizontalAngle) / v89;

	num = Clamp(-1.0f, this->m_vecFront.z, 1.0f);

	float v155 = asin(num);
	float v90 = fabs(v165 - v160);
	num = v90;
	if (v90 > flt_8CCE90 
		&& ((num - flt_8CCE90) * 1.2f) / (rwPI - flt_8CCE90) != 1.0f ) // CHECK
	{
		float v94 = ClampMax((num - flt_8CCE90) * 1.2f / (rwPI - flt_8CCE90), 1.0f);
		num = DEG_TO_RAD(90.0f) - (DEG_TO_RAD(90.0f) - flt_8CCE8C) * v94;
		float v95 = pow(flt_8CCE88, CTimer::ms_fTimeStep);
		if (v155 <= (double)num)
		{
			if (-num > v155)
				v155 = v155 * v95 - (1.0 - v95) * num;
		}
		else
		{
			v155 = (1.0 - v95) * num + v95 * v155;
		}
	}
	float v96 = 0.0;
	if (v169 != 0.0 || byte_B6EC54 && pPed->bIsStanding)
	{
		num = 0.0;
		float v98;
		if (pPed->m_pIntelligence->GetTaskJetPack())
		{
			v98 = -DEG_TO_RAD(20.0f);
		}
		else if (pPed->m_pIntelligence->GetTaskSwim())
		{
			v98 = -DEG_TO_RAD(15.0f);
		}
		else if (pPed->bIsStanding)
		{
			num = Clamp(-1.0f, DotProduct(pPed->field_578, pPed->m_matrix->up), 1.0f);//v99

			v98 = -asin(num);
		}
		else
		{
			v98 = num;
		}
		
		float v100 = ClampMax(flt_8CCE84 * v151, 1.0f);

		float v148 = flt_8CCE80 * v147;
		if (v169 != 0.0)
		{
			v100 = v100 * v169;
			v148 = v148 * v169;
		}
		v96 = Clamp(-v148, v100 * (v98 - v155), v148);
	}
	float v156 = Clamp(-v172, v155 + v96 - v162, v171);
	float v149 = CTimer::ms_fTimeStep * PEDCAM_SET[iPedCamSet].field_18;
	float v103 = (1.0 - pow(PEDCAM_SET[iPedCamSet].field_14, CTimer::ms_fTimeStep)) * (v156 - this->m_fVerticalAngle);
	num = Clamp(-v149, v103, v149);
	float v164 = pPad->AimWeaponLeftRight(pPed);
	v169 = pPad->AimWeaponUpDown(pPed);
	float v161 = v169;

	if (pPed->m_pIntelligence->GetTaskUseGun())
	{
		int v105 = pPed->m_pIntelligence->GetTaskUseGun()->m_nLastCommand;
		if (v105 == 2 || v105 == 3)
		{
			if (pPed->m_pIntelligence->GetTaskUseGun()->m_pWeaponInfo)
			{
				if (false == pPed->m_pIntelligence->GetTaskUseGun()->m_pWeaponInfo->m_nFlags.bAimWithArm)
				{
					v169 = pPad->GetPedWalkLeftRight();
					if (fabs(v164) < fabs(v169))
					{
						v169 = -pPad->GetPedWalkLeftRight();
						v164 = v169;
					}
				}
			}
		}
	}
#if 0
	if ((v161 != 0.0 || v164 != 0.0) && !pPad->GetPedWalkLeftRight() && !pPad->GetPedWalkUpDown())
	{
		CPed * pPlayerPed = FindPlayerPed(0);
		CVector out = pPlayerPed->m_matrix->up;

		CPlaceable__getTopVector(&TheCamera.__parent, &outVec);

		if (DotProduct(out, outVec) > 0.3f)
		{
			outVec = pPlayerPed->GetPosition() + outVec * 5.0;
			IKChainManager_c::LookAt("FollowPedSA", pPlayerPed, 0, 1500, -1, outVec.ToRwV3D(), 0, 0.25, 500, 3, 0);
		}
	}
#endif
	float v113 = this->m_fFOV * 0.0125;
	v164 = fabs(v164) * (v113 * 0.071428575) * AIMWEAPON_STICK_SENS * AIMWEAPON_STICK_SENS * v164;
	v161 = fabs(v161)
		* (v113
			* 0.042857144)
		* AIMWEAPON_STICK_SENS
		* AIMWEAPON_STICK_SENS
		* v161;

	if (pPed->m_pIntelligence->GetTaskClimb())
	{
		pPed->m_pIntelligence->GetTaskClimb()->GetCameraStickModifier(pPed, this->m_fVerticalAngle,
			this->m_fHorizontalAngle, v161, v164);
	}
	else
	{
		if (pPed->m_pIntelligence->m_TaskMgr.GetActiveTask()->GetId() == 701)
		{
			CTaskComplexEnterCar * pTask = (CTaskComplexEnterCar *)pPed->m_pIntelligence->m_TaskMgr.GetActiveTask();
			pTask->GetCameraStickModifier(
				pPed,
				v158, this->m_fVerticalAngle, this->m_fHorizontalAngle,
				v161, v164);
		}
	}
	float v153 = pow(PEDCAM_SET[iPedCamSet].field_20, CTimer::ms_fTimeStep);
	float v150 = PEDCAM_SET[iPedCamSet].field_24;
	float v115 = Clamp(-v150, v164 + v174, v150);
	this->m_fBetaSpeed = Lerp(v115, this->m_fBetaSpeed, v153); //v116
	
	if (fabs(this->m_fBetaSpeed) < flt_8CCE7C)
		this->m_fBetaSpeed = 0.0;

	if (!CCamera::m_bUseMouse3rdPerson || pPad->DisablePlayerControls)
	{
		this->m_fHorizontalAngle += CTimer::ms_fTimeStep * this->m_fBetaSpeed;
	}
	else
	{
		this->m_fHorizontalAngle += CPad::NewMouseControllerState.X * -2.5 * (this->m_fFOV * 0.0125) * CCamera::m_fMouseAccelHorzntl;
		this->m_fBetaSpeed = 0.0;
		//v164 = v117;
	}

	this->NormalizeHorizontalAngle();

	this->m_fAlphaSpeed = Clamp(-v150, (1.0f - v153) * this->m_fAlphaSpeed + *(float *)&v161 * v153, v150); // v118,v174

	if (fabs(this->m_fAlphaSpeed) < flt_8CCE78)
		this->m_fAlphaSpeed = 0.0f;

	float v157 = CTimer::ms_fTimeStep * this->m_fAlphaSpeed + v156;

	float v122;
	if (!CCamera::m_bUseMouse3rdPerson || pPad->DisablePlayerControls)
	{
		v122 = num;
	}
	else
	{
		if ((!TheCamera.m_bFading || TheCamera.GetFadingDirection() != 1 || CDraw::FadeValue <= 45)
			&& CDraw::FadeValue <= 200)
		{
			v122 = CPad::NewMouseControllerState.Y * 2.5 * (this->m_fFOV * 0.0125) * CCamera::m_fMouseAccelHorzntl;
			this->m_fAlphaSpeed = 0.0;
		}
		else
		{
			float v121 = -v162;
			float v170 = v121;
			if (v121 - 0.05f <= this->m_fVerticalAngle)
			{
				float v123 = this->m_fVerticalAngle;
				float v125 = v170;
				if (v125 <= this->m_fVerticalAngle) // CHECK
				{
					v122 = v125 - this->m_fVerticalAngle;
					this->m_fAlphaSpeed = 0.0;
				}
				else if (v125 + 0.05f < this->m_fVerticalAngle)
				{
					v122 = -0.05f;
					this->m_fAlphaSpeed = 0.0;
				}
				else if (this->m_fVerticalAngle > v170)
				{
					v122 = v170 - this->m_fVerticalAngle;
					this->m_fAlphaSpeed = 0.0;
				}
				else
				{
					v122 = 0.0;
					this->m_fAlphaSpeed = 0.0;
				}
			}
			else
			{
				v122 = 0.05f;
				this->m_fAlphaSpeed = 0.0f;
			}
		}
	}
	this->m_fVerticalAngle += v122;
	if (this->m_fVerticalAngle <= (double)v171)
	{
		if (-v172 > this->m_fVerticalAngle)
		{
			this->m_fVerticalAngle = -v172;
			this->m_fAlphaSpeed = 0.0f;
		}
	}
	else
	{
		this->m_fVerticalAngle = v171;
		this->m_fAlphaSpeed = 0.0f;
	}

	if (fabs(flt_8CCE74 - this->m_fVerticalAngle) < flt_8CCE70)
		this->m_fVerticalAngle = flt_8CCE74;

	float v131 = flt_8CCE6C - this->m_fHorizontalAngle;
	
	flt_8CCE74 = this->m_fVerticalAngle;
	
	if (fabs(v131) < flt_8CCE68)
		this->m_fHorizontalAngle = flt_8CCE6C;

	flt_8CCE6C = this->m_fHorizontalAngle;

	this->m_vecFront.x = -(cos(this->m_fHorizontalAngle) * cos(this->m_fVerticalAngle));
	this->m_vecFront.y = -(sin(this->m_fHorizontalAngle) * cos(this->m_fVerticalAngle));
	this->m_vecFront.z = sin(this->m_fVerticalAngle);

	this->GetVectorsReadyForRW();
	TheCamera.m_bCamDirectlyBehind = false;
	TheCamera.m_bCamDirectlyInFront = false;

	this->m_vecSource = targPosn - v158 * this->m_vecFront;

	VecTrunc(&this->m_vecSource, 4);

	CVector vecRot;
	vecRot.x = -(cos(this->m_fHorizontalAngle) * cos(v157 + v162));
	vecRot.y = -(sin(this->m_fHorizontalAngle) * cos(v157 + v162));
	vecRot.z = sin(v157 + v162);

	this->m_avecTargetHistoryPos[0] = targPosn - vecRot * v168;
	this->m_avecTargetHistoryPos[1] = targPosn - vecRot * v158;

	if (pPad->GetForceCameraBehindPlayer() && pPad->AimWeaponLeftRight(0))
	{
		float v142 = NormalizeRadianAngle(this->m_fHorizontalAngle - (pPed->m_fCurrentRotation - DEG_TO_RAD(90.0f)));
		
		if (CTimer::ms_fTimeStep * 0.1 > fabs(v142))
			pPed->m_fAimingRotation = this->m_fHorizontalAngle + DEG_TO_RAD(90.0f);
	}

	TheCamera.HandleCameraMotionForDucking(pPed, &this->m_vecSource, &targPosn, 0);

	this->m_vecTargetCoorsForFudgeInter = targPosn;

	VecTrunc(&this->m_vecSource, 4);

	CCamera::SetColVarsPed(iPedCamSet, TheCamera.m_nPedZoom);

	if (gCameraDirection == 3)
	{
		TheCamera.CameraGenericModeSpecialCases(pPed);
		CCollision::bCamCollideWithVehicles = true;
		CCollision::bCamCollideWithObjects = true;
		CCollision::bCamCollideWithPeds = true;

		TheCamera.CameraColDetAndReact(&this->m_vecSource, &targPosn);
		TheCamera.ImproveNearClip(nullptr, pPed, &this->m_vecSource, &targPosn);

		VecTrunc(&this->m_vecSource, 4);
	}

	TheCamera.m_bCamDirectlyBehind = false;
	TheCamera.m_bCamDirectlyInFront = false;

	VecTrunc(&this->m_vecSource, 4);

	this->GetVectorsReadyForRW();

	if (iPedCamSet
		|| TheCamera.m_nWhoIsInControlOfTheCamera == 1
		|| false == pPed->bIsStanding
		//|| CGameLogic::IsCoopGameGoingOn()
		|| TheCamera.m_bFOVLerpProcessed
		|| TheCamera.m_bVecMoveLinearProcessed
		|| TheCamera.m_bVecTrackLinearProcessed
		|| pPed->m_vecMoveSpeed.MagnitudeSquared() > 0.0001f)
	{

		//gIdleCam.field_94 = 0;
		this->m_bResetStatics = false;
	}
	else
	{
		//gIdleCam.Process();
		this->m_bResetStatics = false;
	}
}

bool CCam::LookLeft(void)
{
	return LookRight(false);
}

bool CCam::LookRight(bool bLookRight)
{
	static float f1stPersonVehicleDefaultClip = 0.05f;

	bool v67 = 1;

	if (this->m_nMode != MODE_CAM_ON_A_STRING
		&& this->m_nMode != MODE_BEHINDBOAT
		&& this->m_nMode != MODE_BEHINDCAR
		|| this->m_pCamTargetEntity->_GetEntityType() != ENTITY_TYPE_VEHICLE)
		v67 = 0;

	float v71 = 1.0; // direction mult
	if (bLookRight)
	{
		this->m_bLookingRight = 1;
	}
	else
	{
		this->m_bLookingLeft = 1;
		v71 = -1.0;
	}

	if (!v67)
	{
		if (this->m_nMode == MODE_1STPERSON && this->m_pCamTargetEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE)
		{
			CVehicle * pVehicle = (CVehicle *)this->m_pCamTargetEntity;

			if (bLookRight)
				this->m_bLookingRight = 1;
			else
				this->m_bLookingLeft = 1;

			RwCameraSetNearClipPlane(Scene.m_pRwCamera, f1stPersonVehicleDefaultClip);
#if 0
			v42 = pThis->CamTargetEntity;
			if (v42->pad13[0] == 5)
			{
				v43 = (CEntity *)v42->field_460;
				if (v43)
				{
					v72 = 0.0;
					v73 = 0.0;
					v74 = 0.0;
					v45 = (CPed *)v43;
					CPed::SetPedPositionInCar((int)v43, v43);
					CEntity::UpdateRW(&v45->__parent.__parent);
					CEntity::UpdateRwFrame(&v45->__parent.__parent);
					CEntity::UpdateRpHAnim(&v45->__parent.__parent);
					CPed::GetBonePosition(v45, (int)&v72, 4, 1);
					v44 = pThis->CamTargetEntity;
					if (bLookRight)
					{
						v66 = CPhysical::GetPosMatrix(&v44->__parent);
						v65 = *(float *)&dword_8CC498;
					}
					else
					{
						v66 = CPhysical::GetPosMatrix(&v44->__parent);
						v65 = dword_8CC49C;
					}
					v46 = scaleMatrixRight(&out, v65, &v66->matrix);
					vectorOpPlus((RwV3D *)&v72, v46);
					v47 = CPhysical::GetPosMatrix(&pThis->CamTargetEntity->__parent);
					v47 = (CMatrixLink *)((char *)v47 + 32);
					v49 = &pThis->Source.x;

					*v49 = flt_8CC494 * v47->matrix.matrix.at.x + v72;

					v49[1] = flt_8CC494 * v47->matrix.matrix.right.y + v73;

					v49[2] = flt_8CC494 * v47->matrix.matrix.right.z + v74;
				}
				else
				{
					pThis->Source.z = pThis->Source.z - 0.5;
				}
			}

			if (v52->pad13[0] != 9)
			{
				pThis->Source = pThis->Source - (this->CamTargetEntity->GetMatrix()._vecRight * 0.35f);
			}
#endif
			this->m_vecUp = this->m_pCamTargetEntity->GetMatrix()->at;

			this->m_vecUp.Normalise();

			this->m_vecFront = this->m_pCamTargetEntity->GetMatrix()->up;

			this->m_vecFront.Normalise();

			if (bLookRight)
				this->m_vecFront = CrossProduct(this->m_vecFront, this->m_vecUp);
			else
				this->m_vecFront = CrossProduct(this->m_vecUp, this->m_vecFront);

			this->m_vecFront.Normalise();

			if (pVehicle->GetVehicleAppearance() == VEHICLE_APPEARANCE_BIKE)
			{
				this->m_vecSource = this->m_vecSource - (this->m_vecFront * 1.45f);
			}
		}
		else if (this->m_pCamTargetEntity->_GetEntityType() != ENTITY_TYPE_PED)
		{
			return false;
		}
		return true;
	}

	float v68;
	float v70;

	CVector vecTargetEntityPos = this->m_pCamTargetEntity->GetPosition();

	if (this->m_nMode == MODE_CAM_ON_A_STRING)
	{
		v68 = this->m_fCaMaxDistance;
	}
	else
	{
		v68 = 9.0;
		if (this->m_nMode == MODE_BEHINDBOAT)
		{
			float fBoatHeightOffset = 0.0;
			if (GetBoatLook_L_R_HeightOffset(fBoatHeightOffset))
			{
#if 0
				if (!CCullZones::Cam1stPersonForPlayer())
					this->m_vecSource.z = vecTargetEntityPos.z + fBoatHeightOffset;
#endif
			}
		}
	}

	CMatrix matTargetEntity = *this->m_pCamTargetEntity->GetMatrix();

	matTargetEntity.up.Normalise();

	float v17 = CGeneral::GetATanOfXY(matTargetEntity.up.x, matTargetEntity.up.y);
	float v19 = v17 + v71 * rwPIOVER2;

	this->m_vecSource.x = cos(v19) * v68 + vecTargetEntityPos.x;
	this->m_vecSource.y = sin(v19) * v68 + vecTargetEntityPos.y;

	CColModel * v20 = this->m_pCamTargetEntity->GetColModel();

	float sourceZ = this->m_vecSource.z;

	CBox & boundBox = v20->m_boundBox;

	CWorld::pIgnoreEntity = this->m_pCamTargetEntity;

	TheCamera.m_nExtraEntitiesCount = 0;

	TheCamera.CameraVehicleModeSpecialCases((CVehicle *)this->m_pCamTargetEntity);

	TheCamera.CameraColDetAndReact(&this->m_vecSource, &vecTargetEntityPos);

	CWorld::pIgnoreEntity = 0;

	float v28;
	if (bLookRight)
	{
		v28 = (*this->m_pCamTargetEntity->GetMatrix()).right.z * boundBox.m_vecMin.x;
	}
	else
	{
		v28 = boundBox.m_vecMax.x * (*this->m_pCamTargetEntity->GetMatrix()).right.z;
	}

	float v74 = vecTargetEntityPos.z + v28;


	float v31 = boundBox.m_vecMax.z * (*this->m_pCamTargetEntity->GetMatrix()).at.z + v74;

	float v32 = ClampMin(v31, this->m_vecTargetCoorsForFudgeInter.z);

	if (sourceZ >= v32 + 0.1)
	{
		v31 = ClampMin(v31, this->m_vecTargetCoorsForFudgeInter.z);

		this->m_vecSource.z = ClampMin(v31 + 0.1f, this->m_vecSource.z);
	}
	else
	{
		this->m_vecSource.z = ClampMin(sourceZ, this->m_vecSource.z);
	}

	this->m_vecFront = this->m_pCamTargetEntity->GetPosition() - this->m_vecSource;

	this->m_vecFront.z = this->m_vecFront.z + 1.1;
	if (this->m_nMode == MODE_BEHINDBOAT)
		this->m_vecFront.z += 1.2;

	this->GetVectorsReadyForRW();

	return true;
}

void CCam::KeepTrackOfTheSpeed(
	CVector const & aSource,
	CVector const & aTargetCoorsForFudgeInter,
	CVector const & aUp,
	float const & aTrueAlpha,
	float const & aTrueBeta,
	float const & aFov)
{
	/* history of previous frame */
	static CVector CCam_vecSource = aSource;
	static CVector CCam_TargetCoorsForFudgeInter = aTargetCoorsForFudgeInter;
	static CVector CCam_vecUp = aUp;
	static float CCam_TrueBeta = aTrueBeta;
	static float CCam_TrueAlpha = aTrueAlpha;
	static float CCam_FOV = aFov;

	if (TheCamera.m_bJust_Switched)
	{
		CCam_vecSource = aSource;
		CCam_TargetCoorsForFudgeInter = aTargetCoorsForFudgeInter;
		CCam_vecUp = aUp;
	}
	this->m_vecSourceSpeedOverOneFrame = aSource - CCam_vecSource;
	this->m_vecTargetSpeedOverOneFrame = aTargetCoorsForFudgeInter - CCam_TargetCoorsForFudgeInter;
	this->m_vecUpOverOneFrame = aUp - CCam_vecUp;

	this->m_fFovSpeedOverOneFrame = aFov - CCam_FOV;
	this->m_fBetaSpeedOverOneFrame = aTrueBeta - CCam_TrueBeta;
	MakeAngleLessThan180(this->m_fBetaSpeedOverOneFrame);
	this->m_fAlphaSpeedOverOneFrame = aTrueAlpha - CCam_TrueAlpha;
	MakeAngleLessThan180(this->m_fAlphaSpeedOverOneFrame);
	CCam_vecSource = aSource;
	CCam_TargetCoorsForFudgeInter = aTargetCoorsForFudgeInter;
	CCam_vecUp = aUp;
	CCam_TrueBeta = aTrueBeta;
	CCam_TrueAlpha = aTrueAlpha;
	CCam_FOV = aFov;
}

void CCam::Process(void)
{
	//gIdleCam.IdleCamGeneralProcess();

	if (!this->m_pCamTargetEntity) {
		this->m_pCamTargetEntity = TheCamera.m_pTargetEntity;
		this->m_pCamTargetEntity->RegisterReference(&this->m_pCamTargetEntity);
	}

	float a4 = 0.0;
	float a2a = 0.0; // angle
#if 0
	if (gCrossHair[0].field_28 == 1)
	{
		v6 = FindPlayerPed(-1);
		if (!v6 || (v7 = &v6->pCurrentObjective->__parent.__parent) == 0 || v7->m_nModelIndex != 520)
			CWeaponEffects::ClearCrossHairImmediately(0);
	}
#endif
	this->m_nFrameNumWereAt++;
	if (this->m_nFrameNumWereAt > this->m_nDoCollisionCheckEveryNumOfFrames)
		this->m_nFrameNumWereAt = 1;

	this->m_bCollisionChecksOn = this->m_nFrameNumWereAt == this->m_nDoCollisionChecksOnFrameNum;

	CVector TargetEntityPos;

	if (this->m_bCamLookingAtVector)
	{
		TargetEntityPos = this->m_vecCamFixedModeVector;
	}
	else
	{
		if (this->m_pCamTargetEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE)
		{
			TargetEntityPos = this->m_pCamTargetEntity->GetPosition();

			if (this->m_pCamTargetEntity->GetMatrix()->up.x != 0.0
				|| this->m_pCamTargetEntity->GetMatrix()->up.y != 0.0)
			{
				a2a = CGeneral::GetATanOfXY(
					this->m_pCamTargetEntity->GetMatrix()->up.x,
					this->m_pCamTargetEntity->GetMatrix()->up.y);
			}
			else
			{
				a2a = 0.0;
			}

			CVector v99 = CVector(0, 0, 0);

			v99.x = this->m_pCamTargetEntity->GetMatrix()->up.x;
			v99.y = this->m_pCamTargetEntity->GetMatrix()->up.y;

			v99.Normalise();

			float v18 = v99.Magnitude2D();

			if (v18 != 0.0)
			{
				v99.x /= v18;
				v99.y /= v18;
			}

			CPhysical * pPhysical = (CPhysical *)this->m_pCamTargetEntity;
			const CVector & vecVelocity = pPhysical->m_vecMoveSpeed;

			float vx = v99.x * vecVelocity.x;
			float vy = v99.y * vecVelocity.y;
			float v20 = sqrt(vx * vx + vy * vy);

			if ((v20 / 1.8) <= 0.5)
			{
				float v27 = v20 / 1.8;
				if (v20 / 1.8 > 0.5)
					v27 = 0.5;
				a4 = -v27;
			}
			else
			{
				a4 = v20 / 0.9;
				if (v20 / 0.9 > 1.0)
				{
					a4 = 1.0;
				}
			}

			this->m_fSpeedVar = a4 * 0.105f + this->m_fSpeedVar * 0.895f;
			if (this->m_nDirectionWasLooking != CAMERA_DIR_LOOK_FORWARD)
			{
				if (false == CPad::GetPad(0)->GetLookBehindForCar()
					|| CPad::GetPad(0)->GetLookLeft()
					|| CPad::GetPad(0)->GetLookRight())
				{
					TheCamera.m_bCamDirectlyInFront = 1;
				}
			}
		}
		else
		{
			if (this->m_pCamTargetEntity != FindPlayerPed())
			{
				TargetEntityPos = this->m_pCamTargetEntity->GetPosition();
			}
			else
			{
				CVector v99 = FindPlayerPed()->GetPosition();

				if (FindPlayerPed()->m_pIntelligence->GetTaskClimb())
				{
					FindPlayerPed()->m_pIntelligence->GetTaskClimb()->GetCameraTargetPos(FindPlayerPed(-1), v99);
				}
#if 0
				if (//!(v40 | v41)|| CHECK
					CTimer::ms_fTimeStep < 0.2
					|| this->Using3rdPersonMouseCam()
					|| TheCamera.m_bCamDirectlyInFront
					|| TheCamera.m_bCameraJustRestored)
				{
					v45 = v99.fX;
					v44 = v99.fY;
					v43 = v99.fZ;
					v100.fX = 0.0;
					v52 = 0;
					v100.fY = 0.0;
					v51 = 0.0;
					v100.fZ = 0.0;
					v1 = 0.0;
				}
				else
				{
					v42 = FindPlayerPed(-1);
					if (CPedIntelligence::GetTaskFighting(v42->pPedIntelligence) && this->Mode == 53)
					{
						v46 = pow(PLAYERFIGHT_LEVEL_SMOOTHING_CONST, CTimer::ms_fTimeStep);
						v47 = 1.0 - v46;
						v48 = scaleVector3((RwV3D *)&v101, v47, &v99);
						v49 = scaleVector3((RwV3D *)&v100, v46, (CVector *)&stru_8CCC3C);
						v50 = (RwV3D *)vectorAdd(&v102, (CVector *)v49, (CVector *)v48);
						v45 = v50->x;
						v44 = v50->y;
						v43 = v50->z;
						v100.fX = 0.0;
						v52 = 0;
						v100.fY = 0.0;
						v51 = 0.0;
						v100.fZ = 0.0;
						v1 = 0.0;
					}
					else
					{
						v53 = 1.0 - pow(PLAYERPED_LEVEL_SMOOTHING_CONST_INV, CTimer::ms_fTimeStep);
						v54 = CTimer::ms_fTimeStep;
						v55 = 1.0 - pow(PLAYERPED_TREND_SMOOTHING_CONST_INV, CTimer::ms_fTimeStep);
						v56 = scaleVector((RwV3D *)&v102, (RwV3D *)&dword_B6EC7C, CTimer::ms_fTimeStep);
						v57 = (CVector *)vectorAdd(&v101, (CVector *)&stru_8CCC3C, (CVector *)v56);
						v58 = 1.0 - v53;
						v59 = scaleVector3((RwV3D *)&v103, v58, v57);
						v60 = scaleVector3((RwV3D *)&v104, v53, &v99);
						v61 = (RwV3D *)vectorAdd(&v105, (CVector *)v60, (CVector *)v59);
						v45 = v61->x;
						v44 = v61->y;
						v62 = v61->z;
						v100.fX = v61->x;
						v100.fY = v44;
						v101.fX = v100.fX - stru_8CCC3C.matrix.right.x;
						v100.fZ = v99.fZ;
						v101.fY = v44 - stru_8CCC3C.matrix.right.y;
						v101.fZ = v99.fZ - stru_8CCC3C.matrix.right.z;
						v63 = 1.0 - v55;
						v64 = scaleVector3((RwV3D *)&v105, v63, (CVector *)&dword_B6EC7C);
						v65 = max(1.0, v54);
						v66 = scaleVector3((RwV3D *)&v104, v55, &v101);
						v67 = (CVector *)scaleVector((RwV3D *)&v103, v66, v65);
						v68 = (RwV3D *)vectorAdd((CVector *)&v106, v67, (CVector *)v64);
						v52 = LODWORD(v68->x);
						v51 = v68->y;
						v69 = v68->z;
						v43 = v100.fZ;
						v101.fZ = 0.0;
						v1 = 0.0;
					}
				}
				TargetEntityPos.fX = v45;
				TargetEntityPos.fY = v44;
				stru_8CCC3C.matrix.right.x = v45;
				stru_8CCC3C.matrix.right.y = v44;
				stru_8CCC3C.matrix.right.z = v43;
				dword_B6EC7C = v52;
				dword_B6EC80 = LODWORD(v51);
				dword_B6EC84 = 0;
#endif
			}

			if (this->m_pCamTargetEntity->GetMatrix()->up.x != 0.0 ||
				this->m_pCamTargetEntity->GetMatrix()->up.y != 0.0)
			{
				a2a = CGeneral::GetATanOfXY(this->m_pCamTargetEntity->GetMatrix()->up.x,
					this->m_pCamTargetEntity->GetMatrix()->up.y);
			}
			else
			{
				a2a = 0.0;
			}

			a4 = 0.0;
			this->m_fSpeedVar = 0.0;
		}
	}

	this->m_nDirectionWasLooking = gCameraDirection;
	gCameraDirection = CAMERA_DIR_LOOK_FORWARD;
	
	float fCamDist;

	if (&TheCamera.m_aCams[TheCamera.m_nActiveCam] == this)
	{
		if (this->m_nMode != MODE_CAM_ON_A_STRING
			&& this->m_nMode != MODE_1STPERSON
			&& this->m_nMode != MODE_BEHINDBOAT
			&& this->m_nMode != MODE_BEHINDCAR
			|| this->m_pCamTargetEntity->_GetEntityType() != ENTITY_TYPE_VEHICLE)
		{
			if (this->m_nMode != MODE_FOLLOWPED || this->m_pCamTargetEntity->_GetEntityType() != ENTITY_TYPE_PED)
			{
				if (this->m_nMode == MODE_AIMWEAPON)
				{
					gCameraDirection = CAMERA_DIR_LOOK_FORWARD;
					if (this->m_nDirectionWasLooking != CAMERA_DIR_LOOK_FORWARD)
						fCamDist = 1.0;
				}
			}
			else
			{
				if (CPad::GetPad(0)->GetLookBehindForPed())
				{
					if (this->m_nDirectionWasLooking == CAMERA_DIR_LOOK_FORWARD)
					{
						gCameraDirection = CAMERA_DIR_LOOK_FORWARD;
					}
					else
					{
						if (TheCamera.m_bTransitionState != 0)//CHECK
							TheCamera.m_bLookingAtPlayer = 1;
						gCameraDirection = CAMERA_DIR_LOOK_FORWARD;
					}
				}
				else
				{
					gCameraDirection = CAMERA_DIR_LOOK_FORWARD;
					if (this->m_nDirectionWasLooking != CAMERA_DIR_LOOK_FORWARD)
						fCamDist = 1.0;
				}
			}
		}
		else
		{
			CVehicle * pVehicle = (CVehicle *)this->m_pCamTargetEntity;
			bool bForceSet = pVehicle
				&& (pVehicle->GetVehicleAppearance() == 3
					|| pVehicle->GetVehicleAppearance() == 5);

			if (CPad::GetPad(0)->GetLookBehindForCar())
			{
				TheCamera.m_bTransitionState = 0;
				TheCamera.m_bDoingSpecialInterPolation = 0;
				TheCamera.m_bItsOkToLookJustAtThePlayer = 0;
				if (this->m_nDirectionWasLooking != CAMERA_DIR_LOOK_BEHIND)
					TheCamera.m_bLookingAtPlayer = 1;
				gCameraDirection = CAMERA_DIR_LOOK_BEHIND;
			}
			else
			{
				if (!CPad::GetPad(0)->GetLookLeft() || bForceSet)
				{
					if (!CPad::GetPad(0)->GetLookRight() || bForceSet)
					{
						gCameraDirection = CAMERA_DIR_LOOK_FORWARD;
					}
					else
					{
						TheCamera.m_bTransitionState = 0;
						TheCamera.m_bDoingSpecialInterPolation = 0;
						TheCamera.m_bItsOkToLookJustAtThePlayer = 0;
						gCameraDirection = CAMERA_DIR_LOOK_RIGHT;
					}
				}
				else
				{
					TheCamera.m_bTransitionState = 0;
					TheCamera.m_bDoingSpecialInterPolation = 0;
					TheCamera.m_bItsOkToLookJustAtThePlayer = 0;
					gCameraDirection = CAMERA_DIR_LOOK_LEFT;
				}
				if (this->m_nDirectionWasLooking != gCameraDirection)
					TheCamera.m_bLookingAtPlayer = 1;
			}
		}

	}

	if (TheCamera.m_bLookingAtPlayer)
	{
		fCamDist = 1.0;
		TheCamera.m_bResetOldMatrix = 1;
	}
#if 0
	if (this->Mode != MODE_BEHINDCAR
		&& this->Mode != MODE_CAM_ON_A_STRING
		&& this->Mode != MODE_BEHINDBOAT
		&& this->Mode != MODE_1STPERSON
		&& this->Mode != MODE_TWOPLAYER_IN_CAR_AND_SHOOTING)
		CPostEffects::m_bSpeedFXUserFlagCurrentFrame = 0;
#endif

	gbFirstPersonRunThisFrame = 0;

	switch (this->m_nMode)
	{
	case MODE_BEHINDCAR:
	case MODE_CAM_ON_A_STRING:
	case MODE_BEHINDBOAT:
		CCam::Process_FollowCar_SA(
			TargetEntityPos,
			a2a,//angle
			this->m_fSpeedVar,
			a4,//entity speed?
			0);
		break;
	case MODE_FOLLOWPED:
		if (!CCamera::m_bUseMouse3rdPerson || gbForceFollowPedMouse)
			this->Process_FollowPed_SA(TargetEntityPos, a2a, this->m_fSpeedVar, a4, false);
		else
			this->Process_FollowPedWithMouse(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_SNIPER:
	case MODE_M16_1STPERSON:
	case MODE_HELICANNON_1STPERSON:
	case MODE_CAMERA:
		this->Process_M16_1stPerson(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_ROCKETLAUNCHER:
		this->Process_Rocket(TargetEntityPos, a2a, this->m_fSpeedVar, a4, 0);
		break;
	case MODE_WHEELCAM:
		this->Process_WheelCam(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_FIXED:
		this->Process_Fixed(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_1STPERSON:
		this->Process_1stPerson(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_FLYBY:
		this->Process_FlyBy(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_PED_DEAD_BABY:
		this->ProcessPedsDeadBaby();
		TheCamera.m_bPlayerWasOnBike = 0;
		TheCamera.m_bJustInitalised = 0;
		break;
	case MODE_ARRESTCAM_ONE:
		this->ProcessArrestCamOne();
		break;
	case MODE_ARRESTCAM_TWO:
		break;
	case MODE_SPECIAL_FIXED_FOR_SYPHON:
		this->Process_SpecialFixedForSyphon(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_SNIPER_RUNABOUT:
	case MODE_ROCKETLAUNCHER_RUNABOUT:
	case MODE_1STPERSON_RUNABOUT:
	case MODE_FIGHT_CAM_RUNABOUT:
	case MODE_M16_1STPERSON_RUNABOUT:
	case MODE_ROCKETLAUNCHER_RUNABOUT_HS:
		this->Process_1rstPersonPedOnPC(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_EDITOR:
		this->Process_Editor(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_ATTACHCAM:
		this->Process_AttachedCam();
		break;
	case MODE_TWOPLAYER:
		this->Process_Cam_TwoPlayer();
		break;
	case MODE_TWOPLAYER_IN_CAR_AND_SHOOTING:
		this->Process_Cam_TwoPlayer_InCarAndShooting();
		break;
	case MODE_TWOPLAYER_SEPARATE_CARS:
		this->Process_Cam_TwoPlayer_Separate_Cars();
		break;
	case MODE_ROCKETLAUNCHER_HS:
		this->Process_Rocket(TargetEntityPos, a2a, this->m_fSpeedVar, a4, 1);
		break;
	case MODE_AIMWEAPON:
	case MODE_AIMWEAPON_FROMCAR:
	case MODE_AIMWEAPON_ATTACHED:
		this->Process_AimWeapon(TargetEntityPos, a2a, this->m_fSpeedVar, a4);
		break;
	case MODE_TWOPLAYER_SEPARATE_CARS_TOPDOWN:
		this->Process_Cam_TwoPlayer_Separate_Cars_TopDown();
		break;
	case MODE_DW_HELI_CHASE:
		this->Process_DW_HeliChaseCam(0);
		break;
	case MODE_DW_CAM_MAN:
		this->Process_DW_CamManCam(0);
		break;
	case MODE_DW_BIRDY:
		this->Process_DW_BirdyCam(0);
		break;
	case MODE_DW_PLANE_SPOTTER:
		this->Process_DW_PlaneSpotterCam(0);
		break;
	case MODE_DW_DOG_FIGHT:
	case MODE_DW_FISH:
		TheCamera.m_bStartInterScript = 0;
		break;
	case MODE_DW_PLANECAM1:
		this->Process_DW_PlaneCam1(0);
		break;
	case MODE_DW_PLANECAM2:
		this->Process_DW_PlaneCam2(0);
		break;
	case MODE_DW_PLANECAM3:
		this->Process_DW_PlaneCam3(0);
		break;
	default:
		this->m_vecSource = CVector(0, 0, 0);
		this->m_vecFront = CVector(0, 1, 0);
		this->m_vecUp = CVector(0, 0, 1);
#if 0
		v100.fX = 0.0;
		v100.fY = 0.0;
		v100.fZ = 1.0;
#endif
		break;
	}

	if (this->m_nMode < MODE_DW_HELI_CHASE || this->m_nMode > MODE_DW_PLANECAM3)
		gDWLastModeForCineyCam = -1;

	gLastCamMode = this->m_nMode;

	CVector v99 = this->m_vecSource - this->m_vecTargetCoorsForFudgeInter;

	this->m_fTrueBeta = CGeneral::GetATanOfXY(v99.x, v99.y);

	float v99Mag = v99.Magnitude2D();

	this->m_fTrueAlpha = CGeneral::GetATanOfXY(v99Mag, v99.z);

	if (TheCamera.m_bTransitionState == 0)
		this->KeepTrackOfTheSpeed(
			this->m_vecSource,
			this->m_vecTargetCoorsForFudgeInter,
			this->m_vecUp,
			this->m_fTrueAlpha,
			this->m_fTrueBeta,
			this->m_fFOV);

	this->m_bLookingRight = 0;
	this->m_bLookingLeft = 0;
	this->m_bLookingBehind = 0;
	this->m_vecSourceBeforeLookBehind = this->m_vecSource;

	if (&TheCamera.m_aCams[TheCamera.m_nActiveCam] == this)
	{
		switch (gCameraDirection)
		{
		case CAMERA_DIR_LOOK_BEHIND:
			this->LookBehind();
			break;
		case CAMERA_DIR_LOOK_LEFT:
			this->LookLeft();
			break;
		case CAMERA_DIR_LOOK_RIGHT:
			this->LookRight(true);
			break;
		}

		this->m_nDirectionWasLooking = gCameraDirection;
	}

	if (TheCamera.m_bFOVLerpProcessed)
	{
		this->m_fFOV = TheCamera.m_fFOVNew;
		TheCamera.m_bFOVLerpProcessed = 0;
	}
	if (TheCamera.m_bVecMoveLinearProcessed)
	{
		this->m_vecSource = TheCamera.m_vecMoveLinear;
		TheCamera.m_bVecMoveLinearProcessed = 0;
	}
	if (TheCamera.m_bVecTrackLinearProcessed)
	{
		this->m_vecFront = TheCamera.m_vecTrackLinear - this->m_vecSource;
		this->m_vecFront.Normalise();

		this->GetVectorsReadyForRW();

		TheCamera.m_bVecTrackLinearProcessed = 0;
	}
}

void CCam::Process_SpecialFixedForSyphon(CVector const & a2, float a3, float a4, float a5)
{
	this->m_vecSource = this->m_vecCamFixedModeSource;

	this->m_vecTargetCoorsForFudgeInter = a2;
	this->m_vecTargetCoorsForFudgeInter.z += this->m_fSyphonModeTargetZOffSet;

	this->m_vecFront = a2 - this->m_vecSource;

	CVector v25 = this->m_vecCamFixedModeSource;

	TheCamera.AvoidTheGeometry(&v25, &this->m_vecTargetCoorsForFudgeInter, &this->m_vecSource, this->m_fFOV);

	this->m_vecFront.z += this->m_fSyphonModeTargetZOffSet;
	this->GetVectorsReadyForRW();

	this->m_vecUp += this->m_vecCamFixedModeUpOffSet;
	this->m_vecUp.Normalise();

	CVector vecFront = CrossProduct(this->m_vecUp, this->m_vecFront);
	vecFront.Normalise();

	this->m_vecFront = CrossProduct(vecFront, this->m_vecUp);

	this->m_vecFront.Normalise();
	this->m_fFOV = 70.0f;
	CEntity * pEntity = this->m_pCamTargetEntity;
	if (!pEntity)
	{
		return;
	}
	if (pEntity->_GetEntityType() != ENTITY_TYPE_PED)
	{
		return;
	}
	CPed * pPed = (CPed *)pEntity;
	if (!pPed->m_pTargetedObject)
		return;
	/*pPed->m_aWeapons[pPed->m_nActiveWeaponSlot]
				v16 = v15->WeaponSlots[v15->m_bActiveWeapon].m_eWeaponType;
				v17 = CPed::GetWeaponSkill(v15);
				v18 = (CWeaponInfo *)CWeaponInfo::GetWeaponInfo(v16, v17);*/
	CWeaponInfo * pWeaponInfo = nullptr;
	if (!pWeaponInfo)
	{
		return;
	}
	if (pWeaponInfo->m_nFlags.bAimWithArm && pPed->bIsDucking == false)
	{
		return;
	}
	if (pWeaponInfo->m_nWeaponFire == WEAPON_FIRE_MELEE)
		return;

	CVector vecEntityToTarget = pPed->m_pTargetedObject->GetPosition() - pPed->GetPosition();

	pPed->m_fAimingRotation = atan2(-vecEntityToTarget.x, vecEntityToTarget.y);
	pPed->m_fCurrentRotation = pPed->m_fAimingRotation;
	pPed->SetHeading(pPed->m_fAimingRotation);
	pPed->UpdateRW();
}

bool CCam::GetBoatLook_L_R_HeightOffset(float & fOffset)
{
	if (!this->m_pCamTargetEntity)
		return false;

	CVehicleModelInfo * pVehicleModelInfo = (CVehicleModelInfo *)CModelInfo::ms_modelInfoPtrs[this->m_pCamTargetEntity->m_nModelIndex];

	tBoatHandlingData * pBoatHandling = mod_HandlingManager.GetBoatPointer(pVehicleModelInfo->m_nHandlingId);
	
	if (!pBoatHandling)
		return false;
	
	fOffset = pBoatHandling->m_fLookLRBehindCamHeight;
	
	return true;
}

bool CCam::Using3rdPersonMouseCam()
{
	return CCamera::m_bUseMouse3rdPerson && this->m_nMode == MODE_FOLLOWPED;
}

void CCam::RotCamIfInFrontCar(CVector const&, float)
{

}

void CCam::LookBehind()
{

}

void CCam::CacheLastSettingsDWCineyCam()
{

}

void CCam::DoAverageOnVector(CVector const&)
{}

void CCam::DoCamBump(float, float)
{}

void CCam::Finalise_DW_CineyCams(CVector*, CVector*, float, float, float, float)
{}

void CCam::GetCoreDataForDWCineyCamMode(CEntity**, CVehicle**,
	CVector*, CVector*, CVector*, CVector*, CVector*, CVector*, float*, CVector*, float*, class CColSphere*)
{}

void CCam::GetLookAlongGroundPos(CEntity*, CPed*, CVector&, CVector&)
{}

void CCam::GetLookFromLampPostPos(CEntity*, CPed*, CVector&, CVector&)
{}

void CCam::GetLookOverShoulderPos(CEntity*, CPed*, CVector&, CVector&)
{}

bool CCam::GetWeaponFirstPersonOn()
{
	CPed * pPed = (CPed *)this->m_pCamTargetEntity;
	
	if (!pPed)
		return false;

	if (pPed->_GetEntityType() != ENTITY_TYPE_PED)
		return false;

	return pPed->m_aWeapons[pPed->m_nActiveWeaponSlot].bWeaponFirstPerson;
}

void CCam::Get_TwoPlayer_AimVector(CVector&)
{

}

void CCam::Init()
{

}

bool CCam::IsTargetInWater(CVector const&)
{
	return false;
}

bool CCam::IsTimeToExitThisDWCineyCamMode(int camId, CVector *vecEnd, CVector *vecStart, float, bool bCheckLineOfSight)
{
	int camIdToArrayId = camId - 20;

	if (gbExitCam[camIdToArrayId])
		return 1;

	CVector vecRay = *vecStart - *vecEnd;
	float fDistance = vecRay.Magnitude();

	bool bInRange = fDistance >= gMovieCamMinDist[camIdToArrayId] && fDistance <= gMovieCamMaxDist[camIdToArrayId];
	bool bSightLineClear = true;

	if (bCheckLineOfSight)
	{
		CWorld::pIgnoreEntity = this->m_pCamTargetEntity;
		CColPoint colPoint;
		CEntity * pEntity = nullptr;
		bSightLineClear = CWorld::ProcessLineOfSight(*vecStart, *vecEnd,
			colPoint, pEntity,
			1, 1,
			0, 0,
			0, 0,
			0, 0);

		CWorld::pIgnoreEntity = 0;
	}
	return camId >= 20 && camId <= 28
		&& 
		(!bInRange 
			|| bSightLineClear
			|| CTimer::m_snTimeInMilliseconds > gDWCineyCamEndTime);
}

void CCam::ProcessArrestCamOne(){}
void CCam::ProcessArrestCamTwo(){}
void CCam::ProcessDWBustedCam1(CPed*, bool){}
void CCam::ProcessPedsDeadBaby(){}
void CCam::Process_1rstPersonPedOnPC(CVector const&, float, float, float){}
void CCam::Process_1stPerson(CVector const&, float, float, float){}
void CCam::Process_AimWeapon(CVector const&, float, float, float){}
void CCam::Process_AttachedCam(){}
void CCam::Process_BehindCar(CVector const&, float, float, float){}
void CCam::Process_Cam_TwoPlayer(){}
void CCam::Process_Cam_TwoPlayer_CalcSource(float, CVector*, CVector*, CVector*){}
void CCam::Process_Cam_TwoPlayer_InCarAndShooting(){}
void CCam::Process_Cam_TwoPlayer_Separate_Cars(){}
void CCam::Process_Cam_TwoPlayer_Separate_Cars_TopDown(){}
void CCam::Process_Cam_TwoPlayer_TestLOSs(CVector){}
void CCam::Process_DW_BirdyCam(bool){}
void CCam::Process_DW_CamManCam(bool){}
void CCam::Process_DW_DogFightCam(bool){}
void CCam::Process_DW_FishCam(bool){}
void CCam::Process_DW_HeliChaseCam(bool){}
void CCam::Process_DW_PlaneCam1(bool){}
void CCam::Process_DW_PlaneCam2(bool){}
void CCam::Process_DW_PlaneCam3(bool){}
void CCam::Process_DW_PlaneSpotterCam(bool){}
void CCam::Process_Editor(CVector const&, float, float, float){}
void CCam::Process_Fixed(CVector const&, float, float, float){}
void CCam::Process_FlyBy(CVector const&, float, float, float){}
void CCam::Process_FollowCar_SA(CVector const&, float, float, float, bool){}
void CCam::Process_FollowPed(CVector const&, float, float, float){}
void CCam::Process_M16_1stPerson(CVector const&, float, float, float){}
void CCam::Process_Rocket(CVector const&, float, float, float, bool)
{

}

void CCam::Process_WheelCam(CVector const&, float, float, float){}