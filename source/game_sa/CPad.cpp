#include "CPad.h"

CMouseControllerState &CPad::NewMouseControllerState = *reinterpret_cast<CMouseControllerState *>(0xB73418);

signed short CPad::LookAroundLeftRight()
{
	return plugin::CallMethodAndReturn<signed short, 0x540E80, CPad *>(this);
}

signed short CPad::LookAroundUpDown()
{
	return plugin::CallMethodAndReturn<signed short, 0x540F80, CPad *>(this);
}

CPad* CPad::GetPad(int padNumber)
{
	return plugin::CallAndReturn<CPad *, 0x53FB70, int>(padNumber);
}

void CPad::ClearMouseHistory()
{
	plugin::Call<0x541BD0>();
}

signed short CPad::AimWeaponLeftRight(CPed * pPed)
{
	return plugin::CallMethodAndReturn<signed short, 0x541040, CPad *, CPed*>(this, pPed);
}

signed short CPad::AimWeaponUpDown(CPed * pPed)
{
	return plugin::CallMethodAndReturn<signed short, 0x5410C0, CPad *, CPed*>(this, pPed);
}

bool CPad::GetForceCameraBehindPlayer()
{
	return plugin::CallMethodAndReturn<bool, 0x540AE0, CPad *>(this);
}

short CPad::GetPedWalkLeftRight()
{
	return plugin::CallMethodAndReturn<short, 0x540DC0, CPad *>(this);
}

short CPad::GetPedWalkUpDown()
{
	return plugin::CallMethodAndReturn<short, 0x540E20, CPad *>(this);
}

bool CPad::GetLookLeft()
{
	return true;
}

bool CPad::GetLookRight()
{
	return true;
}

bool CPad::GetLookBehindForCar()
{
	return true;
}

bool CPad::GetLookBehindForPed()
{
	return true;
}