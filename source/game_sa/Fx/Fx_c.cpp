#include "StdInc.h"

Fx_c &g_fx = *reinterpret_cast<Fx_c *>(0xA9AE00);

// Converted from thiscall void Fx_c::AddSparks(CVector &origin, CVector &direction, float force, int amount, CVector across, uchar sparksType, float spread, float life) 0x49F040
void Fx_c::AddSparks(CVector& origin, CVector& direction, float force, int amount, CVector across, unsigned char sparksType, float spread, float life) {
	((void(__thiscall *)(Fx_c*, CVector&, CVector&, float, int, CVector, unsigned char, float, float))0x49F040)(this, origin, direction, force, amount, across, sparksType, spread, life);
}