#include "CSimpleTransform.h"
#include "CMatrix.h"

void CSimpleTransform::UpdateRwMatrix(RwMatrix * m)
{
	float fCos = cos(this->m_fHeading);
	float fSin = sin(this->m_fHeading);
	m->right.z = 0.0;
	m->up.z = 0.0;
	m->at.x = 0.0;
	m->at.y = 0.0;
	m->at.z = 1.0;
	m->right.x = fCos;
	m->right.y = fSin;
	m->up.x = -fSin;
	m->up.y = fCos;
	m->pos.x = this->m_vPosn.x;
	m->pos.y = this->m_vPosn.y;
	m->pos.z = this->m_vPosn.z;
	RwMatrixUpdate(m);
}

CVector CSimpleTransform::GetForward() const
{
	float fAngle = this->m_fHeading;

	return CVector(-sin(fAngle), cos(fAngle), 0.0f);
}

CVector CSimpleTransform::GetRight() const
{
	float fAngle = this->m_fHeading;
	return CVector(cos(fAngle), sin(fAngle), 0.0f);
}

CVector CSimpleTransform::GetAt() const
{
	return CVector(0.f, 0.f, 1.f);
}

void CSimpleTransform::UpdateMatrix(CMatrix * m)
{
	m->SetTranslate(this->m_vPosn.x, this->m_vPosn.y, this->m_vPosn.z);
	m->SetRotateZOnly(this->m_fHeading);
}