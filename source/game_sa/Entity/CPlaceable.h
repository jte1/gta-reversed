/*
    Plugin-SDK (Grand Theft Auto San Andreas) header file
    Authors: GTA Community. See more here
    https://github.com/DK22Pac/plugin-sdk
    Do not delete this comment block. Respect others' work!
*/
#pragma once
#include "PluginBase.h"
#include "CSimpleTransform.h"
#include "CMatrixLink.h"

class  CPlaceable {
protected:
    CPlaceable(plugin::dummy_func_t) {}
public:
    CSimpleTransform m_placement;
    CMatrixLink *m_matrix;

    virtual ~CPlaceable() {};
    
    CMatrix *GetMatrix();
    
    static void ShutdownMatrixArray();
    static void InitMatrixArray();
    
    CVector GetRightDirection();
    CVector GetTopDirection();
    CVector GetAtDirection();

	CVector GetForward() const;
    
    void FreeStaticMatrix();
    void SetPosn(float x, float y, float z);
    void SetPosn(CVector const& posn);
    void SetOrientation(float x, float y, float z);
    void GetOrientation(float& x, float& y, float& z);
    void SetHeading(float heading);
    float GetHeading();
    bool IsWithinArea(float x1, float y1, float x2, float y2);
    bool IsWithinArea(float x1, float y1, float z1, float x2, float y2, float z2);
    void RemoveMatrix();
    void AllocateStaticMatrix();
    void AllocateMatrix();
    void SetMatrix(CMatrix  const& matrix);
    
    inline CVector &GetPosition() {
        return m_matrix ? m_matrix->pos : m_placement.m_vPosn;
    }

	inline float CPlaceable::_GetHeading(void) const {
		if (this->m_matrix) {
			return atan2f(this->m_matrix->up.y, -this->m_matrix->up.x);
		}
		return this->m_placement.m_fHeading;
	}

	inline void _UpdateRwMatrix(RwMatrix * pMatrix) {
		if (this->m_matrix) {
			this->m_matrix->UpdateRwMatrix(pMatrix);
		}
		else {
			this->m_placement.UpdateRwMatrix(pMatrix);
		}
	}

	inline CMatrix & _GetMatrixNoUpdate()
	{
		return *m_matrix;
	}

	inline CVector & _GetPosNoUpdate()
	{
		return m_matrix->pos;
	}

	inline void _EnsureMatrixExists()
	{
		if (!m_matrix)
		{
			this->AllocateMatrix();
			this->m_placement.UpdateMatrix(this->m_matrix);
		}
	}
};

VALIDATE_SIZE(CPlaceable, 0x18);