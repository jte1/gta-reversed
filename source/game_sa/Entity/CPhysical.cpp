#include "StdInc.h"

float &DAMPING_LIMIT_OF_SPRING_FORCE = *reinterpret_cast<float *>(0x008CD7A4);
float &DAMPING_LIMIT_IN_FRAME = *reinterpret_cast<float *>(0x8CD7A0);
float &PHYSICAL_SHIFT_SPEED_DAMP = *reinterpret_cast<float *>(0x008CD788);

void CPhysical::UnsetIsInSafePosition(void)
{
	this->m_vecMoveSpeed *= -1.0f;
	this->m_vecTurnSpeed *= -1.0f;

	this->ApplySpeed();

	this->m_vecMoveSpeed *= -1.0f;
	this->m_vecTurnSpeed *= -1.0f;

	this->m_bIsInSafePosition = false;
}

void CPhysical::ApplyTurnForce(CVector vecForce, CVector vecPos)
{
	if (this->m_nPhysicalFlags.bDisableTurnForce)
	{
		return;
	}
	CVector vecOrigin = CVector(0.f, 0.f, 0.f);

	if (false == this->m_nPhysicalFlags.bInfiniteMass)
	{
		vecOrigin = Multiply3x3(*this->GetMatrix(), this->m_vecCentreOfMass);
	}
	if (this->m_nPhysicalFlags.bDisableMoveForce)
	{
		vecPos.z = 0.0;
		vecForce.z = 0.0;
	}
	CVector vecPositionVector = vecPos - vecOrigin;
	CVector vecTorque = CrossProduct(vecPositionVector, vecForce);
		
	m_vecTurnSpeed += vecTorque / this->m_fTurnMass;
}

void CPhysical::ApplyForce(CVector vecForce, CVector vecPos, bool bApplyRotation)
{
	if (this->m_nPhysicalFlags.bDisableZ)
		vecForce.z = 0.0;

	if (false == this->m_nPhysicalFlags.bDisableMoveForce 
	&& false == this->m_nPhysicalFlags.bInfiniteMass)
	{
		this->m_vecMoveSpeed += vecForce / this->m_fMass;
	}

	if (this->m_nPhysicalFlags.bDisableTurnForce || false == bApplyRotation)
	{
		return;
	}

	float fTurnMass = this->m_fTurnMass;
	CVector vecOrigin = CVector(0, 0, 0);

	if (this->m_nPhysicalFlags.bInfiniteMass)
	{
		fTurnMass += this->m_vecCentreOfMass.z * this->m_fMass * this->m_vecCentreOfMass.z * 0.5f;
	}
	else
	{
		vecOrigin = Multiply3x3(*this->GetMatrix(), this->m_vecCentreOfMass);
	}
	if (this->m_nPhysicalFlags.bDisableMoveForce)
	{
		vecOrigin.z = 0.0;
		vecForce.z = 0.0;
	}
	CVector vecPositionVector = vecPos - vecOrigin;
	CVector vecTorque = CrossProduct(vecPositionVector, vecForce);

	this->m_vecTurnSpeed += vecTorque / fTurnMass;
}

void CPhysical::ApplyMoveForce(CVector vecForce)
{
	if (this->m_nPhysicalFlags.bDisableMoveForce || this->m_nPhysicalFlags.bInfiniteMass)
		return;

	if (this->m_nPhysicalFlags.bDisableZ)
		vecForce.z = 0.f;

	this->m_vecMoveSpeed += vecForce / this->m_fMass;
}

void CPhysical::ApplyGravity()
{
	if (false == this->m_nPhysicalFlags.bApplyGravity)
		return;

	if (this->m_nPhysicalFlags.bDisableMoveForce)
		return;

	if (this->m_nPhysicalFlags.bInfiniteMass)
	{
		CVector vecGravity(0.f, 0.f, CTimer::ms_fTimeStep * this->m_fMass * -0.008f);
		CVector vecPos = Multiply3x3(*this->GetMatrix(), this->m_vecCentreOfMass);

		this->ApplyForce(vecGravity, vecPos, true);
	}
	else if (this->m_bUsesCollision)
	{
		this->m_vecMoveSpeed.z -= CTimer::ms_fTimeStep * 0.008f;
	}
}

void CPhysical::ApplyMoveSpeed()
{
	if (this->m_nPhysicalFlags.bDontApplySpeed || this->m_nPhysicalFlags.bDisableMoveForce)
	{
		this->m_vecMoveSpeed = CVector(0, 0, 0);
	}
	else
	{
		(*this->GetMatrix()).pos += CTimer::ms_fTimeStep * this->m_vecMoveSpeed;
	}
}

void CPhysical::ApplyTurnSpeed()
{
	if (this->m_nPhysicalFlags.bDontApplySpeed)
	{
		this->m_vecTurnSpeed = CVector(0, 0, 0);
		return;
	}

	CVector dTurnSpeed = CTimer::ms_fTimeStep * this->m_vecTurnSpeed;//a

	this->GetMatrix()->right += CrossProduct(dTurnSpeed, this->GetMatrix()->right);
	this->GetMatrix()->up += CrossProduct(dTurnSpeed, this->GetMatrix()->up);
	this->GetMatrix()->at += CrossProduct(dTurnSpeed, this->GetMatrix()->at);

	if (this->m_nPhysicalFlags.bDisableMoveForce)
		return;
	
	if (this->m_nPhysicalFlags.bInfiniteMass)
		return;

	CVector vecMass = -this->m_vecCentreOfMass;
	CVector vecOrigin = Multiply3x3(*this->GetMatrix(), vecMass);

	this->GetMatrix()->pos += CrossProduct(dTurnSpeed, vecOrigin);
}

CVector CPhysical::GetSpeed(CVector vecPos)
{
	CVector vecOrigin(0, 0, 0);

	if (false == this->m_nPhysicalFlags.bInfiniteMass)
	{
		vecOrigin = Multiply3x3(*this->GetMatrix(), this->m_vecCentreOfMass);
	}
	CVector vecPositionVector = vecPos - vecOrigin;

	CVector vecTotalTurnSpeed = this->m_vecTurnSpeed + this->m_vecFrictionTurnSpeed;

	CVector vecTurnSpeed = CrossProduct(vecTotalTurnSpeed, vecPositionVector);
	
	return vecTurnSpeed + this->m_vecMoveSpeed + this->m_vecFrictionMoveSpeed;
}

void CPhysical::ApplyFrictionMoveForce(CVector vecForce)
{
	if (this->m_nPhysicalFlags.bDisableMoveForce)
		return;

	if (this->m_nPhysicalFlags.bInfiniteMass)
		return;

	if (this->m_nPhysicalFlags.bDisableZ)
		vecForce.z = 0.0f;
		
	this->m_vecFrictionMoveSpeed += vecForce / this->m_fMass;
}

void CPhysical::ProcessEntityCollision(CEntity * pEntity, CColPoint * pColPoint)
{
	CPhysical * pPhysical = (CPhysical *)pEntity;

	pEntity->_EnsureMatrixExists();

	CColModel * pOurColModel = CModelInfo::ms_modelInfoPtrs[this->m_nModelIndex]->m_pColModel;

	CColModel * pOtherColModel = pEntity->GetColModel();

	CColPoint colPoint;

	int nNumCollisions = CCollision::ProcessColModels(
		*this->GetMatrix(), *pOurColModel, 
		*pEntity->GetMatrix(), *pOtherColModel, 
		&colPoint, 0, 0, 0);

	if (nNumCollisions > 0)
	{
		this->AddCollisionRecord(pPhysical);

		if (pPhysical->_GetEntityType() != ENTITY_TYPE_BUILDING)
			pPhysical->AddCollisionRecord(this);

		if (pPhysical->_GetEntityType() == ENTITY_TYPE_BUILDING
			|| pPhysical->m_bHasContacted
			|| pPhysical->m_bIsStaticWaitingForCollision)
			this->m_bHasHitWall = true;
	}
}

bool CPhysical::ApplyFriction(float fFriction, CColPoint & colPoint)
{
	if (this->m_nPhysicalFlags.bDisableCollisionForce)
		return false;

	if (this->m_nPhysicalFlags.bDisableTurnForce)
	{
		float dot = DotProduct(this->m_vecTurnSpeed, colPoint.m_vecNormal);
		CVector vec = dot * colPoint.m_vecNormal;

		CVector ray = this->m_vecTurnSpeed - vec;
		
		float a3a = ray.Magnitude();
		
		if (a3a <= 0.0f)
		{
			return false;
		}

		float a2a = -(CTimer::ms_fTimeStep / this->m_fMass * fFriction);

		float v13 = ClampMin(-a3a, a2a);

		CVector vecChange(ray.x / a3a * v13, ray.y / a3a * v13, 0.f);

		this->m_vecFrictionMoveSpeed += vecChange;

		return true;
	}

	CVector vecPos = colPoint.m_vecPoint - this->_GetMatrixNoUpdate().pos;//v35,v18,b

	CVector vecSpeed = this->GetSpeed(vecPos);
	float v19 = DotProduct(vecSpeed, colPoint.m_vecNormal);
	CVector vecSpeedProj = v19 * colPoint.m_vecNormal;

	CVector vecD = vecSpeed - vecSpeedProj;
	float retaddr = vecD.Magnitude();

	if (retaddr <= 0.0)
		return false;

	CVector vecDNormalized = vecD / retaddr;
	
	CVector vecOrigin = Multiply3x3(this->_GetMatrixNoUpdate(), this->m_vecCentreOfMass);
	
	CVector vecOffset = vecPos - vecOrigin;

	CVector v26 = CrossProduct(vecOffset, vecDNormalized);

	float v27 = -(1.0 / (v26.MagnitudeSquared() / this->m_fTurnMass + 1.0 / this->m_fMass) * retaddr);
	v27 = ClampMin(v27, -fFriction);

	CVector vecFrictionForce = vecDNormalized * v27;

	CPhysical::ApplyFrictionForce(vecFrictionForce, vecPos);

	if (retaddr <= 0.1f)
		return true;

	if (EFrictionEffectNone == g_surfaceInfos.GetFrictionEffect(colPoint.materialB.surfaceType))
		return true;

	if (g_surfaceInfos.GetFrictionEffect(colPoint.materialA.surfaceType) != EFrictionEffectSparks
		&& this->_GetEntityType() != ENTITY_TYPE_VEHICLE)
		return true;

	if (this->_GetEntityType() == ENTITY_TYPE_VEHICLE)
	{
		CVehicle * pVehicle = (CVehicle *)this;
		if (pVehicle->m_nVehicleSubClass == VEHICLE_BMX
			&& pVehicle->m_pDriver
			&& fabs(DotProduct(colPoint.m_vecNormal, this->_GetMatrixNoUpdate().right)) < 0.87f)
			return true;
	}

	float v28 = *(float *)&retaddr * 0.25f;
	CVector v41 = v28 * vecDNormalized;

	float fSparkForce = *(float *)&retaddr * 12.5f;

	CVector vecDirection = vecDNormalized;
	vecDirection += 0.1 * colPoint.m_vecNormal;

	CVector v37 = CrossProduct(colPoint.m_vecNormal, this->m_vecTurnSpeed);
	v37.Normalise();

	for (int i = 0; i < 8; ++i)
	{
		float fRand = rand();
		float fRandOffset = fRand * 0.00003f * 0.4f - 0.2;
			
		CVector vecOrigin = colPoint.m_vecPoint + v37 * fRandOffset;
			
		g_fx.AddSparks(vecOrigin, vecDirection, 
			fSparkForce, 1, v41, 0, 0.1, 1.0);
	}
	return true;
}

void CPhysical::ApplyFrictionForce(CVector vecForce, CVector vecPos)
{
	CVector vecLocalForce(vecForce);

	if (this->m_nPhysicalFlags.bDisableZ)
		vecLocalForce.z = 0.0;

	if (false == this->m_nPhysicalFlags.bDisableMoveForce
	&& false == this->m_nPhysicalFlags.bInfiniteMass)
	{
		this->m_vecFrictionMoveSpeed += vecLocalForce / this->m_fMass;
	}

	if (this->m_nPhysicalFlags.bDisableTurnForce)
		return;

	float fTurnMass = this->m_fTurnMass;

	CVector vecOrigin = CVector(0.0f, 0.0f, 0.0f);

	if (this->m_nPhysicalFlags.bInfiniteMass)
	{
		fTurnMass += this->m_vecCentreOfMass.z * this->m_fMass * this->m_vecCentreOfMass.z * 0.5;
	}
	else
	{
		vecOrigin = Multiply3x3(*this->GetMatrix(), this->m_vecCentreOfMass);
	}

	if (this->m_nPhysicalFlags.bDisableMoveForce)
	{
		vecPos.z = 0.0;
		vecForce.z = 0.0;
	}

	CVector vecPositionVector = vecPos - vecOrigin;

	this->m_vecFrictionTurnSpeed += CrossProduct(vecPositionVector, vecForce) / fTurnMass;
}

void CPhysical::ApplyFrictionTurnForce(CVector vecForce, CVector vecPos)
{
	if (this->m_nPhysicalFlags.bDisableTurnForce)
		return;

	CVector vecOrigin(0.0f, 0.0f, 0.0f);

	float fTurnMass = this->m_fTurnMass;

	if (this->m_nPhysicalFlags.bInfiniteMass)
	{
		fTurnMass += this->m_vecCentreOfMass.z * this->m_fMass * this->m_vecCentreOfMass.z * 0.5f;
	}
	else
	{
		vecOrigin = Multiply3x3(this->_GetMatrixNoUpdate(), this->m_vecCentreOfMass);
	}

	if (this->m_nPhysicalFlags.bDisableMoveForce)
	{
		vecPos.z = 0.0;
		vecForce.z = 0.0;
	}

	CVector vecOffset = vecPos - vecOrigin;
	
	CVector vecTorque = CrossProduct(vecOffset, vecForce);

	this->m_vecFrictionTurnSpeed += vecTorque / fTurnMass;
}
void CPhysical::ApplySpeed()
{
#if 0
	CObject *v1; // esi
	int v2; // eax
	double v3; // st7
	CMatrix *v4; // ecx
	_DWORD *v5; // eax
	_DWORD *v6; // edx
	double v7; // st7
	double v8; // st7
	double v9; // st7
	double v10; // st7
	char v11; // bl
	CPhysical *v12; // ecx
	CMatrix *v13; // ecx
	double v14; // st7
	double v15; // st7
	double v16; // st7
	CPhysical *v17; // ecx
	CMatrix *v18; // ecx
	double v19; // st7
	double v20; // st7
	double v21; // st7
	float v22; // ST04_4
	CMatrix *v23; // eax
	double v24; // st7
	double v25; // st7
	float *v26; // eax
	int v27; // ST2C_4
	float v28; // ST30_4
	double v29; // st6
	float v30; // ST34_4
	float a2; // ST00_4
	CPhysical *v32; // ecx
	double v33; // st7
	__int16 v34; // fps
	bool v35; // c0
	char v36; // c2
	bool v37; // c3
	int result; // eax
	CMatrix *v39; // eax
	double v40; // st7
	double v41; // st7
	double v42; // st7
	float v43; // ST08_4
	CPhysical *v44; // ecx
	double v45; // st7
	long double v46; // st7
	__int16 v47; // fps
	long double v48; // st6
	bool v49; // c0
	char v50; // c2
	bool v51; // c3
	float v52; // [esp+10h] [ebp-60h]
	float v53; // [esp+10h] [ebp-60h]
	float v54; // [esp+10h] [ebp-60h]
	float v55; // [esp+14h] [ebp-5Ch]
	float v56; // [esp+18h] [ebp-58h]
	float v57; // [esp+18h] [ebp-58h]
	float v58; // [esp+1Ch] [ebp-54h]
	float v59; // [esp+1Ch] [ebp-54h]
	float v60; // [esp+20h] [ebp-50h]
	float v61; // [esp+24h] [ebp-4Ch]
	float v62; // [esp+38h] [ebp-38h]
	float v63; // [esp+3Ch] [ebp-34h]
	int a3; // [esp+44h] [ebp-2Ch]
	float v65; // [esp+48h] [ebp-28h]
	float v66; // [esp+4Ch] [ebp-24h]
	float v67; // [esp+54h] [ebp-1Ch]
	float v68; // [esp+58h] [ebp-18h]
	int v69; // [esp+5Ch] [ebp-14h]

	v1 = (CObject *)this;
	v55 = CTimer::ms_fTimeStep;
	v2 = this->nImmunities;

	// Process snooker cue / balls
	if (this->m_nPhysicalFlags.bDisableZ)
	{
		v3 = CTimer::ms_fTimeStep;

		if (this->m_nPhysicalFlags.bApplyGravity)
		{
			if (v3 * v1->__parent.m_vVelocity.z + v4->rwMat.pos.z < CWorld::SnookerTableMin.z)
			{
				v4->rwMat.pos.z = CWorld::SnookerTableMin.z;
				this->m_vecMoveSpeed = CVector(0.f, 0.f, 0.f);
				this->m_vecTurnSpeed = CVector(0.f, 0.f, 0.f);
			}
			this->ApplyMoveSpeed();
			this->ApplyTurnSpeed();
			v33 = v55;
			if (v55 <= 0.00001f)
				v33 = 0.00001f;
			CTimer::ms_fTimeStep = v33;
			return;
		}
		v56 = 1000.0;
		v52 = 1000.0;

		v7 = v3 * this->m_vecMoveSpeed.x + this->_GetPosNoUpdate().x;
		if (v7 > CWorld::SnookerTableMax.x && this->m_vecMoveSpeed.x > 0.0)
		{
			v56 = (CWorld::SnookerTableMax.x - this->_GetPosNoUpdate().x) / this->m_vecMoveSpeed.x;
		}
		else if (v7 < CWorld::SnookerTableMin.x && this->m_vecMoveSpeed.x < 0.0)
		{
			v56 = (CWorld::SnookerTableMin.x - this->_GetPosNoUpdate().x) / this->m_vecMoveSpeed.x;
		}


		v9 = CTimer::ms_fTimeStep * this->m_vecMoveSpeed.y + this->_GetPosNoUpdate().y;
		if (v9 > CWorld::SnookerTableMax.y && this->m_vecMoveSpeed.y > 0.0)
		{
			v52 = (CWorld::SnookerTableMax.y - this->_GetPosNoUpdate().y) / this->m_vecMoveSpeed.y;
		}
		else if (v9 < CWorld::SnookerTableMin.y && this->m_vecMoveSpeed.y < 0.0)
		{
			v52 = (CWorld::SnookerTableMin.y - this->_GetPosNoUpdate().y) / this->m_vecMoveSpeed.y;
		}
		
		v11 = 1;
		if (CWorld::SnookerTableMax.x - CWorld::SnookerTableMin.x < CWorld::SnookerTableMax.y
			- CWorld::SnookerTableMin.y)
			v11 = 0;

		CVector vecNormal(0.0f, 0.0f, 0.0f);

		if (v56 < (double)v52 && v56 < 1000.0)
		{
			vecNormal.x = -1.0;
			
			v58 = fabs(v1->__parent.m_vVelocity.x);
			
			if (v1->__parent.m_vVelocity.x <= 0.0)
				vecNormal.x = 1.0;

			sub_541F40(v56);
			this->ApplyMoveSpeed();
			this->ApplyTurnSpeed();
			v13 = v1->__parent.__parent.__parent.m_pCoords;
			if (CWorld::SnookerTableMax.y - 0.06f >= v13->rwMat.pos.y
				&& CWorld::SnookerTableMin.y + 0.06f <= v13->rwMat.pos.y
				&& (v11
					|| (v14 = (CWorld::SnookerTableMin.y + CWorld::SnookerTableMax.y) * 0.5,
						v14 - 0.06f >= v13->rwMat.pos.y)
					|| v13->rwMat.pos.y >= v14 + 0.059999999))
			{
				v1->__parent.m_vVelocity.x = v1->__parent.m_vVelocity.x * -1.0;
				v16 = v55 - v56;
			}
			else
			{
				v15 = v55 * v1->__parent.m_vVelocity.x;
				v1->__parent.nImmunities |= 2u;
				if (v15 <= 0.03f)
				{
					if (v15 < -0.03f)
						v1->__parent.m_vVelocity.x = -(0.03f / v55);
					v16 = v55 - v56;
				}
				else
				{
					v1->__parent.m_vVelocity.x = 0.03f / v55;
					v16 = v55 - v56;
				}
			}
			goto LABEL_51;
		}
		if (v52 < 1000.0)
		{
			vecNormal.y = -1.0;
			v58 = fabs(v1->__parent.m_vVelocity.y);
			if (v1->__parent.m_vVelocity.y <= 0.0)
				vecNormal.y = 1.0;

			CTimer::_SetTimeStepClipped(v52);
			this->ApplyMoveSpeed();
			this->ApplyTurnSpeed();

			v18 = v1->__parent.__parent.__parent.m_pCoords;
			
			if (CWorld::SnookerTableMax.x - 0.06f < v18->rwMat.pos.x
				|| CWorld::SnookerTableMin.x + 0.06f > v18->rwMat.pos.x
				|| v11
				&& (v19 = (CWorld::SnookerTableMin.x + CWorld::SnookerTableMax.x) * 0.5,
					v19 - 0.06f < v18->rwMat.pos.x)
				&& v18->rwMat.pos.x < v19 + 0.06f)
			{
				v20 = v55 * v1->__parent.m_vVelocity.y;
				v1->__parent.nImmunities |= 2u;
				if (v20 <= 0.03f)
				{
					if (v20 >= -0.03f)
					{
					LABEL_50:
						v16 = v55 - v52;
					LABEL_51:
						v22 = v16;
						sub_541F40(v22);
						if (v58 > 0.0)
						{
							float fRadius = CModelInfo::ms_modelInfoPtrs[this->m_nModelIndex]->m_pColModel->m_boundSphere.m_fRadius;
							CColPoint colPoint;
							colPoint.m_vecPoint = this->GetPosition() - vecNormal * fRadius;
							colPoint.m_vecNormal = vecNormal;
							float fFriction = 10.0 * v58;
							this->ApplyFriction(fFriction, colPoint);
							if (this->_GetEntityType() == ENTITY_TYPE_OBJECT)
							{
								CObject * pObject = (CObject *)this;
								AudioEngine.ReportMissionAudioEvent(SOUND_POOL_HIT_CUSHION, pObject);
								if (pObject->m_nLastWeaponDamage == -1)
									pObject->m_nLastWeaponDamage = 54;
								else
									pObject->m_nLastWeaponDamage = 50;
							}
						}
						this->ApplyMoveSpeed();
						this->ApplyTurnSpeed();
						CTimer::_SetTimeStepClipped(v55);

						return;
					}
					v1->__parent.m_vVelocity.y = -(0.03f / v55);
				}
				else
				{
					v1->__parent.m_vVelocity.y = 0.03f / v55;
				}
			}
			else
			{
				v1->__parent.m_vVelocity.y = v1->__parent.m_vVelocity.y * -1.0;
			}
			goto LABEL_50;
		}
		this->ApplyMoveSpeed();
		this->ApplyTurnSpeed();
		CTimer::_SetTimeStepClipped(v55);
		return;
	}

	// Process door
	CObject * pObject = (CObject *)this;
	if (this->m_nPhysicalFlags.bDisableMoveForce
		&& this->_GetEntityType() == ENTITY_TYPE_OBJECT
		&& pObject->m_fDoorStartAngle > -1000.0)
	{
		v57 = pObject->m_fDoorStartAngle;
		
		/** CHECK
		if (v39)
			v53 = atan2(-v39->rwMat.up.x, v39->rwMat.up.y);
		else
			v53 = this->__parent.__parent.placement.angle;
		*/

		v53 = this->_GetHeading();

		if (v57 + rwPI >= v53)
		{
			if (v57 - rwPI > v53)
				v53 += 2 * rwPI;
		}
		else
		{
			v53 -= 2 * rwPI;
		}

		v59 = -1000.0;
		if (this->m_vAngularVelocity.z <= 0.0
			|| (v41 = flt_8CD7E8 + v57, CTimer::ms_fTimeStep * this->m_vAngularVelocity.z + v53 <= v41))
		{
			if (this->m_vAngularVelocity.z < 0.0)
			{
				v42 = v57 - flt_8CD7E8;
				if (CTimer::ms_fTimeStep * this->m_vAngularVelocity.z + v53 < v42)
					v59 = (v42 - v53) / this->m_vAngularVelocity.z;
			}
		}
		else
		{
			v59 = (v41 - v53) / this->m_vAngularVelocity.z;
		}

		if (-CTimer::ms_fTimeStep <= v59)
		{
			sub_541F40(v59);
			this->ApplyTurnSpeed();
			this->m_vecTurnSpeed.z *= flt_8CD7E4;
			v43 = v55 - v59;
			sub_541F40(v43);
			this->m_nPhysicalFlags.b31 = true;
		}

		this->ApplyMoveSpeed();
		this->ApplyTurnSpeed();
		CTimer::_SetTimeStepClipped(v55);
		if (pObject->m_nObjectFlags.bIsDoorMoving)
		{
			v45 = CPlaceable::getRotation((_CPlaceable *)v1);
			if (v45 + rwPI >= v57)
			{
				if (v45 - rwPI > v57)
					v45 -= 2 * rwPI;
			}
			else
			{
				v45 += 2 * rwPI;
			}
			v54 = v53 - v57;
			v46 = v45 - v57;
			if (fabs(v54) < 0.001)
				v54 = 0.0;
			if (fabs(v46) < 0.001)
				v46 = 0.0;
			v48 = v54 * v46;
			if (v48 <= 0.0)//CHECK
				this->m_vecTurnSpeed.z = 0.0f;
		}
	}
	else
	{
		this->ApplyMoveSpeed();
		this->ApplyTurnSpeed();
		v33 = v55;
		if (v55 <= 0.00001f)
			v33 = 0.00001f;
		CTimer::ms_fTimeStep = v33;
		return;
	}
#endif
}

void CPhysical::SkipPhysics()
{
	if (this->_GetEntityType() != ENTITY_TYPE_PED
		&& this->_GetEntityType() != ENTITY_TYPE_VEHICLE)
		this->m_nPhysicalFlags.bSubmergedInWater = false;

	this->m_bHasContacted = false;
	this->m_bIsInSafePosition = false;
	this->m_bWasPostponed = false;
	this->m_bHasHitWall = false;

	if (this->m_nStatus != STATUS_SIMPLE)
	{
		this->m_nPhysicalFlags.bOnSolidSurface = false;
		this->m_nNumEntitiesCollided = 0;
		this->m_nPieceType = 0;
		this->m_fDamageIntensity = 0.0f;

		if (this->m_pDamageEntity)
			this->m_pDamageEntity->CleanUpOldReference(&this->m_pDamageEntity);

		this->m_vecFrictionTurnSpeed = CVector(0.f, 0.f, 0.f);
		this->m_vecFrictionMoveSpeed = CVector(0.f, 0.f, 0.f);
		this->m_pDamageEntity = nullptr;
	}
}

bool CPhysical::ApplyCollision(CEntity * pEntity, CColPoint & colPoint, float & fImpulse)
{
	if (this->m_nPhysicalFlags.bDisableTurnForce)
	{
		float fDot = DotProduct(this->m_vecMoveSpeed, colPoint.m_vecNormal);//v6
		if (fDot >= 0.0)
		{
			return false;
		}
		fImpulse = -(fDot * this->m_fMass);//v7
		
		CVector vecMoveForce = fImpulse * colPoint.m_vecNormal;
		this->ApplyMoveForce(vecMoveForce);

		float a8 = fImpulse / this->m_fMass;
#if 0
		AudioEngine.ReportCollision(
			this,
			(int)colEntity,
			(unsigned __int8)colPoint_->material1.surfaceType,
			(unsigned __int8)colPoint_->material2.surfaceType,
			(int)colPoint_,
			(int)a8,
			a8,
			1.0,
			0,
			0);
#endif
		return true;
	}

	CVector vecPos = colPoint.m_vecPoint - this->_GetPosNoUpdate();

	CVector vecSpeed = this->GetSpeed(vecPos);

	float v12 = DotProduct(colPoint.m_vecNormal, vecSpeed);
	
	if (v12 >= 0.0)
	{
		return false;
	}

	CVector vecOrigin = Multiply3x3(this->_GetMatrixNoUpdate(), this->m_vecCentreOfMass);

	CVector vecOffset = vecPos - vecOrigin;

	CVector vecTorque = CrossProduct(vecOffset, colPoint.m_vecNormal);

	float colPointa = 1.0 / (vecTorque.MagnitudeSquared() / this->m_fTurnMass + 1.0 / this->m_fMass);
	fImpulse = -((1.0f + this->m_fElasticity) * colPointa * v12);

	CVector vecForce = colPoint.m_vecNormal * fImpulse;

	if (this->_GetEntityType() == ENTITY_TYPE_VEHICLE)
	{
		if (colPoint.m_vecNormal.z <= 0.7f)
			vecForce.z *= 0.3f;
	}

	if (false == this->m_nPhysicalFlags.bDisableCollisionForce)
	{
		bool bApplyRotation = true;
		if (this->_GetEntityType() == ENTITY_TYPE_VEHICLE == 2 && CWorld::bNoMoreCollisionTorque)
			bApplyRotation = false;

		this->ApplyForce(vecForce, vecOffset, bApplyRotation);
	}
	
#if 0
	a8 = fImpulse / colPointa;
	v27 = &colPoint_->Normal;
	AudioEngine.ReportCollision(
		this,
		(int)colEntity,
		(unsigned __int8)colPoint_->material1.surfaceType,
		(unsigned __int8)colPoint_->material2.surfaceType,
		(int)colPoint_,
		(int)v27,
		a8,
		1.0,
		0,
		0);
#endif
	return true;
}

bool CPhysical::ApplySpringCollision(float k_constant, CVector & vecForce, 
	CVector & vecPoint, float damp, float bias, float & fForce)
{
	float inv = 1.0f - damp;

	if (inv <= 0.0f)
		return true;

	float dt = ClampMax(CTimer::ms_fTimeStep, 3.f);
	fForce = inv * this->m_fMass * k_constant * 0.016f * dt * bias;

	CVector vecSpringForce = -fForce * vecForce;

	this->ApplyForce(vecSpringForce, vecPoint, true);
	return true;
}

bool CPhysical::ApplySpringCollisionAlt(float k_constant, CVector & vecForce, CVector & vecPoint, float damp, 
	float bias, CVector & vecNormal, float & fForce)
{
	float inv_d = 1.0 - damp;
	
	if (inv_d <= 0.0)
		return true;

	if (DotProduct(vecForce, vecNormal) > 0.0)
	{
		vecNormal *= -1.0f;
	}

	float dt = ClampMax(CTimer::ms_fTimeStep, 3.0f);

	fForce = inv_d * (dt * this->m_fMass) * k_constant * bias * 0.016f;
	
	if (this->m_nPhysicalFlags.b01)
		fForce *= 0.75f;

	CVector vecSpringForce = fForce * vecNormal;
	
	this->ApplyForce(vecSpringForce, vecPoint, true);

	return true;
}

bool CPhysical::ApplySpringDampening(float fDampingForce, float a3, CVector & vecForce, CVector & vecPoint, CVector & vecNormal)
{
	float v20 = DotProduct(vecNormal, vecForce);

	CVector vecSpeed = this->GetSpeed(vecPoint);

	float v29 = DotProduct(vecSpeed, vecForce);

	float dt = ClampMax(CTimer::ms_fTimeStep, 3.0f);

	float fDamping = dt * fDampingForce;
	
	if (this->m_nPhysicalFlags.b01)
		fDamping = fDamping + fDamping;

	fDamping = Clamp(-DAMPING_LIMIT_IN_FRAME, fDamping, DAMPING_LIMIT_IN_FRAME);

	float v12 = -(fDamping * v20);

	if (v12 > 0.0 && v12 + v29 > 0.0)
	{
		if (v29 >= 0.0)
		{
			v12 = 0.0;
		}
		else
			v12 = -v29;
	}
	else if (v12 < 0.0 && v12 + v29 < 0.0)
	{
		if (v29 <= 0.0)
			v12 = 0.0;
		else
			v12 = -v29;
	}

	CVector vecOrigin = Multiply3x3(this->_GetMatrixNoUpdate(), this->m_vecCentreOfMass);
	
	CVector vecOffset = vecPoint - vecOrigin;
	
	CVector vecTorque = CrossProduct(vecOffset, vecForce);
	
	float fLimit = fabs(a3) * DAMPING_LIMIT_OF_SPRING_FORCE;

	float fForce = ClampMax(v12 / (vecTorque.MagnitudeSquared() / this->m_fTurnMass + 1.0f / this->m_fMass), fLimit);

	CVector vecDampForce = fForce * vecForce;

	this->ApplyForce(vecDampForce, vecPoint, true);

	return true;
}

void CPhysical::PlacePhysicalRelativeToOtherPhysical(CPhysical * pFirst, CPhysical * pSecond, CVector vecOffset)
{
	CVector vecPos = pFirst->_GetMatrixNoUpdate() * vecOffset;

	float dt = CTimer::ms_fTimeStep * 0.9f;

	CVector vecNewPos = vecPos + dt * pFirst->m_vecMoveSpeed;

	CWorld::Remove(pSecond);

	pSecond->_GetMatrixNoUpdate() = pFirst->_GetMatrixNoUpdate();
	pSecond->GetPosition() = vecNewPos;
	pSecond->m_vecMoveSpeed = pFirst->m_vecMoveSpeed;
	pSecond->UpdateRW();
	pSecond->UpdateRwFrame();

	CWorld::Add(pSecond);
}
 
void CPhysical::ProcessShift()
{
	CRect boundRect = this->GetBoundRect();
	this->m_fMovingSpeed = 0.0f;

	if (this->m_nStatus == STATUS_SIMPLE || (this->m_nPhysicalFlags.bDisableMoveForce
		&& this->m_nPhysicalFlags.bInfiniteMass
		&& this->m_nPhysicalFlags.bDisableZ))
	{
		if (this->m_nPhysicalFlags.bDisableMoveForce
			&& this->m_nPhysicalFlags.bInfiniteMass
			&& this->m_nPhysicalFlags.bDisableZ)
		{
			this->m_vecTurnSpeed = CVector(0.f, 0.f, 0.f);
		}

		this->m_bIsStuck = false;
		this->m_bIsInSafePosition = true;

		this->RemoveAndAdd();
		return;
	}
	if (this->m_bIsStaticWaitingForCollision)
	{
		CPed * pPed = (CPed *)this;
		CPhysical * pContactEntity = (CPhysical *)pPed->m_pContactEntity;
		if (this->_GetEntityType() == ENTITY_TYPE_PED 
			&& 
			(pContactEntity == nullptr
				|| pContactEntity->m_nPhysicalFlags.bDisableCollisionForce == false
				|| pContactEntity->m_nPhysicalFlags.bCollidable)
			|| CWorld::bSecondShift)
		{
			this->m_vecMoveSpeed *= pow(PHYSICAL_SHIFT_SPEED_DAMP, CTimer::ms_fTimeStep);
			this->m_vecTurnSpeed *= pow(PHYSICAL_SHIFT_SPEED_DAMP, CTimer::ms_fTimeStep);
		}
	}
		
	CMatrix v36(this->_GetMatrixNoUpdate());
		
	this->ApplySpeed();
	this->_GetMatrixNoUpdate().Reorthogonalise();
		
	CWorld::_IncrementScanCode();
	bool  v26 = 0;
		
	if (this->_GetEntityType() == ENTITY_TYPE_VEHICLE)
		this->m_nPhysicalFlags.b16 = true;

	int iSectorLeft = (unsigned __int64)floor(boundRect.left * 0.02 + 60.0);
	int iSectorBottom = (unsigned __int64)floor(boundRect.bottom * 0.02 + 60.0);
	int iSectorRight = (unsigned __int64)floor(boundRect.right * 0.02 + 60.0);
	int iSectorTop = (unsigned __int64)floor(boundRect.top * 0.02 + 60.0);

	for (int iSectorY = iSectorBottom; iSectorY <= iSectorTop; ++iSectorY)
	{
		for (int iSectorX = iSectorLeft; iSectorX <= iSectorRight; ++iSectorX)
		{
			if (this->ProcessShiftSectorList(iSectorX, iSectorY))
				v26 = 1;
		}
	}
		
	this->m_nPhysicalFlags.b16 = false;

	if (v26 || this->_GetEntityType() == ENTITY_TYPE_VEHICLE)
	{
		CWorld::_IncrementScanCode();

		bool v27 = 0;
		
		iSectorLeft = (unsigned __int64)floor(boundRect.left * 0.02 + 60.0);
		iSectorBottom = (unsigned __int64)floor(boundRect.bottom * 0.02 + 60.0);
		iSectorRight = (unsigned __int64)floor(boundRect.right * 0.02 + 60.0);
		iSectorTop = (unsigned __int64)floor(boundRect.top * 0.02 + 60.0);
		
		for (int iSectorY = iSectorBottom; iSectorY <= iSectorTop; ++iSectorY)
		{
			for (int iSectorX = iSectorLeft; iSectorX <= iSectorRight; ++iSectorX)
			{
				if (this->ProcessCollisionSectorList(iSectorX, iSectorY))
				{
					if (!CWorld::bSecondShift)
					{
						this->_GetMatrixNoUpdate() = v36;
						return;
					}
					v27 = 1;
				}
			}
		}

		if (v27)
		{
			this->_GetMatrixNoUpdate() = v36;
			return;
		}
	}

	this->m_bIsStuck = false;
	this->m_bIsInSafePosition = true;

	CVector vecDeltaPos = this->_GetPosNoUpdate() - v36.pos;

	this->m_fMovingSpeed = vecDeltaPos.Magnitude();

	this->RemoveAndAdd();
}

void CPhysical::ApplyAirResistance()
{
	if (this->m_fAirResistance <= 0.1f
		|| this->_GetEntityType() == ENTITY_TYPE_VEHICLE)
	{
		float v6 = this->m_vecMoveSpeed.Magnitude() * this->m_fAirResistance;

		if (CCullZones::DoExtraAirResistanceForPlayer()
			&& this->_GetEntityType() == ENTITY_TYPE_VEHICLE)
		{
			CVehicle * pVehicle = (CVehicle *)this;

			if (pVehicle->m_nVehicleSubClass == VEHICLE_AUTOMOBILE || pVehicle->m_nVehicleSubClass == VEHICLE_BIKE)
				v6 *= CVehicle::m_fAirResistanceMult;
		}

		float fResist = pow(1.0 - v6, CTimer::ms_fTimeStep);

		this->m_vecMoveSpeed *= fResist;
		this->m_vecTurnSpeed *= 0.99f;
		
		return;
	}
	float fResist = pow(this->m_fAirResistance, CTimer::ms_fTimeStep);
	this->m_vecMoveSpeed *= fResist;
	this->m_vecTurnSpeed *= fResist;
}

void CPhysical::ProcessControl()
{
	if (this->_GetEntityType() != ENTITY_TYPE_PED)
		this->m_nPhysicalFlags.bSubmergedInWater = false;

	this->m_bHasContacted = false;
	this->m_bHasHitWall = false;
	this->m_bIsInSafePosition = false;
	this->m_bWasPostponed = false;
 
	if (this->m_nStatus != STATUS_SIMPLE)
	{
		this->m_nPhysicalFlags.b31 = false;
		this->m_nPhysicalFlags.bOnSolidSurface = false;
		this->m_nNumEntitiesCollided = 0;
		this->m_nPieceType = 0;
		this->m_fDamageIntensity = 0.0f;
		
		if (this->m_pDamageEntity)
			this->m_pDamageEntity->CleanUpOldReference(&this->m_pDamageEntity);
		
		this->m_pDamageEntity = nullptr;
		
		this->ApplyFriction();

		if (!this->m_pAttachedTo || this->m_nPhysicalFlags.bInfiniteMass)
		{
			this->ApplyGravity();
			this->ApplyAirResistance();
		}
	}
}

// Converted from thiscall bool CPhysical::ProcessShiftSectorList(int sectorX,int sectorY) 0x546670
bool CPhysical::ProcessShiftSectorList(int sectorX, int sectorY)
{
	return ((bool(__thiscall *)(CPhysical*, int, int))0x546670)(this, sectorX, sectorY);
}

// Converted from thiscall bool CPhysical::ProcessCollisionSectorList(int sectorX,int sectorY) 0x54BA60
bool CPhysical::ProcessCollisionSectorList(int sectorX, int sectorY)
{
	return ((bool(__thiscall *)(CPhysical*, int, int))0x54BA60)(this, sectorX, sectorY);
}

// Converted from thiscall void CPhysical::AddCollisionRecord(CEntity *collidedEntity) 0x543490
void CPhysical::AddCollisionRecord(CEntity* collidedEntity)
{
	((void(__thiscall *)(CPhysical*, CEntity*))0x543490)(this, collidedEntity);
}

// Converted from thiscall void CPhysical::ApplyFriction(void) 0x5483D0
void CPhysical::ApplyFriction()
{
	((void(__thiscall *)(CPhysical*))0x5483D0)(this);
}

// Converted from thiscall void CPhysical::RemoveAndAdd(void) 0x542560
void CPhysical::RemoveAndAdd()
{
	((void(__thiscall *)(CPhysical*))0x542560)(this);
}