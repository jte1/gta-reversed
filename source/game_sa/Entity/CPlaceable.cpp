#include "CPlaceable.h"

void CPlaceable::SetHeading(float heading)
{
	if (this->m_matrix)
		this->m_matrix->SetRotateZOnly(heading);
	else
		this->m_placement.m_fHeading = heading;
}

CVector CPlaceable::GetRightDirection()
{
	if (this->m_matrix)
	{
		return this->m_matrix->right;
	}
	else
	{
		return this->m_placement.GetRight();
	}
}

CVector CPlaceable::GetTopDirection()
{
	if (this->m_matrix)
	{
		return this->m_matrix->up;
	}
	else
	{
		return this->m_placement.GetForward();
	}
}

CVector CPlaceable::GetAtDirection()
{
	if (this->m_matrix)
	{
		return this->m_matrix->at;
	}
	else
	{
		return this->m_placement.GetAt();
	}
}

void CPlaceable::SetPosn(float x, float y, float z)
{
	((void(__thiscall *)(CPlaceable *, float, float, float))0x420B80)(this, x, y, z);
}

void CPlaceable::SetPosn(CVector const& posn)
{
	((void(__thiscall *)(CPlaceable *, CVector const&))0x4241C0)(this, posn);
}

void CPlaceable::SetOrientation(float x, float y, float z)
{
	((void(__thiscall *)(CPlaceable *, float, float, float))0x439A80)(this, x, y, z);
}

float CPlaceable::GetHeading()
{
	if (this->m_matrix)
		return atan2(-this->m_matrix->up.x, this->m_matrix->up.y);
	else
		return this->m_placement.m_fHeading;
}

bool CPlaceable::IsWithinArea(float x1, float y1, float x2, float y2)
{
	return ((bool(__thiscall *)(CPlaceable *, float, float, float, float))0x54F200)(this, x1, y1, x2, y2);
}

bool CPlaceable::IsWithinArea(float x1, float y1, float z1, float x2, float y2, float z2)
{
	return ((bool(__thiscall *)(CPlaceable *, float, float, float, float, float, float))0x54F2B0)(this, x1, y1, z1, x2, y2, z2);
}

void CPlaceable::RemoveMatrix()
{
	((void(__thiscall *)(CPlaceable *))0x54F3B0)(this);
}

void CPlaceable::AllocateStaticMatrix()
{
	((void(__thiscall *)(CPlaceable *))0x54F4C0)(this);
}

void CPlaceable::AllocateMatrix()
{
	((void(__thiscall *)(CPlaceable *))0x54F560)(this);
}

void CPlaceable::SetMatrix(CMatrix  const& matrix)
{
	((void(__thiscall *)(CPlaceable *, CMatrix  const&))0x54F610)(this, matrix);
}

void CPlaceable::ShutdownMatrixArray() {
	((void(__cdecl *)())0x54EFD0)();
}

void CPlaceable::InitMatrixArray() {
	((void(__cdecl *)())0x54F3A0)();
}

void CPlaceable::FreeStaticMatrix() {
	((void(__thiscall *)(CPlaceable *))0x54F010)(this);
}

void CPlaceable::GetOrientation(float& x, float& y, float& z)
{
	if (this->m_matrix)
	{
		x = asinf(this->m_matrix->up.z);

		float cosx = cosf(x);
		float cosy = this->m_matrix->at.z / cosx;
		y = acosf(cosy);
		float cosz = this->m_matrix->up.y / cosx;
		z = acosf(cosz);
	}
	else
	{
		z = this->m_placement.m_fHeading;
	}
}

CVector CPlaceable::GetForward() const
{
	if (this->m_matrix)
	{
		return this->m_matrix->up;
	}
	else
	{
		return this->m_placement.GetForward();
	}
}

CMatrix * CPlaceable::GetMatrix()
{
	if (!this->m_matrix)
	{
		this->AllocateMatrix();
		this->m_placement.UpdateMatrix(this->m_matrix);
	}
	return this->m_matrix;
}