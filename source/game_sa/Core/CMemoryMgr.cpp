#include "StdInc.h"
#include "CMemoryMgr.h"

void * CMemoryMgr::Malloc(unsigned int nSize, unsigned int nHint)
{
	return plugin::CallAndReturn<void*, 0x72F420, unsigned int, unsigned int>(nSize, nHint);
	//return malloc(nSize);
}

void CMemoryMgr::Free(void * pMemory)
{
	plugin::Call<0x0072F430, void*>(pMemory);
	//free(pMemory);
}