#pragma once

class CMemoryMgr
{
public:
	static void * Malloc(unsigned int nSize, unsigned int nHint = 0);

	static void Free(void * pMemory);
};