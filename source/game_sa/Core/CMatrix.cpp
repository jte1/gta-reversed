#include "StdInc.h"

struct M {
	CVector vectors[4];
};

const int sm = sizeof(M);
const int sm2 = sizeof(CVector) * 4;

CMatrix::CMatrix(plugin::dummy_func_t)
	: m_pRwMatrix(nullptr)
	, m_bRemoveRwMatrix(false)
{}

// EU-1.00 @ 0x0059C050
CMatrix::CMatrix(RwMatrix * pMatrix, bool bAutoRemove)
	: m_pRwMatrix(nullptr)
{
	this->Attach(pMatrix, bAutoRemove);
}

CMatrix::CMatrix(CMatrix const & refMx)
	: m_pRwMatrix(nullptr)
	, m_bRemoveRwMatrix(false)
{
	*this = refMx;
}

CMatrix::~CMatrix(void) {
	this->Detach();
}

void CMatrix::CopyOnlyMatrix(CMatrix const & refMatrix) {
	this->matRW = refMatrix.matRW;
}

// EU-1.00 @ 0x0059BD10
void CMatrix::Attach(RwMatrix * pRwMatrix, bool bAutoRemove) {
	this->Detach();
	this->m_pRwMatrix = pRwMatrix;
	this->m_bRemoveRwMatrix = bAutoRemove;
	this->Update();
}

// EU-1.00 @ 0x0059BD90
void CMatrix::AttachRW(RwMatrix * pRwMatrix, bool bAutoRemove) {
	this->Detach();
	this->m_pRwMatrix = pRwMatrix;
	this->m_bRemoveRwMatrix = bAutoRemove;
	this->UpdateRW();
}

void CMatrix::Detach(void) {
	if (this->m_bRemoveRwMatrix && this->m_pRwMatrix) {
		RwMatrixDestroy(this->m_pRwMatrix);
	}
	this->m_pRwMatrix = nullptr;
}

// EU-1.00 @ 0x0059B6A0
void CMatrix::Reorthogonalise(void) {
	//__DUMMY();
}

// EU-1.00 @ 0x0059AE70
void CMatrix::SetUnity(void) {
	this->SetScale(1);
}

// EU-1.00 @ 0x0059AED0
void CMatrix::SetScale(float fScale) {
	this->SetScale(fScale, fScale, fScale);
}

// EU-1.00 @ 0x0059AF00
void CMatrix::SetScale(float fScaleX, float fScaleY, float fScaleZ) {
	this->right = CVector(fScaleX, 0, 0);
	this->up = CVector(0, fScaleY, 0);
	this->at = CVector(0, 0, fScaleZ);
	this->SetTranslateOnly(0, 0, 0);
}

// EU-1.00 @ 0x0059AF40
void CMatrix::SetTranslate(float fX, float fY, float fZ) {
	this->SetUnity();
	this->SetTranslateOnly(fX, fY, fZ);
}

void CMatrix::SetTranslateOnly(float fX, float fY, float fZ) {
	this->pos = CVector(fX, fY, fZ);
}

void CMatrix::Rotate(float fX, float fY, float fZ) {
	//__DUMMY();
}

// EU-1.00 @ 0x0059AFA0
void CMatrix::SetRotateXOnly(float fAngle) {
	float fSin = sin(fAngle), fCos = cos(fAngle);

	this->right = CVector(1, 0, 0);
	this->up = CVector(0, fCos, fSin);
	this->at = CVector(0, -fSin, fCos);
}

// EU-1.00 @ 0x0059AFE0
void CMatrix::SetRotateYOnly(float fAngle) {
	float fSin = sin(fAngle), fCos = cos(fAngle);

	this->right = CVector(fCos, 0, -fSin);
	this->up = CVector(0, 1, 0);
	this->at = CVector(fSin, 0, fCos);
}

// EU-1.00 @ 0x0059B020
void CMatrix::SetRotateZOnly(float fAngle) {
	float fSin = sin(fAngle), fCos = cos(fAngle);

	this->right = CVector(fCos, fSin, 0);
	this->up = CVector(-fSin, fCos, 0);
	this->at = CVector(0, 0, 1);
}

// EU-1.00 @ 0x0059B060
void CMatrix::SetRotateX(float fAngle) {
	// set rotate
	this->SetRotateXOnly(fAngle);
	// reset position
	this->SetTranslateOnly(0, 0, 0);
}

// EU-1.00 @ 0x0059B0A0
void CMatrix::SetRotateY(float fAngle) {
	// set rotate
	this->SetRotateYOnly(fAngle);
	// reset position
	this->SetTranslateOnly(0, 0, 0);
}

// EU-1.00 @ 0x0059B0E0
void CMatrix::SetRotateZ(float fAngle) {
	// set rotate
	this->SetRotateZOnly(fAngle);
	// reset position
	this->SetTranslateOnly(0, 0, 0);
}

// EU-1.00 @ 0x0059B120
void CMatrix::SetRotate(float fRotX, float fRotY, float fRotZ) {
	float fCosX = cos(fRotX), fSinX = sin(fRotX);
	float fCosY = cos(fRotY), fSinY = sin(fRotY);
	float fCosZ = cos(fRotZ), fSinZ = sin(fRotZ);

	this->right.x = fCosY * fCosZ - fSinX * fSinY * fSinZ;
	this->right.y = fCosY * fSinZ + fSinX * fSinY * fCosZ;
	this->right.z = -(fSinY * fCosX);

	this->up.x = -(fSinZ * fCosX);
	this->up.y = fCosZ * fCosX;
	this->up.z = fSinX;

	this->at.x = fCosZ * fSinY + fSinX * fCosY * fSinZ;
	this->at.y = fSinZ * fSinY - fSinX * fCosY * fCosZ;
	this->at.z = fCosY * fCosX;

	// reset position
	this->SetTranslateOnly(0, 0, 0);
}

// EU-1.00 @ 0x0059BBF0
void CMatrix::SetRotate(CQuaternion const & refQuaternion) {
#if 0
	float fX2 = refQuaternion.x + refQuaternion.x;
	float fY2 = refQuaternion.y + refQuaternion.y;
	float fZ2 = refQuaternion.z + refQuaternion.z;

	float fXX = refQuaternion.x * fX2, fXY = refQuaternion.x * fY2, fXZ = refQuaternion.x * fZ2;
	float fYY = refQuaternion.y * fY2, fYZ = refQuaternion.y * fZ2;
	float fZZ = refQuaternion.z * fZ2;
	float fWX = refQuaternion.w * fX2, fWY = refQuaternion.w * fY2, fWZ = refQuaternion.w * fZ2;

	this->_vecRight.x = 1 - (fZZ + fYY);
	this->_vecRight.y = fXY + fWZ;
	this->_vecRight.z = fXZ - fWY;
	this->_vecUp.x = fXY - fWZ;
	this->_vecUp.y = 1 - (fZZ + fXX);
	this->_vecUp.z = fWX + fYZ;
	this->_vecAt.x = fWY + fXZ;
	this->_vecAt.y = fYZ - fWX;
	this->_vecAt.z = 1 - (fYY + fXX);
#endif
}
#if 0
// EU-1.00 @ 0x0059B7E0
void CMatrix::ForceUpVector(CVector vecAt) {
	this->_vecRight.x = vecAt.z * this->_vecUp.y - vecAt.y * this->_vecUp.z;
	this->_vecRight.y = vecAt.x * this->_vecUp.z - vecAt.z * this->_vecUp.x;
	this->_vecRight.z = vecAt.y * this->_vecUp.x - vecAt.x * this->_vecUp.y;

	this->_vecUp.x = this->_vecRight.z * vecAt.y - this->_vecRight.y * vecAt.z;
	this->_vecUp.y = this->_vecRight.x * vecAt.z - this->_vecRight.z * vecAt.x;
	this->_vecUp.z = vecAt.x * this->_vecRight.y - this->_vecRight.x * vecAt.y;

	this->at = vecAt;
}
#endif
// EU-1.00 @ 0x0059BB60
void CMatrix::Update(void) {
	this->UpdateMatrix(this->m_pRwMatrix);
}


// EU-1.00 @ 0x0059AD20
void CMatrix::UpdateMatrix(RwMatrix * pRwMatrix) {
	this->right.FromRwV3d(pRwMatrix->right);
	this->up.FromRwV3d(pRwMatrix->up);
	this->at.FromRwV3d(pRwMatrix->at);
	this->pos.FromRwV3d(pRwMatrix->pos);
}

// EU-1.00 @ 0x0059BBB0
void CMatrix::UpdateRW(void) {
	if (this->m_pRwMatrix) {
		this->UpdateRwMatrix(this->m_pRwMatrix);
	}
}

// EU-1.00 @ 0x0059AD70
void CMatrix::UpdateRwMatrix(RwMatrix * pRwMatrix) {
	pRwMatrix->right = this->right.ToRwV3d();
	pRwMatrix->up = this->up.ToRwV3d();
	pRwMatrix->at = this->at.ToRwV3d();
	pRwMatrix->pos = this->pos.ToRwV3d();
	RwMatrixUpdate(pRwMatrix);
}

// EU-1.00 @ 0x0059B8B0
void CMatrix::CopyToRwMatrix(RwMatrix * pRwMatrix) const {
	pRwMatrix->right = this->right.ToRwV3d();
	pRwMatrix->up = this->up.ToRwV3d();
	pRwMatrix->at = this->at.ToRwV3d();
	pRwMatrix->pos = this->pos.ToRwV3d();
	RwMatrixUpdate(pRwMatrix);
}

// EU-1.00 @ 0x0059BBC0
CMatrix & CMatrix::operator = (CMatrix const & refMatrix) {
	this->CopyOnlyMatrix(refMatrix);

	if (this->m_pRwMatrix) {
		this->UpdateRW();
	}
	return *this;
}

// EU-1.00 @ 0x0059ADF0
CMatrix & CMatrix::operator += (CMatrix const & refRight) {
	this->right += refRight.right;
	this->up += refRight.up;
	this->at += refRight.at;
	this->pos += refRight.pos;
	return *this;
}

CMatrix & CMatrix::operator *= (CMatrix const & refMatrix) {
	*this = *this * refMatrix;
	return *this;
}

CMatrix operator + (CMatrix const & refLeft, CMatrix const & refRight) {
	return CMatrix(refLeft) += refRight;
}

CVector operator * (CMatrix const & refMx, CVector const & refVec) {
	return CVector(
		refMx.pos.x + refMx.right.x * refVec.x + refMx.up.x * refVec.y + refMx.at.x * refVec.z,
		refMx.pos.y + refMx.right.y * refVec.x + refMx.up.y * refVec.y + refMx.at.y * refVec.z,
		refMx.pos.z + refMx.right.z * refVec.x + refMx.up.z * refVec.y + refMx.at.z * refVec.z
	);
}

CMatrix operator * (CMatrix const & refLeft, CMatrix const & refRight) {
	//__DUMMY();
	return CMatrix(plugin::dummy);
}

// EU-1.00 @ 0x0059BDD0
CMatrix Invert(CMatrix const & refSrc) {
	return CMatrix(Invert(refSrc, CMatrix(plugin::dummy)));
}

// EU-1.00 @ 0x0059B920
CMatrix & Invert(CMatrix const & refSrc, CMatrix & refDst) {
	refDst.pos = CVector(0, 0, 0);

	refDst.right = CVector(refSrc.right.x, refSrc.up.x, refSrc.at.x);
	refDst.up = CVector(refSrc.right.y, refSrc.up.y, refSrc.at.y);
	refDst.at = CVector(refSrc.right.z, refSrc.up.z, refSrc.at.z);;

	refDst.pos.x = refSrc.pos.x * refDst.right.x;
	refDst.pos.y = refDst.right.y * refSrc.pos.x;
	refDst.pos.z = refDst.right.z * refSrc.pos.x;

	refDst.pos.x += refSrc.pos.y * refDst.up.x;
	refDst.pos.y += refDst.up.y * refSrc.pos.y;
	refDst.pos.z += refDst.up.z * refSrc.pos.y;

	refDst.pos.x += refDst.at.x * refSrc.pos.z;
	refDst.pos.y += refSrc.pos.z * refDst.at.y;
	refDst.pos.z += refDst.at.z * refSrc.pos.z;

	refDst.pos *= -1;

	return refDst;
}
