/*
    Plugin-SDK (Grand Theft Auto San Andreas) header file
    Authors: GTA Community. See more here
    https://github.com/DK22Pac/plugin-sdk
    Do not delete this comment block. Respect others' work!
*/
#pragma once

#include "PluginBase.h"
#include "CLink.h"

template<typename T>
class CLinkList {
public:
    CLink<T> usedListHead;
    CLink<T> usedListTail;
    CLink<T> freeListHead;
    CLink<T> freeListTail;
    CLink<T>* links;

	void InsertIntoFree(CLink<T> * pLink)
	{
		// Remove from used list
		pLink->Remove();

		// Insert into free list
		pLink->next = this->freeListHead.next;
		this->freeListHead.next->prev = pLink;
		pLink->prev = &this->freeListHead;
		this->freeListHead.next = pLink;
	}

	void InsertIntoUsed(CLink<T> * pLink)
	{
		pLink->Remove();

		pLink->next = this->usedListHead.next;
		this->usedListHead.next->prev = pLink;
		pLink->prev = &this->usedListHead;
		this->usedListHead.next = pLink;
	}

	CLink<T> * Insert(T const & refData) {

		if (this->freeListHead.next == &this->freeListTail) {
			return nullptr;
		}
		CLink<T> * pLink = this->freeListHead.next;

		pLink->data = refData;

		// remove from free list
		pLink->Remove();

		// insert into used list
		this->usedListHead.Insert(pLink);

		return pLink;
	}
};

VALIDATE_SIZE(CLinkList<void *>, 0x34);