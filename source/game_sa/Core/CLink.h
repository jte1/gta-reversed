/*
    Plugin-SDK (Grand Theft Auto San Andreas) header file
    Authors: GTA Community. See more here
    https://github.com/DK22Pac/plugin-sdk
    Do not delete this comment block. Respect others' work!
*/
#pragma once

#include "PluginBase.h"

template<typename T>
class CLink {
public:
    T data;
    CLink<T>* prev;
    CLink<T>* next;


	void Insert(CLink<T> * pLink) {
		pLink->next = this->next;
		this->next->prev = pLink;
		pLink->prev = this;
		this->next = pLink;
	}

	void Remove(void) {
		this->next->prev = this->prev;
		this->prev->next = this->next;
	}
};

VALIDATE_SIZE(CLink<void *>, 0xC);