#include "CPedIntelligence.h"

bool CPedIntelligence::GetUsingParachute()
{
	return plugin::CallMethodAndReturn<bool, 0x006011B0, CPedIntelligence*>(this);
}

CTaskSimpleClimb* CPedIntelligence::GetTaskClimb()
{
	return plugin::CallMethodAndReturn<CTaskSimpleClimb*, 0x601180, CPedIntelligence*>(this);
}

CTaskSimpleJetPack* CPedIntelligence::GetTaskJetPack()
{
	return plugin::CallMethodAndReturn<CTaskSimpleJetPack*, 0x601110, CPedIntelligence*>(this);
}

CTaskSimpleSwim* CPedIntelligence::GetTaskSwim()
{
	return plugin::CallMethodAndReturn<CTaskSimpleSwim*, 0x601070, CPedIntelligence*>(this);
}

CTaskSimpleUseGun* CPedIntelligence::GetTaskUseGun()
{
	return plugin::CallMethodAndReturn<CTaskSimpleUseGun*, 0x600F70, CPedIntelligence*>(this);
}

// Converted from thiscall CTaskSimpleDuck* CPedIntelligence::GetTaskDuck(bool arg1) 0x6010A0 
CTaskSimpleDuck* CPedIntelligence::GetTaskDuck(bool arg1) {
	return plugin::CallMethodAndReturn<CTaskSimpleDuck*, 0x6010A0, CPedIntelligence *, bool>(this, arg1);
}