#pragma once

struct CColLighting
{
	unsigned char day : 4;    // 0-15
	unsigned char night : 4;  // 0-15
};


struct MaterialInfo
{
	unsigned char surfaceType;
	unsigned char pieceType;
	union
	{
		unsigned char lighting;
		CColLighting lightingDetails;
	};
};