/*
Plugin-SDK (Grand Theft Auto San Andreas) header file
Authors: GTA Community. See more here
https://github.com/DK22Pac/plugin-sdk
Do not delete this comment block. Respect others' work!
*/
#pragma once

#include "PluginBase.h"
#include "eCamMode.h"
#include "CVector.h"

/* http://code.google.com/p/mtasa-blue/source/browse/tags/1.3.4/MTA10/game_sa/CCamSA.h */

class CEntity;
class CPed;
class CVehicle;

extern bool &gbFirstPersonRunThisFrame;

enum
{
	CAMERA_DIR_LOOK_BEHIND,
	CAMERA_DIR_LOOK_LEFT,
	CAMERA_DIR_LOOK_RIGHT,
	CAMERA_DIR_LOOK_FORWARD
};

class  CCam {
public:
    bool          m_bBelowMinDist;
    bool          m_bBehindPlayerDesired;
    bool          m_bCamLookingAtVector;
    bool          m_bCollisionChecksOn;
    bool          m_bFixingBeta;
    bool          m_bTheHeightFixerVehicleIsATrain;
    bool          m_bLookBehindCamWasInFront;
    bool          m_bLookingBehind;
    bool          m_bLookingLeft;
    bool          m_bLookingRight;
    bool          m_bResetStatics;
    bool          m_bRotating;
    eCamMode      m_nMode;
private:
    char _pad[2];
public:
    unsigned int  m_nFinishTime;
    unsigned int  m_nDoCollisionChecksOnFrameNum;
    unsigned int  m_nDoCollisionCheckEveryNumOfFrames;
    unsigned int  m_nFrameNumWereAt;
    unsigned int  m_nRunningVectorArrayPos;
    unsigned int  m_nRunningVectorCounter;
    unsigned int  m_nDirectionWasLooking;
    float         m_fMaxRoleAngle;
    float         m_fRoll;
    float         m_fRollSpeed;
    float         m_fSyphonModeTargetZOffSet;
    float         m_fAmountFractionObscured;
    float         m_fAlphaSpeedOverOneFrame;
    float         m_fBetaSpeedOverOneFrame;
    float         m_fBufferedTargetBeta;
    float         m_fBufferedTargetOrientation;
    float         m_fBufferedTargetOrientationSpeed;
    float         m_fCamBufferedHeight;
    float         m_fCamBufferedHeightSpeed;
    float         m_fCloseInPedHeightOffset;
    float         m_fCloseInPedHeightOffsetSpeed;
    float         m_fCloseInCarHeightOffset;
    float         m_fCloseInCarHeightOffsetSpeed;
    float         m_fDimensionOfHighestNearCar;
    float         m_fDistanceBeforeChanges;
    float         m_fFovSpeedOverOneFrame;
    float         m_fMinDistAwayFromCamWhenInterPolating;
    float         m_fPedBetweenCameraHeightOffset;
    float         m_fPlayerInFrontSyphonAngleOffSet;
    float         m_fRadiusForDead;
    float         m_fRealGroundDist;
    float         m_fTargetBeta;
    float         m_fTimeElapsedFloat;
    float         m_fTilt;
    float         m_fTiltSpeed;
    float         m_fTransitionBeta;
    float         m_fTrueBeta;
    float         m_fTrueAlpha;
    float         m_fInitialPlayerOrientation;
    float         m_fVerticalAngle; // alpha
    float         m_fAlphaSpeed;
    float         m_fFOV;
    float         m_fFOVSpeed;
    float         m_fHorizontalAngle; //beta
    float         m_fBetaSpeed;
    float         m_fDistance;
    float         m_fDistanceSpeed;
    float         m_fCaMinDistance;
    float         m_fCaMaxDistance;
    float         m_fSpeedVar;
    float         m_fCameraHeightMultiplier;
    float         m_fTargetZoomGroundOne;
    float         m_fTargetZoomGroundTwo;
    float         m_fTargetZoomGroundThree;
    float         m_fTargetZoomOneZExtra;
    float         m_fTargetZoomTwoZExtra;
    float         m_fTargetZoomTwoInteriorZExtra;
    float         m_fTargetZoomThreeZExtra;
    float         m_fTargetZoomZCloseIn;
    float         m_fMinRealGroundDist;
    float         m_fTargetCloseInDist;
    float         m_fBeta_Targeting;
    float         m_fX_Targetting;
    float         m_fY_Targetting;
    CVehicle     *m_pCarWeAreFocussingOn;
    CVehicle     *m_pCarWeAreFocussingOnI;
    float         m_fCamBumpedHorz;
    float         m_fCamBumpedVert;
    unsigned int  m_nCamBumpedTime;
    CVector       m_vecSourceSpeedOverOneFrame;
    CVector       m_vecTargetSpeedOverOneFrame;
    CVector       m_vecUpOverOneFrame;
    CVector       m_vecTargetCoorsForFudgeInter;
    CVector       m_vecCamFixedModeVector;
    CVector       m_vecCamFixedModeSource;
    CVector       m_vecCamFixedModeUpOffSet;
    CVector       m_vecLastAboveWaterCamPosition;
    CVector       m_vecBufferedPlayerBodyOffset;
    CVector       m_vecFront;
    CVector       m_vecSource;
    CVector       m_vecSourceBeforeLookBehind;
    CVector       m_vecUp;
    CVector       m_avecPreviousVectors[2];
    CVector       m_avecTargetHistoryPos[4];
    unsigned int  m_anTargetHistoryTime[4];
    unsigned int  m_nCurrentHistoryPoints;
    CEntity      *m_pCamTargetEntity;
    float         m_fCameraDistance;
    float         m_fIdealAlpha;
    float         m_fPlayerVelocity;
    CVehicle     *m_pLastCarEntered;
    CPed         *m_pLastPedLookedAt;
    bool          m_bFirstPersonRunAboutActive;

	void CacheLastSettingsDWCineyCam();
	void DoAverageOnVector(CVector const&);
	void DoCamBump(float, float);
	void Finalise_DW_CineyCams(CVector*, CVector*, float, float, float, float);
	bool GetBoatLook_L_R_HeightOffset(float&);
	void GetCoreDataForDWCineyCamMode(CEntity**, CVehicle**, CVector*, CVector*, CVector*, CVector*, CVector*, CVector*, float*, CVector*, float*, class CColSphere*);
	void GetLookAlongGroundPos(CEntity*, CPed*, CVector&, CVector&);
	void GetLookFromLampPostPos(CEntity*, CPed*, CVector&, CVector&);
	void GetLookOverShoulderPos(CEntity*, CPed*, CVector&, CVector&);
	bool GetWeaponFirstPersonOn();
	void Get_TwoPlayer_AimVector(CVector&);
	void Init();
	bool IsTargetInWater(CVector const&);
	bool IsTimeToExitThisDWCineyCamMode(int, CVector*, CVector*, float, bool);
	void KeepTrackOfTheSpeed(
		CVector const & aSource,
		CVector const & aTargetCoorsForFudgeInter,
		CVector const & aUp,
		float const & aTrueAlpha,
		float const & aTrueBeta,
		float const & aFov);
	void LookBehind();
	bool LookLeft();
	bool LookRight(bool bLookRight);
	void Process();
	void ProcessArrestCamOne();
	void ProcessArrestCamTwo();
	void ProcessDWBustedCam1(CPed*, bool);
	void ProcessPedsDeadBaby();
	void Process_1rstPersonPedOnPC(CVector const&, float, float, float);
	void Process_1stPerson(CVector const&, float, float, float);
	void Process_AimWeapon(CVector const&, float, float, float);
	void Process_AttachedCam();
	void Process_BehindCar(CVector const&, float, float, float);
	void Process_Cam_TwoPlayer();
	void Process_Cam_TwoPlayer_CalcSource(float, CVector*, CVector*, CVector*);
	void Process_Cam_TwoPlayer_InCarAndShooting();
	void Process_Cam_TwoPlayer_Separate_Cars();
	void Process_Cam_TwoPlayer_Separate_Cars_TopDown();
	void Process_Cam_TwoPlayer_TestLOSs(CVector);
	void Process_DW_BirdyCam(bool);
	void Process_DW_CamManCam(bool);
	void Process_DW_DogFightCam(bool);
	void Process_DW_FishCam(bool);
	void Process_DW_HeliChaseCam(bool);
	void Process_DW_PlaneCam1(bool);
	void Process_DW_PlaneCam2(bool);
	void Process_DW_PlaneCam3(bool);
	void Process_DW_PlaneSpotterCam(bool);
	void Process_Editor(CVector const&, float, float, float);
	void Process_Fixed(CVector const&, float, float, float);
	void Process_FlyBy(CVector const&, float, float, float);    
	void Process_FollowCar_SA(CVector const&, float, float, float, bool);
	void Process_FollowPed(CVector const&, float, float, float);
	void Process_M16_1stPerson(CVector const&, float, float, float);
	void Process_Rocket(CVector const&, float, float, float, bool);
	void Process_SpecialFixedForSyphon(CVector const&, float, float, float);
	void Process_WheelCam(CVector const&, float, float, float);
	void RotCamIfInFrontCar(CVector const&, float);
	bool Using3rdPersonMouseCam();

	void Process_FollowPedWithMouse(CVector const & vecPos, float fAngle, float fSpeed, float);
	void Process_FollowPed_SA(CVector const & vecPos, float fAngle, float fSpeed, float, bool);
	void GetVectorsReadyForRW(void);

	static void InjectHooks();
protected:
	void NormalizeHorizontalAngle();
};

VALIDATE_SIZE(CCam, 0x238);
