#include "StdInc.h"

void CTaskComplexEnterCar::GetCameraStickModifier(CPed* pPed, float f1, float& f2, float& f3, float& f4, float& f5)
{
	plugin::CallMethod<0x63A380, CTaskComplexEnterCar*, CPed*, float, float&, float&, float&, float&>(
		this, pPed, f1, f2, f3, f4, f5
		);
}