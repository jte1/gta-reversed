#include "StdInc.h"
#include "CMemoryMgr.h"

CCollisionData::CCollisionData(void) {
	_Constructor();
}

CCollisionData * CCollisionData::_Constructor(void) {
	this->m_nNumSpheres = 0;
	this->m_nNumBoxes = 0;
	this->m_nNumTriangles = 0;
	this->m_nNumLinesOrDisks = 0;
	this->m_nFlags.bUsesDisks = false;
	this->m_nFlags.bNotEmpty = false;
	this->m_nFlags.b03 = false;
	this->m_pSpheres = nullptr;
	this->m_pBoxes = nullptr;
	this->m_pLinesOrDisks = nullptr;
	this->m_pVertices = nullptr;
	this->m_pTriangles = nullptr;
	this->m_pTrianglePlanes = nullptr;
	this->m_nNumShadowTriangles = 0;
	this->m_nNumShadowVertices = 0;
	this->m_pShadowVertices = nullptr;
	this->m_pShadowTriangles = nullptr;
	return this;
}

void CCollisionData::Copy(CCollisionData const & refData) {
	if (refData.m_nNumSpheres)
	{
		if (this->m_nNumSpheres != refData.m_nNumSpheres)
		{
			this->m_nNumSpheres = refData.m_nNumSpheres;

			if (this->m_pSpheres)
				CMemoryMgr::Free(this->m_pSpheres);

			this->m_pSpheres = (CColSphere *)CMemoryMgr::Malloc(sizeof(CColSphere) * this->m_nNumSpheres);
		}

		for (int i = 0; i < this->m_nNumSpheres; ++i)
		{
			memcpy(&this->m_pSpheres[i], &refData.m_pSpheres[i], sizeof(refData.m_pSpheres[i]));
		}
	}
	else
	{
		this->m_nNumSpheres = 0;
		if (this->m_pSpheres)
			CMemoryMgr::Free(this->m_pSpheres);
		this->m_pSpheres = 0;
	}

	if (refData.m_nNumLinesOrDisks)
	{
		if (refData.m_nFlags.bUsesDisks)
		{
			if (this->m_nNumLinesOrDisks != refData.m_nNumLinesOrDisks || !this->m_nFlags.bUsesDisks)
			{
				this->m_nNumDisks = refData.m_nNumLinesOrDisks;
				if (this->m_pLinesOrDisks)
					CMemoryMgr::Free(this->m_pLinesOrDisks);
				this->m_pDisks = (CColDisk *)CMemoryMgr::Malloc(sizeof(CColDisk) * this->m_nNumDisks);
			}

			for (int i = 0; i < this->m_nNumDisks; ++i)
			{
				memcpy(&this->m_pDisks[i], &refData.m_pDisks[i], sizeof(this->m_pDisks[i]));
			}
		}
		else
		{
			if (this->m_nNumLinesOrDisks != refData.m_nNumLinesOrDisks || this->m_nFlags.bUsesDisks)
			{
				this->m_nNumLines = refData.m_nNumLinesOrDisks;

				if (this->m_pLinesOrDisks)
					CMemoryMgr::Free(this->m_pLinesOrDisks);

				this->m_pLines = (CColLine *)CMemoryMgr::Malloc(sizeof(CColLine) * this->m_nNumLines);
			}
			
			for (int i = 0; i < this->m_nNumLines; ++i)
			{
				memcpy(&this->m_pLines[i], &refData.m_pLines[i], sizeof(refData.m_pLines[i]));
			}
		}
	}
	else
	{
		this->m_nNumLinesOrDisks = 0;
		if (this->m_pLinesOrDisks)
			CMemoryMgr::Free(this->m_pLinesOrDisks);
		this->m_pLinesOrDisks = 0;
	}

	unsigned char * pThisFlags = (unsigned char*)&this->m_nFlags;
	unsigned char * pOtherFlags = (unsigned char*)&refData.m_nFlags;

	*pThisFlags = (*pThisFlags ^ *pOtherFlags) & 1 ^ *pThisFlags;
	*pThisFlags = *pThisFlags ^ (*pThisFlags ^ *pOtherFlags) & 4;

	if (refData.m_nNumBoxes)
	{
		if (this->m_nNumBoxes != refData.m_nNumBoxes)
		{
			this->m_nNumBoxes = refData.m_nNumBoxes;
			if (this->m_pBoxes)
				CMemoryMgr::Free(this->m_pBoxes);
			this->m_pBoxes = (CColBox *)CMemoryMgr::Malloc(sizeof(CColBox) * this->m_nNumBoxes);
		}

		for (int i = 0; i < this->m_nNumBoxes; ++i)
		{
			this->m_pBoxes[i] = refData.m_pBoxes[i];
		}
	}
	else
	{
		this->m_nNumBoxes = 0;
		if (this->m_pBoxes)
			CMemoryMgr::Free(this->m_pBoxes);
		this->m_pBoxes = 0;
	}

	if (refData.m_nNumTriangles == 0)
	{
		this->m_nNumTriangles = 0;
		if (this->m_pTriangles)
			CMemoryMgr::Free(this->m_pTriangles);
		this->m_pTriangles = 0;

		if (this->m_pVertices)
			CMemoryMgr::Free(this->m_pVertices);
		this->m_pVertices = 0;
	}
	else
	{
		int maxIndex = 0;

		for (int i = 0; i < refData.m_nNumTriangles; ++i)
		{
			if (refData.m_pTriangles[i].m_nVertA > maxIndex)
				maxIndex = refData.m_pTriangles[i].m_nVertA;
			if (refData.m_pTriangles[i].m_nVertB > maxIndex)
				maxIndex = refData.m_pTriangles[i].m_nVertB;
			if (refData.m_pTriangles[i].m_nVertC > maxIndex)
				maxIndex = refData.m_pTriangles[i].m_nVertC;
		}

		maxIndex++;

		if (this->m_pVertices)
			CMemoryMgr::Free(this->m_pVertices);

		if (maxIndex)
		{
			this->m_pVertices = (CompressedVector *)CMemoryMgr::Malloc(sizeof(CompressedVector) * maxIndex);

			for (int i = 0; i < maxIndex; ++i)
			{
				this->m_pVertices[i] = refData.m_pVertices[i];
			}
		}

		if (this->m_nNumTriangles != refData.m_nNumTriangles)
		{
			this->m_nNumTriangles = refData.m_nNumTriangles;
			if (this->m_pTriangles)
				CMemoryMgr::Free(this->m_pTriangles);
			this->m_pTriangles = (CColTriangle *)CMemoryMgr::Malloc(sizeof(CColTriangle) * this->m_nNumTriangles);
		}

		for (int i = 0; i < this->m_nNumTriangles; ++i)
		{
			this->m_pTriangles[i] = refData.m_pTriangles[i];
		}
	}
	
	if (refData.m_nNumShadowTriangles && refData.m_nFlags.b03)
	{
		int maxIndex = 0;
		for (int i = 0; i < refData.m_nNumShadowTriangles; ++i)
		{
			if (refData.m_pShadowTriangles[i].m_nVertA > maxIndex)
				maxIndex = refData.m_pShadowTriangles[i].m_nVertA;
			if (refData.m_pShadowTriangles[i].m_nVertB > maxIndex)
				maxIndex = refData.m_pShadowTriangles[i].m_nVertB;
			if (refData.m_pShadowTriangles[i].m_nVertC > maxIndex)
				maxIndex = refData.m_pShadowTriangles[i].m_nVertC;
		}

		maxIndex++;

		if (this->m_pShadowVertices)
			CMemoryMgr::Free(this->m_pShadowVertices);

		if (maxIndex)
		{
			this->m_pShadowVertices = (CompressedVector *)CMemoryMgr::Malloc(sizeof(CompressedVector) * maxIndex);
			
			for (int i = 0; i < maxIndex; ++i)
			{
				this->m_pShadowVertices[i] = refData.m_pShadowVertices[i];
			}
		}

		if (this->m_nNumShadowTriangles != refData.m_nNumShadowTriangles)
		{
			this->m_nNumShadowTriangles = refData.m_nNumShadowTriangles;
			if (this->m_pShadowTriangles)
				CMemoryMgr::Free(this->m_pShadowTriangles);
			this->m_pShadowTriangles = (CColTriangle *)CMemoryMgr::Malloc(sizeof(CColTriangle) * this->m_nNumShadowTriangles);
		}

		for (int i = 0; i < this->m_nNumShadowTriangles; ++i)
		{
			this->m_pShadowTriangles[i] = refData.m_pShadowTriangles[i];
		}

		this->m_nNumShadowVertices = refData.m_nNumShadowVertices;
	}
	else
	{
		this->m_nNumShadowTriangles = 0;
		this->m_nNumShadowVertices = 0;

		if (this->m_pShadowTriangles)
			CMemoryMgr::Free(this->m_pShadowTriangles);
		this->m_pShadowTriangles = 0;

		if (this->m_pShadowVertices)
			CMemoryMgr::Free(this->m_pShadowVertices);
		this->m_pShadowVertices = 0;
	}
}

void CCollisionData::RemoveTrianglePlanes(void) {
	if (this->m_pTrianglePlanes) {
		CMemoryMgr::Free(this->m_pTrianglePlanes);
	}
	this->m_pTrianglePlanes = nullptr;
}

// EU-1.00 @ 0x0040F070
void CCollisionData::RemoveCollisionVolumes(void) {
	if (this->m_pSpheres)
		CMemoryMgr::Free(this->m_pSpheres);
	if (this->m_pLinesOrDisks)
		CMemoryMgr::Free(this->m_pLinesOrDisks);
	if (this->m_pBoxes)
		CMemoryMgr::Free(this->m_pBoxes);
	if (this->m_pVertices)
		CMemoryMgr::Free(this->m_pVertices);
	if (this->m_pTriangles)
		CMemoryMgr::Free(this->m_pTriangles);
	if (this->m_pShadowTriangles)
		CMemoryMgr::Free(this->m_pShadowTriangles);
	if (this->m_pShadowVertices)
		CMemoryMgr::Free(this->m_pShadowVertices);

	CCollision::RemoveTrianglePlanes_(this);

	this->m_nNumSpheres = 0;
	this->m_nNumLinesOrDisks = 0;
	this->m_nNumBoxes = 0;
	this->m_nNumTriangles = 0;
	this->m_pSpheres = nullptr;
	this->m_pLinesOrDisks = nullptr;
	this->m_pBoxes = nullptr;
	this->m_pVertices = nullptr;
	this->m_pTriangles = nullptr;
	this->m_pShadowTriangles = nullptr;
	this->m_pShadowVertices = nullptr;
}

CLink<CCollisionData *> * CCollisionData::GetLinkPtr(void) {
	unsigned int addr = PtrToUint(&this->m_pTrianglePlanes[this->m_nNumTriangles]);
	uintptr_t * ptr = (uintptr_t *)((addr + 3) & ~3);
	CLink<CCollisionData *> ** ppLink = reinterpret_cast<CLink<CCollisionData *> **>(ptr);
	return *ppLink;
}

void CCollisionData::SetLinkPtr(CLink<CCollisionData *> * pPtr)
{
	unsigned int addr = PtrToUint(&this->m_pTrianglePlanes[this->m_nNumTriangles]);
	uintptr_t * ptr = (uintptr_t *)((addr + 3) & ~3);
	CLink<CCollisionData *> ** ppLink = reinterpret_cast<CLink<CCollisionData *> **>(ptr);
	*ppLink = pPtr;
}

void CCollisionData::CalculateTrianglePlanes(void)
{
	// last slot is used for link ptr
	this->m_pTrianglePlanes = (CColTrianglePlane *)CMemoryMgr::Malloc(sizeof(CColTrianglePlane) * (this->m_nNumTriangles + 1));

	for (int i = 0; i < this->m_nNumTriangles; ++i)
	{
		this->m_pTrianglePlanes[i].Set(this->m_pVertices, this->m_pTriangles[i]);
	}
}

void CCollisionData::GetTrianglePoint(CVector & vecOut, int pointId) const
{
	vecOut = _DecompressVector<128>(this->m_pVertices[pointId]);
}

void CCollisionData::GetShadTrianglePoint(CVector & vecOut, int pointId) const
{
	vecOut = _DecompressVector<128>(this->m_pShadowVertices[pointId]);
}

void CCollisionData::InjectHooks()
{
	//InjectHook(0x40F030, &CCollisionData::_Constructor, PATCH_JUMP);
	InjectHook(0x40F070, &CCollisionData::RemoveCollisionVolumes, PATCH_JUMP);
	InjectHook(0x40F120, &CCollisionData::Copy, PATCH_JUMP);
	InjectHook(0x40F590, &CCollisionData::CalculateTrianglePlanes, PATCH_JUMP);
	InjectHook(0x40F5E0, &CCollisionData::GetTrianglePoint, PATCH_JUMP);
	InjectHook(0x40F640, &CCollisionData::GetShadTrianglePoint, PATCH_JUMP);
	InjectHook(0x40F6A0, &CCollisionData::RemoveTrianglePlanes, PATCH_JUMP);
	InjectHook(0x40F6C0, &CCollisionData::SetLinkPtr, PATCH_JUMP);
	InjectHook(0x40F6E0, &CCollisionData::GetLinkPtr, PATCH_JUMP);
}