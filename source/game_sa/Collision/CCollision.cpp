#include "StdInc.h"

CLinkList<CCollisionData *> &CCollision::ms_colModelCache = *(CLinkList < CCollisionData *> *)0x96592C;
int &CCollision::ms_collisionInMemory = *(int *)0x9655D4;
bool &CCollision::bCamCollideWithBuildings = *(bool *)0x8A5B16;
bool &CCollision::bCamCollideWithObjects = *(bool *)0x8A5B15;
bool &CCollision::bCamCollideWithPeds = *(bool *)0x8A5B17;
bool &CCollision::bCamCollideWithVehicles = *(bool *)0x8A5B14;
int &CCollision::ms_iProcessLineNumCrossings = *(int *)0x9655D0;
float &CCollision::relVelCamCollisionVehiclesSqr = *(float *)0x8A5B18;

void CCollision::InjectHooks()
{
	InjectHook(0x417950, &CCollision::ProcessLineOfSight, PATCH_JUMP);
	InjectHook(0x416400, &CCollision::RemoveTrianglePlanes_, PATCH_JUMP);
	InjectHook(0x416330, &CCollision::CalculateTrianglePlanes_, PATCH_JUMP);
	InjectHook(0x412C70, &CCollision::TestLineBox, PATCH_JUMP);
	InjectHook(0x417470, &CCollision::TestLineSphere, PATCH_JUMP);
	InjectHook(0x411E70, &CCollision::TestSphereSphere, PATCH_JUMP);
	InjectHook(0x413080, &CCollision::TestVerticalLineBox, PATCH_JUMP);
	InjectHook(0x4120C0, &CCollision::TestSphereBox, PATCH_JUMP);
	InjectHook(0x417BF0, &CCollision::ProcessVerticalLine, PATCH_JUMP);

	CCollisionData::InjectHooks();
}

bool CCollision::ProcessLineOfSight(CColLine const & colLine,
	CMatrix const & matEntity,
	CColModel & entityColModel,
	CColPoint & hitPoint,
	float & maxDepth,
	bool bIgnoreSeeThroughSurfaces,
	bool bIgnoreShootThroughSurfaces)
{
#ifdef USE_DEFAULT_FUNCTIONS
	return plugin::CallAndReturn<bool, 0x417950, CColLine const&, CMatrix const&, CColModel&, ColPoint&, float&, bool, bool>(colLine, matEntity, entityColModel, hitPoint, maxDepth, bIgnoreSeeThroughSurfaces, bIgnoreShootThroughSurfaces);
#else
	static CMatrix matInverted(plugin::dummy);

	CCollisionData * pCollisionData = entityColModel.m_pColData;

	if (!pCollisionData)
		return false;

	matInverted = Invert(matEntity, matInverted);

	CVector vecStart = matInverted * colLine.m_vecStart;
	CVector vecEnd = matInverted * colLine.m_vecEnd;
	CColLine entityColLine(vecStart, vecEnd);

	if (!CCollision::TestLineBox(entityColLine, entityColModel.m_boundBox))
		return 0;

	float depth = maxDepth;

	for (int i = 0; i < pCollisionData->m_nNumSpheres; ++i)
	{
		if (bIgnoreSeeThroughSurfaces && g_surfaceInfos.IsSeeThrough(pCollisionData->m_pSpheres[i].material.surfaceType))
			continue;
		if (bIgnoreShootThroughSurfaces && g_surfaceInfos.IsShootThrough(pCollisionData->m_pSpheres[i].material.surfaceType))
			continue;

		CCollision::ProcessLineSphere(entityColLine, pCollisionData->m_pSpheres[i], hitPoint, depth);
	}

	for (int i = 0; i < pCollisionData->m_nNumBoxes; ++i)
	{
		if (bIgnoreSeeThroughSurfaces && g_surfaceInfos.IsSeeThrough(pCollisionData->m_pBoxes[i].material.surfaceType))
			continue;
		if (bIgnoreShootThroughSurfaces && g_surfaceInfos.IsShootThrough(pCollisionData->m_pBoxes[i].material.surfaceType))
			continue;

		CCollision::ProcessLineBox(entityColLine, pCollisionData->m_pBoxes[i], hitPoint, depth);
	}

	CCollision::CalculateTrianglePlanes_(pCollisionData);

	for (int i = 0; i < pCollisionData->m_nNumTriangles; ++i)
	{
		if (bIgnoreSeeThroughSurfaces && g_surfaceInfos.IsSeeThrough(pCollisionData->m_pTriangles[i].m_nMaterial))
			continue;
		if (bIgnoreShootThroughSurfaces && g_surfaceInfos.IsShootThrough(pCollisionData->m_pTriangles[i].m_nMaterial))
			continue;

		if (CCollision::ProcessLineTriangle(
			entityColLine,
			pCollisionData->m_pVertices,
			pCollisionData->m_pTriangles[i],
			pCollisionData->m_pTrianglePlanes[i],
			hitPoint,
			depth,
			0))
		{
			++CCollision::ms_iProcessLineNumCrossings;
		}
	}

	if (depth >= maxDepth)
		return false;

	hitPoint.m_vecPoint = matEntity * hitPoint.m_vecPoint;
	hitPoint.m_vecNormal = Multiply3x3(matEntity, hitPoint.m_vecNormal);

	maxDepth = depth;

	return true;
#endif
}


unsigned int _calculate_flags(float a, float b, float c, float d, float e, float f)
{
	unsigned int flags = 0x00;
	if (a < 0) flags |= 0x01;
	if (b < 0) flags |= 0x02;
	if (c < 0) flags |= 0x20;
	if (d < 0) flags |= 0x10;
	if (e < 0) flags |= 0x08;
	if (f < 0) flags |= 0x04;
	return flags;
}

bool CCollision::TestLineBox(CColLine const& line, CBox const& box)
{
#ifdef USE_DEFAULT_FUNCTIONS
	return plugin::CallAndReturn<bool, 0x417950, CColLine const&, CBox const&>(line, box);
#else
		unsigned int v2; // eax
		unsigned int v3; // edx
		float v6; // ST34_4
		float v7; // ST38_4
		float v8; // ST3C_4
		float v9; // ST40_4
		float v10; // ST44_4
		float v11; // ST48_4
		char v12; // al
		double v13; // st7
		double v14; // st7
		double v15; // st7
		double v16; // st7
		double v17; // st7
		double v18; // st7
		float v20; // [esp+0h] [ebp-44h]
		float v21; // [esp+0h] [ebp-44h]
		float v22; // [esp+0h] [ebp-44h]
		float v23; // [esp+0h] [ebp-44h]
		float v24; // [esp+4h] [ebp-40h]
		float v25; // [esp+4h] [ebp-40h]
		float v26; // [esp+4h] [ebp-40h]
		float v27; // [esp+8h] [ebp-3Ch]
		float v28; // [esp+Ch] [ebp-38h]
		float v29; // [esp+10h] [ebp-34h]
		float v30; // [esp+14h] [ebp-30h]
		float v31; // [esp+18h] [ebp-2Ch]
		float v32; // [esp+1Ch] [ebp-28h]
		float v33; // [esp+20h] [ebp-24h]
		float v34; // [esp+24h] [ebp-20h]
		float v35; // [esp+28h] [ebp-1Ch]
		float v36; // [esp+48h] [ebp+4h]
		float v37; // [esp+48h] [ebp+4h]
		float v38; // [esp+48h] [ebp+4h]
		float v39; // [esp+48h] [ebp+4h]
		float v40; // [esp+4Ch] [ebp+8h]
		float v41; // [esp+4Ch] [ebp+8h]
		float v42; // [esp+4Ch] [ebp+8h]
		float v43; // [esp+4Ch] [ebp+8h]

		v30 = box.m_vecMax.x - line.m_vecStart.x;
		v31 = box.m_vecMax.y - line.m_vecStart.y;
		v32 = box.m_vecMax.z - line.m_vecStart.z;

		v33 = line.m_vecStart.x - box.m_vecMin.x;
		v34 = line.m_vecStart.y - box.m_vecMin.y;
		v35 = line.m_vecStart.z - box.m_vecMin.z;

		v6 = box.m_vecMax.x - line.m_vecEnd.x;
		v7 = box.m_vecMax.y - line.m_vecEnd.y;
		v8 = box.m_vecMax.z - line.m_vecEnd.z;

		v3 = _calculate_flags(v33, v30, v32, v35, v31, v34);

		v9 = line.m_vecEnd.x - box.m_vecMin.x;
		v10 = line.m_vecEnd.y - box.m_vecMin.y;
		v11 = line.m_vecEnd.z - box.m_vecMin.z;

		v2 = _calculate_flags(v9, v6, v8, v11, v7, v10);
		
		if ((v2 & v3) != 0)
			return false;
		
		if (0 == (v3 * v2))
			return true;

		v12 = v3 | v2;

		CVector lineDir = line.m_vecEnd - line.m_vecStart;//v27,v28,v29

		v27 = line.m_vecEnd.x - line.m_vecStart.x;
		v28 = line.m_vecEnd.y - line.m_vecStart.y;
		v29 = line.m_vecEnd.z - line.m_vecStart.z;
		if (v12 & 3)
		{
			v24 = 1.0 / v27;
			if (v12 & 1)
			{
				v13 = -(v33 * v24);
				v20 = v28 * v13 + line.m_vecStart.y;
				v40 = v29 * v13 + line.m_vecStart.z;
				if (v20 > box.m_vecMin.y
					&& v20 < box.m_vecMax.y
					&& v40 >box.m_vecMin.z
					&& v40 < box.m_vecMax.z)
				{
					return true;
				}
			}
			if (v12 & 2)
			{
				v14 = v30 * v24;
				v21 = v28 * v14 + line.m_vecStart.y;
				v41 = v29 * v14 + line.m_vecStart.z;
				if (v21 > box.m_vecMin.y
					&& v21 < box.m_vecMax.y
					&& v41 >box.m_vecMin.z
					&& v41 < box.m_vecMax.z)
				{
					return true;
				}
			}
		}
		if (v12 & 0xC)
		{
			v25 = 1.0 / v28;
			if (v12 & 4)
			{
				v15 = -(v34 * v25);
				v36 = v27 * v15 + line.m_vecStart.x;
				v42 = v29 * v15 + line.m_vecStart.z;
				if (v36 > box.m_vecMin.x
					&& v36 < box.m_vecMax.x
					&& v42 >box.m_vecMin.z
					&& v42 < box.m_vecMax.z)
				{
					return true;
				}
			}
			if (v12 & 8)
			{
				v16 = v31 * v25;
				v37 = v27 * v16 + line.m_vecStart.x;
				v43 = v29 * v16 + line.m_vecStart.z;
				if (v37 > box.m_vecMin.x
					&& v37 < box.m_vecMax.x
					&& v43 >box.m_vecMin.z
					&& v43 < box.m_vecMax.z)
				{
					return true;
				}
			}
		}
		if (v12 & 0x30
			&& ((v26 = 1.0 / v29, v12 & 0x10)
				&& (v17 = -(v35 * v26),
					v38 = v27 * v17 + line.m_vecStart.x,
					v22 = v28 * v17 + line.m_vecStart.y,
					v38 > box.m_vecMin.x)
				&& v38 < box.m_vecMax.x
				&& v22 >box.m_vecMin.y
				&& v22 < box.m_vecMax.y
				|| v12 & 0x20
				&& (v18 = v32 * v26, v39 = v27 * v18 + line.m_vecStart.x,
					v23 = v28 * v18 + line.m_vecStart.y,
					v39 > box.m_vecMin.x)
				&& v39 < box.m_vecMax.x
				&& v23 >box.m_vecMin.y
				&& v23 < box.m_vecMax.y))
		{
			return true;
		}
		else
		{
			return false;
		}
#endif
}

bool CCollision::ProcessLineSphere(CColLine const& line, CColSphere const& sphere, CColPoint& colPoint, float& depth)
{
	return plugin::CallAndReturn<bool, 0x412AA0, CColLine const&, CColSphere const&, CColPoint&, float&>(
		line, sphere, colPoint, depth);
}

bool CCollision::ProcessLineBox(CColLine const& line, CColBox const& box, CColPoint& colPoint, float& maxTouchDistance)
{
	return plugin::CallAndReturn<bool, 0x413100, CColLine const&, CColBox const&, CColPoint&, float&>(
		line, box, colPoint, maxTouchDistance);
}

bool CCollision::ProcessLineTriangle(CColLine const& line, CompressedVector const* verts, CColTriangle const& tri, CColTrianglePlane const& triPlane, CColPoint& colPoint, float& maxTouchDistance, CStoredCollPoly* collPoly)
{
	return plugin::CallAndReturn<bool, 0x4140F0, CColLine const&, CompressedVector const*, CColTriangle const&, CColTrianglePlane const&, CColPoint&, float&, CStoredCollPoly*>(
		line, verts, tri, triPlane, colPoint, maxTouchDistance, collPoly);
}

void CCollision::RemoveTrianglePlanes_(CCollisionData* colData)
{
	if (!colData->m_pTrianglePlanes)
		return;

	CLink<CCollisionData *> * pLink = colData->GetLinkPtr();

	CCollision::ms_colModelCache.InsertIntoFree(pLink);
	
	colData->RemoveTrianglePlanes();
}

void CCollision::CalculateTrianglePlanes_(CCollisionData* colData)
{
	if (!colData->m_nNumTriangles)
	{
		return;
	}

	if (colData->m_pTrianglePlanes)
	{
		CLink<CCollisionData *> * pLink = colData->GetLinkPtr();

		CCollision::ms_colModelCache.InsertIntoUsed(pLink);
	}
	else
	{
		CLink<CCollisionData *> * pLinkPtr = CCollision::ms_colModelCache.Insert(colData);

		if (!pLinkPtr)
		{
			CLink<CCollisionData *> * pLastUsed = CCollision::ms_colModelCache.usedListTail.prev;
			pLastUsed->data->RemoveTrianglePlanes();

			CCollision::ms_colModelCache.InsertIntoFree(pLastUsed);

			pLinkPtr = CCollision::ms_colModelCache.Insert(colData);
		}
		colData->CalculateTrianglePlanes();
		colData->SetLinkPtr(pLinkPtr);
	}
}

bool CCollision::TestLineSphere(CColLine const & colLine, CColSphere const & colSphere)
{
	// info: https://en.wikipedia.org/wiki/Line%E2%80%93sphere_intersection

	CVector lineDir = colLine.m_vecEnd - colLine.m_vecStart;
	float lineLength = lineDir.Magnitude();

	CVector lineToSphere = colSphere.m_vecCenter - colLine.m_vecStart;

	if (lineLength < 0.000001f)
	{
		return colSphere.m_fRadius * colSphere.m_fRadius >= lineToSphere.MagnitudeSquared();
	}

	float b = DotProduct(lineToSphere, lineDir) * -2.f;
	float lineLengthSq = lineLength * lineLength;

	float discriminant = b * b
		- (DotProduct(colSphere.m_vecCenter, colSphere.m_vecCenter)
			+ DotProduct(colLine.m_vecStart, colLine.m_vecStart)
			- 2.f * DotProduct(colLine.m_vecStart, colSphere.m_vecCenter)
			- colSphere.m_fRadius * colSphere.m_fRadius)
		* lineLengthSq
		* 4.0;

	if (discriminant >= 0.0)
	{
		float dist = (-b - sqrt(discriminant)) / (2.f * lineLengthSq);
		if (dist >= 0.0 && dist <= 1.0)
			return true;
	}
	return false;
}

bool CCollision::TestSphereSphere(CColSphere const & sphereA, CColSphere const & sphereB)
{
	CVector vecCenterDir = sphereA.m_vecCenter - sphereB.m_vecCenter;
	float totalRadius = sphereA.m_fRadius + sphereB.m_fRadius;

	return vecCenterDir.MagnitudeSquared() < totalRadius * totalRadius;
}

bool CCollision::TestVerticalLineBox(CColLine const & colLine, CBox const & box)
{
	if (colLine.m_vecStart.x > box.m_vecMin.x
		&& colLine.m_vecStart.y > box.m_vecMin.y
		&& colLine.m_vecStart.x < box.m_vecMax.x
		&& colLine.m_vecStart.y < box.m_vecMax.y)
	{
		float max;
		float min;
		if (colLine.m_vecStart.z >= colLine.m_vecEnd.z)
		{
			max = colLine.m_vecEnd.z;
			min = colLine.m_vecStart.z;
		}
		else
		{
			max = colLine.m_vecStart.z;
			min = colLine.m_vecEnd.z;
		}
		if (max <= box.m_vecMax.z && min >= box.m_vecMin.z)
			return true;
	}
	return false;
}

bool CCollision::TestSphereBox(CSphere const & sphere, CBox const & box)
{
	return sphere.m_vecCenter.x + sphere.m_fRadius >= box.m_vecMin.x
		&& sphere.m_vecCenter.x - sphere.m_fRadius <= box.m_vecMax.x
		&& sphere.m_vecCenter.y + sphere.m_fRadius >= box.m_vecMin.y
		&& sphere.m_vecCenter.y - sphere.m_fRadius <= box.m_vecMax.y
		&& sphere.m_vecCenter.z + sphere.m_fRadius >= box.m_vecMin.z
		&& sphere.m_vecCenter.z - sphere.m_fRadius <= box.m_vecMax.z;
}

bool CCollision::ProcessVerticalLine(CColLine const & colLine,
	CMatrix const & matTarget,
	CColModel & targetColModel,
	CColPoint & colPoint,
	float & maxDepth,
	bool bIgnoreSeeThroughSurfaces,
	bool,
	CStoredCollPoly * pCollPoly)
{
	static CStoredCollPoly tempCollPoly;
	CCollisionData * pColData = targetColModel.m_pColData;

	if (!pColData)
		return false;

	CVector startToTarget = colLine.m_vecStart - matTarget.pos;
	CVector start;

	start.x = DotProduct(startToTarget, matTarget.right);
	start.y = DotProduct(startToTarget, matTarget.up);
	start.z = DotProduct(startToTarget, matTarget.at);

	CVector endToTarget = colLine.m_vecEnd - matTarget.pos;
	CVector end;

	end.x = DotProduct(endToTarget, matTarget.right);
	end.y = DotProduct(endToTarget, matTarget.up);
	end.z = DotProduct(endToTarget, matTarget.at);

	CColLine a1(start, end);

	if (!CCollision::TestLineBox(a1, targetColModel.m_boundBox))
		return 0;

	float depth = maxDepth;

	for (int i = 0; i < pColData->m_nNumSpheres; ++i)
	{
		if (bIgnoreSeeThroughSurfaces
			&& g_surfaceInfos.IsSeeThrough(pColData->m_pSpheres[i].material.surfaceType))
		{
			continue;
		}

		CCollision::ProcessLineSphere(a1, pColData->m_pSpheres[i], colPoint, depth);
	}


	for (int i = 0; i < pColData->m_nNumBoxes; ++i)
	{
		if (bIgnoreSeeThroughSurfaces
			&& g_surfaceInfos.IsSeeThrough(pColData->m_pBoxes[i].material.surfaceType))
		{
			continue;
		}

		CCollision::ProcessLineBox(a1, pColData->m_pBoxes[i], colPoint, depth);
	}

	CCollision::CalculateTrianglePlanes_(pColData);

	tempCollPoly.m_bIsActual = 0;

	for (int i = 0; i < pColData->m_nNumTriangles; ++i)
	{
		if (bIgnoreSeeThroughSurfaces
			&& g_surfaceInfos.IsSeeThrough(pColData->m_pTriangles[i].m_nMaterial))
		{
			continue;
		}

		CCollision::ProcessLineTriangle(
			a1,
			pColData->m_pVertices,
			pColData->m_pTriangles[i],
			pColData->m_pTrianglePlanes[i],
			colPoint,
			depth,
			&tempCollPoly);
	}

	if (depth >= maxDepth)
		return false;

	colPoint.m_vecPoint = matTarget * colPoint.m_vecPoint;
	colPoint.m_vecNormal = Multiply3x3(matTarget, colPoint.m_vecPoint);

	if (tempCollPoly.m_bIsActual && pCollPoly)
	{
		*pCollPoly = tempCollPoly;

		pCollPoly->m_aMeshVertices[0] = matTarget * pCollPoly->m_aMeshVertices[0];
		pCollPoly->m_aMeshVertices[1] = matTarget * pCollPoly->m_aMeshVertices[1];
		pCollPoly->m_aMeshVertices[2] = matTarget * pCollPoly->m_aMeshVertices[2];
	}

	maxDepth = depth;

	return true;
}

static CColSphere g_faceGroupsMatrixArray[128];
static int g_faceGroupsPassed[128];
static CColSphere g_shadowMeshMatrixArray[128];
static int g_shadowMeshPassed[128];
static int dword_967040[64];
static int dword_9666E0[599];

int CCollision::ProcessColModels(
	CMatrix const & pMat1Model, CColModel & pCol1Model,
	CMatrix const & pMat2Model, CColModel & pCol2Model,
	CColPoint * pCol1Point, CColPoint * pCol2Point,
	float *fUnk, bool bUnk) 
{
#if 0
	static CMatrix g_shadowMeshMatrixTransform(plugin::dummy);
	static CMatrix g_faceGroupsMatrixTransform(plugin::dummy);
	
	CCollisionData * pColData1 = pCol1Model.m_pColData;
	CCollisionData * pColData2 = pCol2Model.m_pColData;

	if (!pColData1)
		return 0;

	if (!pColData2)
		return 0;

	g_faceGroupsMatrixTransform = Invert(pMat2Model, g_faceGroupsMatrixTransform);

	g_faceGroupsMatrixTransform *= pMat1Model;

	CColSphere vecCol1Sphere;
	vecCol1Sphere.m_fRadius = pCol1Model.m_boundSphere.m_fRadius;
	vecCol1Sphere.m_vecCenter = g_faceGroupsMatrixTransform * pCol1Model.m_boundSphere.m_vecCenter;

	if (!CCollision::TestSphereBox(vecCol1Sphere, pCol2Model.m_boundBox))
		return 0;

	g_shadowMeshMatrixTransform = Invert(pMat1Model, g_shadowMeshMatrixTransform);

	g_shadowMeshMatrixTransform *= pMat2Model;

	for (int i = 0; i < pColData1->m_nNumSpheres; ++i)
	{
		g_faceGroupsMatrixArray[i] = pColData1->m_pSpheres[i];
		g_faceGroupsMatrixArray[i].m_vecCenter = g_faceGroupsMatrixTransform * g_faceGroupsMatrixArray[i].m_vecCenter;
	}

	int v213 = 0;

	for (int i = 0; i < pColData1->m_nNumSpheres; ++i)
	{
		if (CCollision::TestSphereBox(g_faceGroupsMatrixArray[i], pCol2Model.m_boundBox))
		{
			g_faceGroupsPassed[v213] = i;
			v213++;
		}
	}

	for (int i = 0; i < pColData2->m_nNumSpheres; ++i)
	{
		g_shadowMeshMatrixArray[i] = pColData2->m_pSpheres[i];
		g_shadowMeshMatrixArray[i].m_vecCenter = g_shadowMeshMatrixTransform * g_shadowMeshMatrixArray[i].m_vecCenter;
	}


	int v32 = 0;

	for (int i = 0; i < pColData2->m_nNumSpheres; ++i)
	{
		if (CCollision::TestSphereBox(g_shadowMeshMatrixArray[i], pCol1Model.m_boundBox))
		{
			g_shadowMeshPassed[v32++] = i;
		}
	}

	if (!v213 && !pColData1->m_nNumLinesOrDisks && !v32)
		return 0;

	int v223 = 0;//v36

	if (pColData2->m_nFlags.bUsesDisks && pColData2->m_nNumLinesOrDisks)
	{
		for (int i = 0; i < pColData2->m_nNumDisks; ++i)
		{
			g_shadowMeshMatrixArray[i + pColData2->m_nNumSpheres].m_fRadius = pColData2->m_pDisks[i].m_fStartRadius;
			g_shadowMeshMatrixArray[i + pColData2->m_nNumSpheres].m_vecCenter = g_shadowMeshMatrixTransform * pColData2->m_pDisks[i].m_vecStart;
			g_shadowMeshMatrixArray[i + pColData2->m_nNumSpheres].material = pColData2->m_pDisks[i].material;
		}

		for (int i = 0; i < pColData2->m_nNumDisks && v223 < ARRAYSIZE(g_shadowMeshPassed); ++i)
		{
			if (CCollision::TestSphereBox(g_shadowMeshMatrixArray[i + pColData2->m_nNumSpheres], pCol1Model.m_boundBox))
			{
				g_shadowMeshPassed[v223++] = i;
			}
		}
	}

	int v222 = 0;//v48

	// ARRAYSIZE(dword_967040) = 64
	for (int i = 0; i < pColData2->m_nNumBoxes && v222 < ARRAYSIZE(dword_967040); ++i)
	{
		if (CCollision::TestSphereBox(vecCol1Sphere, pColData2->m_pBoxes[i]))
		{
			dword_967040[v222++] = i;
		}
	}

	pColData2->CalculateTrianglePlanes();


	v53 = ;
	v54 = 0;
	v55 = 0;
	v224 = pColData2->m_pTrianglePlanes;
	v220 = 0;

	if (pColData2->m_bFlags & 2)
	{
		curSphereb = 0;
		for (i = 0; curSphereb < v56; i += 28)
		{
			v56 = pColData2->m_bFlags & 2 ? *(_DWORD *)&pColData2->m_pTriangles[-1].V1_pointIndex : 0;

			v57 = (int)&pColData2->m_pTriangles[-4] - i;

			if (_vecCol1Sphere_x + fCol1Model_Radius >= *(float *)v57
				&& _vecCol1Sphere_x - fCol1Model_Radius <= *(float *)(v57 + 12)
				&& _vecCol1Sphere_y + fCol1Model_Radius >= *(float *)(v57 + 4)
				&& _vecCol1Sphere_y - fCol1Model_Radius <= *(float *)(v57 + 16)
				&& _vecCol1Sphere_z + fCol1Model_Radius >= *(float *)(v57 + 8)
				&& _vecCol1Sphere_z - fCol1Model_Radius <= *(float *)(v57 + 20))
			{
				v58 = *(signed __int16 *)(v57 + 24);
				if (v58 <= *(signed __int16 *)(v57 + 26))
				{
					v59 = v58;
					do
					{
						if (CCollision::TestSphereTriangle(
							(CColSphere *)&_vecCol1Sphere_x,
							pColData2->m_pTrianglePoints,
							(CColTriangle *)&pColData2->m_pTriangles[v58],
							&pColData2->m_pTrianglePlanes[v59]))
						{
							v60 = v220;
							dword_9666E0[v220] = v58;
							v220 = v60 + 1;
							if (v60 + 1 >= 599)
								break;
						}
						++v58;
						++v59;
					} while (v58 <= *(signed __int16 *)(v57 + 26));
				}
			}
			v54 = v220;
			v48 = v222;
			++curSphereb;
		}
	}
	else if (v53 > 0)
	{
		for (int i = 0; i < pColData2->_iTriangles && i < ARRAYSIZE(dword_9666E0); ++i)
		{
			if (CCollision::TestSphereTriangle(
				vecCol1Sphere,
				pColData2->_pVertices,
				pColData2->_pTriangles[i],
				&pColData2->_pTrianglePlanes[i]))
			{
				dword_9666E0[v54++] = i;
			}
		}

		v48 = v222;
		v220 = v54;
	}

	*(float *)&v62 = 0.0;

	if (!v221 && !v48 && !v54)
		return 0;

	v23 = v213 == 0;
	v24 = v213 < 0;
	*(float *)&v211 = 0.0;

	pColPoint1->fDepth = -1.0;

	v205 = 0;
	if (!((unsigned __int8)v24 | (unsigned __int8)v23))
	{
		v63 = g_faceGroupsPassed;
		i = (int)g_faceGroupsPassed;
		do
		{
			v64 = 0;
			v192 = 0;
			v218 = (int)&_pCol1Data->m_pSpheres[*v63];
			*(float *)&a4 = 1.0e24;
			if (v221 > 0)
			{
				while (1)
				{
					v66 = pColData2->m_wNumSpheres;
					v65 = g_shadowMeshPassed[v64];
					if (v65 < v66)
						break;
					if (pColData2->m_bFlags & 1 && pColData2->m_bNumLines)
					{
						v68 = &pColPoint1[v62];
						v70 = v65 - v66;
						v69 = (char *)pColData2->m_pDisks;
						v67 = 9 * v70;
					LABEL_102:
						if (CCollision::ProcessSphereSphere(
							(CColSphere *)&g_faceGroupsMatrixArray[*v63],
							(CColSphere *)&v69[4 * v67],
							v68,
							(float *)&a4))
						{
							v192 = 1;
						}
					}
					if (++v64 >= v221)
						goto LABEL_105;
				}
				v68 = &pColPoint1[v62];
				v69 = (char *)pColData2->m_pSpheres;
				v67 = 5 * v65;
				goto LABEL_102;
			}
		LABEL_105:

			v71 = 0;
			curSpherec = 0;
			if (v222 > 0)
			{
				v72 = &pColPoint1[v62];
				do
				{
					if (CCollision::ProcessSphereBox(
						(CColSphere *)&g_faceGroupsMatrixArray[*v63],
						pColData2->_GetBox(dword_967040[v71]),
						v72,
						(int)&a4))
					{
						v73 = v218 + 16;
						v74 = (int)&v72->ucSurfaceTypeA;
						*(_WORD *)v74 = *(_WORD *)(v218 + 16);
						*(_BYTE *)(v74 + 2) = *(_BYTE *)(v73 + 2);
						if (bUnk && *(_BYTE *)(v218 + 17) <= 2u && v62 < 31)
						{
							++v62;
							++v72;
							v192 = 0;
							*(float *)&a4 = 1.0e24;
							v72->fDepth = -1.0;
						}
						else
						{
							v192 = 1;
						}
					}
					v63 = (int *)i;
					v71 = curSpherec++ + 1;
				} while (curSpherec < v222);
			}

			curSphered = 0;
			if (v220 > 0)
			{
				v75 = &pColPoint1[v62];
				v216 = (int)&pColPoint1[v62];
				v76 = &pColPoint1[v62];
				while (1)
				{
					if (CCollision::ProcessSphereTriangle(
						(int)&g_faceGroupsMatrixArray[*v63],
						(int)pColData2->m_pTrianglePoints,
						(int)&pColData2->m_pTriangles[dword_9666E0[curSphered]],
						(int)&pColData2->m_pTrianglePlanes[dword_9666E0[curSphered]],
						(int)v75,
						(int)&a4))
					{
						if (bUnk && *(_BYTE *)(v218 + 17) <= 2u && v62 < 31)
						{
							++v62;
							++v76;
							v192 = 0;
							*(float *)&a4 = 1.0e24;
							v216 = (int)v76;
							v76->fDepth = -1.0;
						}
						else
						{
							v192 = 1;
						}
					}
					if (++curSphered >= v220)
						break;
					v75 = (CColPoint *)v216;
				}
			}

			if (v192)
			{
				if (v62 >= 31)
					break;
				pColPoint1[++v62].fDepth = -1.0;
			}
			++v63;
			v77 = __OFSUB__(v205 + 1, v213);
			v24 = v205++ + 1 - v213 < 0;
			i = (int)v63;
		} while ((unsigned __int8)v24 ^ v77);

		v211 = v62;
	}


	v78 = _pCol1Data;

	if (pColData1->_nbFlags.bUseDisks)
	{
		for (int i = 0; i < pColData1->_nbDisks; ++i)
		{
			pColData1->_pDisks[i].m_vecStart.z = fUnk[i];
		}

		curSpheree = 0;
		if (v78->m_bNumLines > 0)
		{
			v81 = _pCol1Data;
			v83 = 0;
			v82 = g_faceGroupsMatrixArray;
			do
			{
				v82->sphere.radius = v81->m_pDisks[v83].radius;
				v84 = operator*(&outPoint, &g_faceGroupsMatrixTransform, &v81->m_pDisks[v83].base);
				v82->sphere.center.x = v84->x;
				v82->sphere.center.y = v84->y;
				v82->sphere.center.z = v84->z;
				v85 = (int)&v81->m_pDisks[v83].surfaceTypeA;
				v86 = (int)&v82->surface;
				*(_WORD *)v86 = *(_WORD *)v85;
				*(_BYTE *)(v86 + 2) = *(_BYTE *)(v85 + 2);
				++v83;
				++v82;
				++curSpheree;
			} while (curSpheree < v81->m_bNumLines);
		}

		curSpheref = 0;
		if (_pCol1Data->m_bNumLines > 0)
		{
			v87 = &pColPoint1[v211];
			i = (int)&v87->fDepth;
			v212 = (int)&v87->fDepth;
			v218 = 0;
			a4 = fUnk;
			v213 = (int)&pColPoint2->ucSurfaceTypeA;
			v219 = &g_faceGroupsMatrixArray[0].sphere.radius;
			do
			{
				v88 = (_DWORD *)i;
				*((_BYTE *)&dword_9666D0 + curSpheref) = 0;
				v89 = v219;
				*v88 = -1082130432;
				if (*v89 + *(v89 - 3) >= pCol2Model->boundingBox.vecBoundMin.fX
					&& *(v219 - 3) - *v219 <= pCol2Model->boundingBox.vecBoundMax.fX
					&& *v219 + *(v219 - 2) >= pCol2Model->boundingBox.vecBoundMin.fY
					&& *(v219 - 2) - *v219 <= pCol2Model->boundingBox.vecBoundMax.fY
					&& *v219 + *(v219 - 1) >= pCol2Model->boundingBox.vecBoundMin.fZ
					&& *(v219 - 1) - *v219 <= pCol2Model->boundingBox.vecBoundMax.fZ)
				{
					v91 = v213;
					v90 = (char *)_pCol1Data->m_pDisks + v218;
					v193 = 0;
					*(float *)&v215 = 1.0e24;
					*(float *)&v216 = 1.0e24;
					for (j = 0; j < v221; ++j)
					{
						if (CCollision::ProcessSphereSphere(
							(CColSphere *)(v219 - 3),
							(CColSphere *)&pColData2->m_pSpheres[g_shadowMeshPassed[j]],
							v87,
							(float *)&v215))
						{
							v93 = operator*((RwV3D *)&v243, &g_shadowMeshMatrixTransform, &v87->Position);
							v94 = v93->x;
							v95 = v93->y;
							v96 = v93->z;
							v236 = v94;
							v237 = v95;
							v238 = v96;
							Multiply3x3((RwV3D *)&out, &g_shadowMeshMatrixTransform, &v87->Normal);
							v97 = _pCol1Data->m_pDisks;
							v98 = v237 - *(float *)((char *)&v97->base.y + v218);
							v99 = v236 - *(float *)((char *)&v97->base.x + v218);
							v92 = sqrt(
								*(float *)((char *)&v97->radius + v218) * *(float *)((char *)&v97->radius + v218)
								- v98 * v98
								- v99 * v99)
								+ v238;
							if (v92 >= *(float *)a4)
							{
								v100 = *(float *)&a4;
								*((_BYTE *)&dword_9666D0 + curSpheref) = 1;
								*(float *)LODWORD(v100) = v92;
								CColPoint::operator=((CColPoint *)(v91 - 32), v87);
								*(_DWORD *)(v91 + 8) = *(_DWORD *)i;
								*(_WORD *)v91 = *((_WORD *)v90 + 8);
								*(_BYTE *)(v91 + 2) = v90[18];
							}
							*(float *)&v215 = 1.0e24;
						}
					}
					v207 = 0;
					if (v222 > 0)
					{
						do
						{
							if (CCollision::ProcessSphereBox(
								(CColSphere *)(v219 - 3),
								&pColData2->m_pBoxes[dword_967040[v207]],
								v87,
								(int)&v215))
							{
								v102 = operator*((RwV3D *)&v242, &g_shadowMeshMatrixTransform, &v87->Position);
								v103 = v102->x;
								v104 = v102->y;
								v105 = v102->z;
								v236 = v103;
								v237 = v104;
								v238 = v105;
								v106 = Multiply3x3((RwV3D *)&m, &g_shadowMeshMatrixTransform, &v87->Normal);
								v107 = v106->x;
								v108 = v106->y;
								v109 = v106->z;
								outPoint.x = v107;
								outPoint.z = v109;
								outPoint.y = v108;
								v101 = (float *)((char *)&_pCol1Data->m_pDisks->base.x + v218);
								if (v238 >= (double)v101[2] || outPoint.z <= 0.5)
								{
									if (*(float *)&v215 < (double)*(float *)&v216)
									{
										v216 = v215;
										v113 = (int)&v87->ucSurfaceTypeA;
										*(_WORD *)v113 = *((_WORD *)v90 + 8);
										v193 = 1;
										*(_BYTE *)(v113 + 2) = v90[18];
									}
								}
								else
								{
									v111 = v237 - v101[1];
									v110 = sqrt(v101[3] * v101[3] - v111 * v111 - (v236 - *v101) * (v236 - *v101)) + v238;
									if (v110 >= *(float *)a4)
									{
										if (0.34999999 * v101[3] < v110 - *(float *)a4)
										{
											v216 = v215;
											v112 = (int)&v87->ucSurfaceTypeA;
											*(_WORD *)v112 = *((_WORD *)v90 + 8);
											v193 = 1;
											*(_BYTE *)(v112 + 2) = v90[18];
										}
										*((_BYTE *)&dword_9666D0 + curSpheref) = 1;
										*(float *)a4 = v110;
										CColPoint::operator=((CColPoint *)(v91 - 32), v87);
										*(_DWORD *)(v91 + 8) = *(_DWORD *)i;
										*(_WORD *)v91 = *((_WORD *)v90 + 8);
										*(_BYTE *)(v91 + 2) = v90[18];
									}
								}
							}
							v77 = __OFSUB__(v207 + 1, v222);
							v24 = v207 + 1 - v222 < 0;
							*(float *)&v215 = 1.0e24;
							++v207;
						} while ((unsigned __int8)v24 ^ v77);
					}
					qmemcpy(&v240, v87, sizeof(v240));
					for (k = 0; k < v220; ++k)
					{
						v115 = dword_9666E0[k];
						v116 = (int)&pColData2->m_pTrianglePlanes[v115];
						v117 = (int)&pColData2->m_pTriangles[v115];
						v118 = (int)pColData2->m_pTrianglePoints;
						v240.fDepth = -1.0;
						if (CCollision::ProcessSphereTriangle((int)(v219 - 3), v118, v117, v116, (int)&v240, (int)&v215))
						{
							v119 = (int)&v224[dword_9666E0[k]];
							v120 = *(signed __int16 *)(v119 + 2);
							v121 = *(signed __int16 *)(v119 + 4);
							v229 = (double)v224[dword_9666E0[k]].V1.x * 0.00024414062;
							v230 = (double)v120 * 0.00024414062;
							v122 = (double)v121 * 0.00024414062;
							v231 = v122;
							if (v122 > 0.30000001 && pMat2Model->matrix.at.z > 0.89999998)
							{
								v240.Normal.x = v229;
								v240.Normal.y = v230;
								v240.Normal.z = v231;
							}
							if (ProcessDiscCollision(
								&v240,
								&g_shadowMeshMatrixTransform,
								(CColDisk *)((char *)_pCol1Data->m_pDisks + v218),
								v87,
								(int)&dword_9666D0 + curSpheref,
								(float *)a4,
								(CColPoint *)(v213 - 32)))
							{
								v193 = 1;
							}
						}
					}
					if (v193 && v211 < 31)
					{
						++v211;
						v212 += 44;
						v87 = (CColPoint *)(v212 - 40);
						i = v212;
						*(_DWORD *)v212 = -1082130432;
					}
					if (*((_BYTE *)&dword_9666D0 + curSpheref))
					{
						v123 = v213;
						v124 = (_DWORD *)(v213 - 32);
						v125 = operator*((RwV3D *)&v232, pMat2Model, (RwV3D *)(v213 - 32));
						*v124 = LODWORD(v125->x);
						v124[1] = LODWORD(v125->y);
						v124[2] = LODWORD(v125->z);
						v126 = (_DWORD *)(v123 - 16);
						v127 = Multiply3x3((RwV3D *)&_vecCol1Sphere_x, pMat2Model, (RwV3D *)(v123 - 16));
						*v126 = LODWORD(v127->x);
						v126[1] = LODWORD(v127->y);
						v126[2] = LODWORD(v127->z);
					}
					else
					{
						*(_DWORD *)a4 = -859915232;
					}
				}
				v219 += 5;
				v218 += 36;
				v128 = _pCol1Data->m_bNumLines;
				v77 = __OFSUB__(curSpheref + 1, v128);
				v24 = curSpheref++ + 1 - v128 < 0;
				v213 += 44;
				a4 += 4;
			} while ((unsigned __int8)v24 ^ v77);
		}
	}
	else
	{
		v130 = _pCol1Data;
		v131 = 0;
		for (v129 = 0; v129 < pColData1->_nbLines; ++v129)
		{
			v132 = operator*(
				(RwV3D *)&_vecCol1Sphere_x,
				&g_faceGroupsMatrixTransform,
				(RwV3D *)((char *)&v130->m_pDisks->base + v131 * 4));
			v133 = &dword_9664D0[v131];
			*v133 = LODWORD(v132->x);
			v133[1] = LODWORD(v132->y);
			v133[2] = LODWORD(v132->z);
			v134 = (int *)operator*(
				(RwV3D *)&v232,
				&g_faceGroupsMatrixTransform,
				(RwV3D *)(&v130->m_pDisks->surfaceTypeA + v131 * 4));
			dword_9664E0[v131] = *v134;
			dword_9664E4[v131] = v134[1];
			dword_9664E8[v131] = v134[2];
			v131 += 8;
		}


		v136 = _pCol1Data;
		v137 = _pCol1Data->m_bNumLines;
		*(float *)&v135 = 0.0;

		for (v212 = 0; v212 < v136->_nbLines; ++v212)
		{
			dword_966490[v212] = v212;
		}

		v208 = 0;
		if (v135 > 0)
		{
			v138 = fUnk;
			v139 = dword_966490;
			do
			{
				*((_BYTE *)&dword_9666D0 + v208) = 0;
				for (curSphereg = 0; curSphereg < v221; ++curSphereg)
				{
					if (CCollision::ProcessLineSphere(
						(CColLine *)&dword_9664D0[8 * *v139],
						&pColData2->m_pSpheres[g_shadowMeshPassed[curSphereg]],
						&pColPoint2[*v139],
						(float *)(v138 + 4 * *v139)))
					{
						*((_BYTE *)&dword_9666D0 + v208) = 1;
					}
				}
				for (curSphereh = 0; curSphereh < v222; ++curSphereh)
				{
					if (CCollision::ProcessLineBox(
						(CColLine *)&dword_9664D0[8 * *v139],
						&pColData2->m_pBoxes[dword_967040[curSphereh]],
						&pColPoint2[*v139],
						(float *)(v138 + 4 * *v139)))
					{
						*((_BYTE *)&dword_9666D0 + v208) = 1;
					}
				}
				v140 = 0;
				for (curSpherei = 0; curSpherei < v220; ++curSpherei)
				{
					if (CCollision::ProcessLineTriangle(
						(CColLine *)&dword_9664D0[8 * *v139],
						pColData2->m_pTrianglePoints,
						&pColData2->m_pTriangles[dword_9666E0[v140]],
						&pColData2->m_pTrianglePlanes[dword_9666E0[v140]],
						&pColPoint2[*v139],
						v138 + 4 * *v139,
						0))
					{
						*((_BYTE *)&dword_9666D0 + v208) = 1;
					}
					v138 = fUnk;
					v140 = curSpherei + 1;
				}
				if (*((_BYTE *)&dword_9666D0 + v208))
				{
					v141 = operator*((RwV3D *)&_vecCol1Sphere_x, pMat2Model, &pColPoint2[*v139].Position);
					v142 = &pColPoint2[*v139];
					v142->Position.x = v141->x;
					v142->Position.y = v141->y;
					v142->Position.z = v141->z;
					v143 = Multiply3x3((RwV3D *)&v232, pMat2Model, &pColPoint2[*v139].Normal);
					v144 = (_DWORD *)&pColPoint2[*v139].Normal.x;
					*v144 = LODWORD(v143->x);
					v144[1] = LODWORD(v143->y);
					v144[2] = LODWORD(v143->z);
				}
				++v139;
				++v208;
			} while (v208 < v212);
		}
	}

	if (v211 > 0)
	{
		v145 = pColPoint1;
		v212 = v211;
		do
		{
			v146 = operator*((RwV3D *)&_vecCol1Sphere_x, pMat2Model, &v145->Position);
			v145->Position.x = v146->x;
			v145->Position.y = v146->y;
			v147 = &v145->Normal.x;
			v145->Position.z = v146->z;
			v148 = Multiply3x3((RwV3D *)&v232, pMat2Model, &v145->Normal);
			*v147 = v148->x;
			v145->Normal.y = v148->y;
			v149 = v148->z;
			++v145;
			LODWORD(v150) = v212 - 1;
			v23 = v212 == 1;
			v147[2] = v149;
			*(float *)&v212 = v150;
		} while (!v23);
	}

	v213 = 0;
	*(float *)&a4 = 0.0;
	*(float *)&v215 = 0.0;
	if (v221 > 0)
	{
		v151 = _pCol1Data;
		if (_pCol1Data->m_wNumTriangles || _pCol1Data->m_wNumBoxes)
		{
			v235 = pCol2Model->boundingBox.fRadius;
			v152 = (int *)operator*(
				(RwV3D *)&_vecCol1Sphere_x,
				&g_shadowMeshMatrixTransform,
				(RwV3D *)&pCol2Model->boundingBox.vecBoundOffset);
			v153 = *v152;
			v154 = *((float *)v152 + 1);
			v155 = *((float *)v152 + 2);
			v23 = v151->m_wNumTriangles == 0;
			v232 = v153;
			v233 = v154;
			v234 = v155;
			if (!v23)
			{
				if (pCol1Model->pColData)
					CCollision::CalculateTrianglePlanes(pCol1Model->pColData);
				v156 = 0;
				if (v151->m_wNumTriangles > 0)
				{
					v157 = 0;
					do
					{
						if (CCollision::TestSphereTriangle(
							(CColSphere *)&v232,
							v151->m_pTrianglePoints,
							(CColTriangle *)&v151->m_pTriangles[v156],
							&v151->m_pTrianglePlanes[v157]))
						{
							v158 = v213;
							dword_965B30[v213] = v156;
							v213 = v158 + 1;
							if (v158 + 1 >= 599)
								break;
						}
						++v156;
						++v157;
					} while (v156 < v151->m_wNumTriangles);
				}
			}
			v159 = 0;
			if (v151->m_wNumBoxes > 0)
			{
				v160 = *(float *)&a4;
				v162 = *(float *)&v232 + v235;
				v161 = 0;
				do
				{
					v164 = v151->m_pBoxes;
					v165 = v162 < v164[v161].boxes.max.x;
					v163 = &v164[v161].boxes.max.x;
					if (!v165
						&& *(float *)&v232 - v235 <= v163[3]
						&& v233 + v235 >= v163[1]
						&& v233 - v235 <= v163[4]
						&& v234 + v235 >= v163[2]
						&& v234 - v235 <= v163[5])
					{
						dword_965A30[LODWORD(v160)++] = v159;
					}
					++v159;
					++v161;
				} while (v159 < v151->m_wNumBoxes);
				*(float *)&a4 = v160;
			}
			if (v213)
			{
				v166 = &pColPoint1[v211];
				v167 = v221 - (_DWORD)v223;
				v23 = v221 == (_DWORD)v223;
				v24 = v221 - (signed int)v223 < 0;
				v166->fDepth = -1.0;
				v209 = 0;
				i = v167;
				if (!((unsigned __int8)v24 | (unsigned __int8)v23))
				{
					v212 = (int)v166;
					*(float *)&v216 = COERCE_FLOAT(g_shadowMeshPassed);
					do
					{
						v168 = 0;
						v224 = (CColTrianglePlane *)0x6753C21C;
						v194 = 0;
						if (v213 > 0)
						{
							do
							{
								if (CCollision::ProcessSphereTriangle(
									g_shadowMeshMatrixArray[v216],
									(int)v151->m_pTrianglePoints,
									(int)&v151->m_pTriangles[dword_965B30[v168]],
									(int)&v151->m_pTrianglePlanes[dword_965B30[v168]],
									(int)v166,
									(int)&v224))
								{
									v194 = 1;
								}
								++v168;
							} while (v168 < v213);

							if (v194)
							{
								v166->Normal *= -1.f;
								if (v211 >= 31)
									break;
								v166 = (CColPoint *)(v212 + 44);
								++v211;
								v212 = (int)v166;
								++v215;
								v166->fDepth = -1.0;
							}
						}
						v77 = __OFSUB__(v209 + 1, i);
						v24 = v209++ + 1 - i < 0;
						v216 += 4;
					} while ((unsigned __int8)v24 ^ v77);
				}
			}
			if (*(float *)&a4 != 0.0)
			{
				v172 = v221 - (_DWORD)v223;
				if (v221 - (signed int)v223 > 0)
				{
					v223 = g_shadowMeshPassed;
					v216 = v172;
					do
					{
						v174 = (int)&pColData2->m_pSpheres[*v223];
						v173 = 0;
						v224 = (CColTrianglePlane *)1733542428;
						v218 = v174;
						if (a4 > 0)
						{
							v175 = &pColPoint1[v211];
							v212 = (int)&pColPoint1[v211];
							do
							{
								if (CCollision::ProcessSphereBox(
									g_shadowMeshMatrixArray[v223],
									&v151->m_pBoxes[dword_965A30[v173]],
									v175,
									(int)&v224))
								{
									v176 = (int)&v151->m_pBoxes[dword_965A30[v173]].surface;
									v177 = (int)&v175->ucSurfaceTypeA;
									*(_WORD *)v177 = *(_WORD *)v176;
									*(_BYTE *)(v177 + 2) = *(_BYTE *)(v176 + 2);

									v178 = v218 + 16;
									v179 = (int)&v175->ucSurfaceTypeB;
									*(_WORD *)v179 = *(_WORD *)(v218 + 16);
									*(_BYTE *)(v179 + 2) = *(_BYTE *)(v178 + 2);

									v175->Normal *= -1.f;

									if (v211 >= 31)
										break;
									v175 = (CColPoint *)(v212 + 44);
									++v211;
									v212 = (int)v175;
									++v215;
									v175->fDepth = -1.0;
								}
								++v173;
							} while (v173 < a4);
						}
						v23 = v216 == 1;
						++v223;
						--v216;
					} while (!v23);
				}
			}
		}
	}
	if (v211 - v215 < v211)
	{
		v183 = (int)&pColPoint1[v211 - v215];//pColPoint
		CColPoint * pColPoint;
		v212 = v215;
		do
		{
			pColPoint->Position = pMat1Model * pColPoint->Position;
			pColPoint->Normal = pMat1Model * pColPoint->Normal;

			MaterialInfo savedMat = pColPoint->material1;
			pColPoint->material1 = pColPoint->material2;
			pColPoint->material2 = savedMat;

			--v212;
		} while (v212 != 0);
	}
#endif
	return 0;
}

// Converted from cdecl void ResetMadeInvisibleObjects(void) 0x415540
void ResetMadeInvisibleObjects() {
	plugin::Call<0x415540>();
}