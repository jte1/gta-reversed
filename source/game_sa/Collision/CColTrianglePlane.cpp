#include "StdInc.h"
#include "CColTrianglePlane.h"

void CColTrianglePlane::Set(CompressedVector  const* vertices, CColTriangle & triangle)
{
	plugin::CallMethod<0x411660, CColTrianglePlane*, CompressedVector const *, CColTriangle&>(this, vertices, triangle);
}