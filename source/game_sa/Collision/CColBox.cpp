#include "StdInc.h"

CColBox & CColBox::operator=(CColBox const& right)
{
	this->m_vecMax = right.m_vecMax;
	this->m_vecMin = right.m_vecMin;
	this->material = right.material;
	return *this;
}