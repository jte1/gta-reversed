#pragma once

class CGameLogic
{
public:
	/**
	*ActivePlayers
		* AfterDeathStartPointOrientations
		* AfterDeathStartPoints
		* GameState
		* MaxPlayerDistance
		* MissionDropOffReadyToBeUsed
		* NumAfterDeathStartPoints
		* SavedWeaponSlots
		* ShortCutDropOffForMission
		* ShortCutDropOffOrientationForMission
		* SkipDestination
		* SkipDestinationOrientation
		* SkipState
		* SkipTimer
		* SkipToBeFinishedByScript
		* SkipVehicle
		* TimeOfLastEvent
		* bLimitPlayerDistance
		* bPenaltyForArrestApplies
		* bPenaltyForDeathApplies
		* bPlayersCanBeInSeparateCars
		* bPlayersCannotTargetEachother
		* bScriptCoopGameGoingOn
		* f2PlayerStartHeading
		*/
	static int &n2PlayerPedInFocus;
		/* nPrintFocusHelpCounter
		* nPrintFocusHelpTimer
		* vec2PlayerStartLocation
		*/
		/**
	*CalcDistanceToForbiddenTrainCrossing(CVector, CVector, bool, CVector*)
		* ClearSkip(bool)
		* Disable2ndControllerForDebug()
		* DoWeaponStuffAtStartOf2PlayerGame(bool)
		* FindCityClosestToPoint(CVector)
		* ForceDeathRestart()
		* InitAtStartOfGame()
		*/
		static bool IsCoopGameGoingOn();
		/* IsPlayerAllowedToGoInThisDirection(CPlayerPed*, CVector, float)
		* IsPlayerUse2PlayerControls(CPlayerPed*)
		* IsPointWithinLineArea(CVector*, int, CVector)
		* IsScriptCoopGameGoingOn()
		* IsSkipWaitingForScriptToFadeIn()
		* LaRiotsActiveHere()
		* Load()
		* PassTime(unsigned int)
		* Remove2ndPlayerIfPresent()
		* ResetStuffUponResurrection()
		* RestorePedsWeapons(CPed*)
		* RestorePlayerStuffDuringResurrection(CPlayerPed*, CVector, float)
		* Save()
		* SetPlayerWantedLevelForForbiddenTerritories(bool)
		* SetUpSkip(CVector, float, bool, CVehicle*, bool)
		* SkipCanBeActivated()
		* SortOutStreamingAndMemory(CVector const&, float)
		* StopPlayerMovingFromDirection(int, CVector)
		* StorePedsWeapons(CPed*)
		* Update()
		* UpdateSkip()
		*/
};