#include "StdInc.h"
#include "CDraw.h"

float &CDraw::ms_fAspectRatio = *reinterpret_cast<float*>(0xC3EFA4);
unsigned char &CDraw::FadeValue = *reinterpret_cast<unsigned char*>(0xC3EFAB);
float &CDraw::ms_fFOV = *reinterpret_cast<float *>(0x8D5038);
float &CDraw::ms_fLODDistance = *reinterpret_cast<float *>(0xC3EF98);
float &CDraw::ms_fFarClipZ = *reinterpret_cast<float *>(0xC3EF9C);
float &CDraw::ms_fNearClipZ = *reinterpret_cast<float *>(0xC3EFA0);
unsigned char &CDraw::FadeRed = *reinterpret_cast<unsigned char *>(0xC3EFA8);
unsigned char &CDraw::FadeGreen = *reinterpret_cast<unsigned char *>(0xC3EFA9);
unsigned char &CDraw::FadeBlue = *reinterpret_cast<unsigned char *>(0xC3EFAA);

void CDraw::SetFOV(float fFOV)
{
	CDraw::ms_fFOV = fFOV;
}

float CDraw::CalculateAspectRatio()
{
	if (FrontEndMenuManager.m_bWidescreenOn)
	{
		CDraw::ms_fAspectRatio = 16.0f / 9.0f;
	}
	else if (TheCamera.m_b1rstPersonRunCloseToAWall)
	{
		CDraw::ms_fAspectRatio = 5.0f / 4.0f;
	}
	else
	{
		CDraw::ms_fAspectRatio = 4.0f / 3.0f;
	}
	return CDraw::ms_fAspectRatio;
}