#pragma once

#include "CZoneDef.h"

enum ECullFlags
{
	CullFlag_CamCloseInForPlayer = 1,
	CullFlag_CamStairsForPlayer=2,
	CullFlag_Cam1stPersonForPlayer=4,
	CullFlag_NoRain = 8,
	CullFlag_NoPolice = 0x10,
	CullFlag_DoINeedToLoadCollision = 0x40,
	CullFlag_PoliceAbandonCars = 0x100,
	CullFlag_InRoomForAudio = 0x200,
	CullFlag_FewerPeds =  0x400,
	CullFlag_MilitaryZone = 0x1000,
	CullFlag_CamInTunnel = 0x2000, // ref CWeather::UpdateTunnelness
	CullFlag_DoExtraAirResistance = 0x4000,
	CullFlag_FewerCars = 0x8000,
};

class CCullZones
{
public:
	static int &CurrentFlags_Camera;
	static int &CurrentFlags_Player;
	static int &NumAttributeZones;
	static int &NumMirrorAttributeZones;
	static int &NumTunnelAttributeZones;
	static CCullZone * aAttributeZones; // CCullZone aAttributeZones[1300]
	static CZoneDef	* aMirrorAttributeZones;
	static CZoneDef	* aTunnelAttributeZones;
	static bool &bMilitaryZonesDisabled;
	//static bool &bRenderCullzones; // unused

	static void AddCullZone(CVector const&, float, float, float, float, float, float, unsigned short, short);
	static void AddMirrorAttributeZone(CVector const&, float, float, float, float, float, float, unsigned short, float, float, float, float);
	static void AddTunnelAttributeZone(CVector const&, float, float, float, float, float, float, unsigned short);
	static bool Cam1stPersonForPlayer();
	static bool CamCloseInForPlayer();
	static bool CamNoRain();
	static bool CamStairsForPlayer();
	static bool DoExtraAirResistanceForPlayer();
	static bool DoINeedToLoadCollision();
	static bool FewerCars();
	static bool FewerPeds();
	static unsigned int FindAttributesForCoors(CVector vecPos);
	static unsigned int FindMirrorAttributesForCoors(CVector);
	static unsigned int FindTunnelAttributesForCoors(CVector);
	static bool FindZoneWithStairsAttributeForPlayer();
	static bool InRoomForAudio();
	static void Init();
	static bool NoPolice();
	static bool PlayerNoRain();
	static bool PoliceAbandonCars();
	static void Update();
};