#include "StdInc.h"

float &CCamera::m_f3rdPersonCHairMultY = *reinterpret_cast<float *>(0xB6EC10);
float &CCamera::m_f3rdPersonCHairMultX = *reinterpret_cast<float *>(0xB6EC14);
float &CCamera::m_fMouseAccelVertical = *reinterpret_cast<float *>(0xB6EC18);
float &CCamera::m_fMouseAccelHorzntl = *reinterpret_cast<float *>(0xB6EC1C);
bool &CCamera::m_bUseMouse3rdPerson = *reinterpret_cast<bool *>(0xB6EC2E);
CCamera &TheCamera = *reinterpret_cast<CCamera *>(0xB6F028);
unsigned char &byte_B6EC34 = *reinterpret_cast<unsigned char*>(0xB6EC34);
bool &bAvoidTest1 = *reinterpret_cast<bool*>(0);
float &fCloseNearClipLimit = *reinterpret_cast<float*>(0);
float &fAvoidTweakFOV = *reinterpret_cast<float*>(0);
float &flt_8CC38C = *reinterpret_cast<float*>(0x8CC38C);
float &fAvoidProbTimerDamp = *reinterpret_cast<float*>(0);
float &flt_B6EC38 = *reinterpret_cast<float *>(0xB6EC38);
float &flt_B6EC3C = *reinterpret_cast<float *>(0xB6EC3C);
bool &byte_B70142 = *reinterpret_cast<bool *>(0xB70142);
bool &byte_B70143 = *reinterpret_cast<bool *>(0xB70143);
float &flt_8CCF20 = *reinterpret_cast<float *>(0x8CCF20);
bool &bDidWeProcessAnyCinemaCam = *reinterpret_cast<bool *>(0xB6EC2D);
/*static*/CMatrix &gmatSavedBeforeMirror = *reinterpret_cast<CMatrix*>(0x00B6FE40);
bool &gPlayerPedVisible = *reinterpret_cast<bool *>(0x008CC380); // script visibility of player
float &flt_8CCB94 = *reinterpret_cast<float *>(0x8CCB94);
float &flt_8CCB98 = *reinterpret_cast<float *>(0x8CCB98);

void WellBufferMe(float fEndAngle, float *fAngle, float *fAngleSpeed, float a4, float fStep, bool bNormalizeAngle)
{
	float fDeltaAngle = fEndAngle - *fAngle;

	if (bNormalizeAngle)
	{
		MakeAngleLessThan180(fDeltaAngle);
	}

	float v7 = fDeltaAngle * a4;
	float v9 = v7 - *fAngleSpeed;
	if (v9 <= 0.0f)
		*fAngleSpeed -= fStep * fabsf(v9) * CTimer::ms_fTimeStep;
	else
		*fAngleSpeed += fStep * fabsf(v9) * CTimer::ms_fTimeStep;

	if (v7 >= 0.0 || v7 <= *fAngleSpeed)
	{
		if (v7 > 0.0 && v7 < *fAngleSpeed)
			*fAngleSpeed = v7;
	}
	else
	{
		*fAngleSpeed = v7;
	}

	float dt = ClampMax(CTimer::ms_fTimeStep, 10.f);

	*fAngle += dt * *fAngleSpeed;
}

void CCamera::InjectHooks()
{
	CCam::InjectHooks();
}

char CCamera::CameraColDetAndReact(CVector *source, CVector *target)
{
	return plugin::CallMethodAndReturn<char, 0x520190, CCamera*, CVector*, CVector*>(this, source, target);
}


void CCamera::SetColVarsPed(int pedtype, int nCamPedZoom)
{
	plugin::Call<0x50CC50, int, int>(pedtype, nCamPedZoom);
}

void CCamera::CameraGenericModeSpecialCases(CPed *targetPed)
{
	plugin::CallMethod<0x50CD30, CCamera*, CPed*>(this, targetPed);
}

void CCamera::HandleCameraMotionForDucking(CPed *ped, CVector *source, CVector *targPosn, bool arg5)
{
	plugin::CallMethod<0x50D090, CCamera*, CPed*, CVector*, CVector*, bool>(this, ped, source, targPosn, arg5);
}

void CCamera::ImproveNearClip(CVehicle *pVehicle, CPed *pPed, CVector *source, CVector *targPosn)
{
	plugin::CallMethod<0x516B20, CCamera*, CVehicle*, CPed*, CVector*, CVector*>(this, pVehicle, pPed, source, targPosn);
}

void CCamera::ResetDuckingSystem(CPed * pPed)
{
	this->m_fDuckCamMotionFactor = 0.0;
	this->m_fDuckAimCamMotionFactor = 0.0;
	if (this->ConsiderPedAsDucking(pPed))
	{
		if (pPed->m_vecMoveSpeed.MagnitudeSquared() <= 0.000001f)
			this->m_fDuckCamMotionFactor = flt_8CCB94 - 1.0;
		else
			this->m_fDuckCamMotionFactor = flt_8CCB98 - 0.5;

		this->m_fDuckAimCamMotionFactor = -0.35f;
	}
}

void CCamera::CamControl()
{
#if 0
	this->m_bObbeCinematicPedCamOn = 0;
	this->m_bObbeCinematicCarCamOn = 0;
	this->m_bUseTransitionBeta = 0;
	this->m_bUseSpecialFovTrain = 0;
	this->m_bJustCameOutOfGarage = 0;
	this->m_bTargetJustCameOffTrain = 0;
	this->m_bInATunnelAndABigVehicle = 0;
	this->m_bJustJumpedOutOf1stPersonBecauseOfTarget = 0;

	int v316 = 0;
	int v317 = 0;
	byte_B6EC34 = 0;

	if (!this->_GetActiveCam()->m_pCamTargetEntity && !this->m_pTargetEntity)
	{
		this->m_pTargetEntity = (CEntity *)&CWorld::Players[CWorld::PlayerInFocus].m_pPed;
		this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
	}

	this->m_nZoneCullFrameNumWereAt++;
	if (this->m_nZoneCullFrameNumWereAt > this->m_nCheckCullZoneThisNumFrames)
		this->m_nZoneCullFrameNumWereAt = 1;
	this->m_bCullZoneChecksOn = this->m_nZoneCullFrameNumWereAt == this->m_nCheckCullZoneThisNumFrames;
	
	if (this->m_bCullZoneChecksOn == 1)
		this->m_bFailedCullZoneTestPreviously = (unsigned __int8)CCullZones::CamCloseInForPlayer() != 0;
	
	if (this->m_bLookingAtPlayer == 1)
	{
		CPad::GetPad(0)->b1stPlayerMode = false;
		FindPlayerPed(-1)->m_bIsVisible = true;
	}
	
	if (CTimer::m_UserPause || CTimer::m_CodePause || this->m_bIdleOn)
		goto LABEL_287;

	int v321 = 0;
	float num = 0.0;
	int currentCamMode;
	
	if (this->m_bTargetJustBeenOnTrain == 1)
	{
		if (this->m_pTargetEntity->_GetEntityType() != ENTITY_TYPE_VEHICLE ||
			((CVehicle *)this->m_pTargetEntity)->m_nVehicleClass != 6)
		{
			this->Restore();
			this->m_bTargetJustCameOffTrain = 1;
			this->m_bTargetJustBeenOnTrain = 0;
			this->m_bWantsToSwitchWidescreenOff = this->m_bWideScreenOn == 1;
		}
	}

	if (this->m_pTargetEntity->_GetEntityType() != ENTITY_TYPE_VEHICLE)
	{
		if (this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_PED)
		{
			if (CPad::GetPad(0)->CycleCameraModeUpJustDown()
				|| CPad::GetPad(0)->CycleCameraModeDownJustDown())
			{
				if (CReplay::Mode != 1
					&& !this->m_bWideScreenOn
					&& !this->m_bFailedCullZoneTestPreviously
					&& !this->m_bFirstPersonBeingUsed
					&& (this->m_bLookingAtPlayer == 1 || this->m_nWhoIsInControlOfTheCamera == 2)
					&& !CGameLogic::IsCoopGameGoingOn())
				{
					if (CPad::GetPad(0)->CycleCameraModeUpJustDown() == 0)
						this->m_nPedZoom++;
					else
						this->m_nPedZoom--;

					if (this->m_nPedZoom > 3)
					{
						this->m_nPedZoom = 1;
					}
					if (this->m_nPedZoom < 1)
						this->m_nPedZoom = 3;
				}
			}
			currentCamMode = MODE_FOLLOWPED;
			if (this->m_bLookingAtPlayer != 1 
				&& !this->m_bEnable1rstPersonCamCntrlsScript
				|| this->m_pTargetEntity->_GetEntityType() != ENTITY_TYPE_PED
				|| this->m_bWideScreenOn && !this->m_bEnable1rstPersonCamCntrlsScript
				|| this->m_aCams[0].Using3rdPersonMouseCam())
			{
				goto LABEL_253;
			}

			if (!FindPlayerPed(-1)->m_pIntelligence->m_TaskMgr.GetTaskSecondary(0))
			{
				if (!CPad::GetPad(0)->LookAroundLeftRight())
				{
					CPad::GetPad(0)->LookAroundUpDown();
				}
			}

			if (this->m_bFirstPersonBeingUsed)
			{
				if (CPad::GetPad(0)->GetPedWalkLeftRight()
					|| CPad::GetPad(0)->GetPedWalkUpDown()
					|| CPad::GetPad(0)->NewState.ButtonSquare
					|| CPad::GetPad(0)->NewState.ButtonTriangle
					|| CPad::GetPad(0)->NewState.ButtonCross
					|| CPad::GetPad(0)->NewState.ButtonCircle
					|| CPad::GetPad(0)->NewState.Select
					|| CTimer::m_snTimeInMilliseconds - this->m_nFirstPersonCamLastInputTime > 2850)
					this->m_bFirstPersonBeingUsed = 0;

				else if (CPad::GetPad(0)->TargetJustDown())
				{
					this->m_bJustJumpedOutOf1stPersonBecauseOfTarget = 1;
					this->m_bFirstPersonBeingUsed = 0;
				}
			}
			if (!FindPlayerPed(-1)->IsPedInControl() 
				|| FindPlayerPed(-1)->m_pPlayerData->m_fMoveBlendRatio > 0.0)
				this->m_bFirstPersonBeingUsed = 0;

			if (this->m_bFirstPersonBeingUsed)
			{
				currentCamMode = MODE_1STPERSON;
				CPad::GetPad(0)->b1stPlayerMode = true;
			}
			v140 = this->m_nPedZoom;
			if (this->m_nPedZoom == 1)
			{
				v144 = this->ActiveCam;
				v142 = this->Cams[v144].m_fTargetZoomGroundOne;
				v141 = (int)v2 + v144 * 568;
			}
			else if (this->m_nPedZoom == 3)
			{
				v143 = this->ActiveCam;
				v142 = this->Cams[v143].m_fTargetZoomGroundThree;
				v141 = (int)v2 + v143 * 568;
			}
			else
			{
				v141 = (int)v2 + 568 * this->ActiveCam;
				v142 = *(float *)(v141 + 596);
			}
			v145 = this->m_fPedZoomSmoothed;
			this->m_fPedZoomBase = v142;
			if (this->m_bUseScriptZoomValuePed == 1)
			{
				v146 = this->m_fPedZoomValueScript;
				v148 = CTimer::ms_fTimeStep * 0.12;
				if (v149 | v150)
				{
					this->m_fPedZoomSmoothed = ClampMax(v148 + this->m_fPedZoomSmoothed, this->m_fPedZoomValueScript);
				}
				else
				{
					this->m_fPedZoomSmoothed = ClampMin(this->m_fPedZoomSmoothed - v148, this->m_fPedZoomValueScript);
				}
			LABEL_286:
				LOBYTE(v163) = sub_4038EB(this->_GetActiveCam());
				return (char)v163;
			}

			if (this->m_bFailedCullZoneTestPreviously)
			{
				num = 0.7f;
				v153 = CTimer::ms_fTimeStep * 0.12;
				if (v154 | v155)
				{
					this->m_fPedZoomSmoothed += v153;
					if (flt_8CCF14 < this->m_fPedZoomSmoothed)
						this->m_fPedZoomSmoothed = flt_8CCF14;
				}
				else
				{
					this->m_fPedZoomSmoothed -= v153;
					if (flt_8CCF14 > this->m_fPedZoomSmoothed)
					{
						this->m_fPedZoomSmoothed = flt_8CCF14;
						goto LABEL_286;
					}
				}
				goto LABEL_286;
			}
			v157 = this->m_fPedZoomBase;
			v159 = CTimer::ms_fTimeStep * 0.12;
			if (v160 | v161)
			{
				v162 = v159 + this->m_fPedZoomSmoothed;
				if (v162 > this->m_fPedZoomBase)
					goto LABEL_282;
			}
			else
			{
				v162 = this->m_fPedZoomSmoothed - v159;
				if (v162 < this->m_fPedZoomBase)
				{
				LABEL_282:
					v162 = this->m_fPedZoomBase;
					goto LABEL_283;
				}
			}
		LABEL_283:
			this->m_fPedZoomSmoothed = v162;
			if (v140 == 3 && this->m_fPedZoomBase == 0.0)
				this->m_fPedZoomSmoothed = this->m_fPedZoomBase;
			goto LABEL_286;
		}
	LABEL_287:
		v13 = currentCamMode;
	}
	else
	{
		if (dword_8CC824 > 0)
		{
			currentCamMode = dword_8CC824;
			dword_8CC824 = -1;
		}
		CVehicle * pVehicle = (CVehicle *)this->m_pTargetEntity;
		if (pVehicle->m_nVehicleClass != 6)
		{
			if (CPad::GetPad(0)->CycleCameraModeUpJustDown()
				|| CPad::GetPad(0)->CycleCameraModeDownJustDown())
			{
				if (CReplay::Mode != 1 && !this->m_bWideScreenOn)
				{
					if (this->m_bFailedCullZoneTestPreviously)
					{
					LABEL_47:
						if (this->m_nCarZoom != 4 && this->m_nCarZoom)
							currentCamMode = MODE_CAM_ON_A_STRING;
					LABEL_50:
						v24 = this->pTargetEntity;
						v25 = v24[25].m_pRWObject;
						if (v25 == (RpClump *)5)
						{
							if (v24->m_wModelIndex != 460)
							{
								currentCamMode = MODE_BEHINDBOAT;
								goto LABEL_177;
							}
							v25 = 0;
						}
						else if (v25 && v25 != (RpClump *)9)
						{
							goto LABEL_177;
						}
						v26 = 0;
						v326 = 0;
						if (v25 == (RpClump *)9)
						{
							if ((unsigned __int8)CCullZones::CamStairsForPlayer((CCullZones *)v315))
							{
								v26 = (signed __int16 *)findCullZoneForPoint();
								v326 = v26;
								if (v26)
									v317 = 1;
							}
						}
						v27 = this->pTargetEntity;
						v28 = v27->__parent.m_pCoords;
						if (v28)
							v29 = (int)&v28->rwMat.pos;
						else
							v29 = (int)&v27->__parent.placement;
						v30.x = *(float *)v29;
						*(_QWORD *)&v30.y = *(_QWORD *)(v29 + 4);
						if (!CGarage__IsPointInAGarageCameraZone(v30) && !v317)
						{
							if (this->m_bPlayerIsInGarage)
							{
								this->m_bJustCameOutOfGarage = 1;
								this->m_bPlayerIsInGarage = 0;
							}
							this->m_bGarageFixedCamPositionSet = 0;
						LABEL_176:
							currentCamMode = 18;
							goto LABEL_177;
						}
						if (!this->m_bGarageFixedCamPositionSet && this->m_bLookingAtPlayer == 1 || this->m_nWhoIsInControlOfTheCamera == 2)
						{
							v31 = (tEffectLight *)this->pToGarageWeAreIn;
							if (v31 || v26)
							{
								v32 = this->pTargetEntity;
								v33 = v32->__parent.m_pCoords;
								if (v33)
									v34 = &v33->rwMat.pos.x;
								else
									v34 = &v32->__parent.placement.pos.x;
								if (v31)
								{
									CGarage::FindDoorsWithGarage(v31, &a2, &a3);
									if (a2)
									{
										v35 = a2->__parent.__parent.__parent.m_pCoords;
										v36 = 1;
										v37 = &v35->rwMat.pos.x;
										if (!v35)
											v37 = &a2->__parent.__parent.__parent.placement.pos.x;
										position.x = *v37;
										if (v35)
											position.y = v35->rwMat.pos.y;
										else
											position.y = a2->__parent.__parent.__parent.placement.pos.y;
										position.z = 0.0;
									}
									else if (a3)
									{
										v36 = 2;
									}
									else
									{
										v38 = this->pTargetEntity;
										v39 = v38->__parent.m_pCoords;
										v36 = 1;
										if (v39)
											v40 = &v39->rwMat.pos.x;
										else
											v40 = &v38->__parent.placement.pos.x;
										position.x = *v40;
										v41 = v38->__parent.m_pCoords;
										if (v41)
											position.y = v41->rwMat.pos.y;
										else
											position.y = v38->__parent.placement.pos.y;
										out.z = 0.0;
									}
								}
								else
								{
									v42 = &this->Cams[this->ActiveCam].Source.x;
									position.x = *v42;
									v43 = v42[1];
									position.z = v42[2];
									what.x = *v34;
									what.y = v34[1];
									position.y = v43;
									what.z = v34[2];
									v36 = 1;
									arrPos = 1;
									if (sqrt((what.y - v43) * (what.y - v43) + (what.x - position.x) * (what.x - position.x)) > 15.0)
									{
										v44 = &v32->__parent.m_pCoords->rwMat.right;
										if (v44)
											v45 = v44 + 4;
										else
											v45 = (CVector *)&v32->__parent.placement;
										vectorSub(&out, v45, &what);
										out.z = 0.0;
										CVector::Normalise((RwV3D *)&out);
										LODWORD(v46) = v326[2];
										v331 = SLOWORD(v46);
										if (SLOWORD(v46) < 0)
											LODWORD(v46) = -SLOWORD(v46);
										num = v46;
										v47 = v326[4];
										v48 = v47;
										v329 = v47;
										if (v47 < 0)
											v48 = -v47;
										v49 = v326[3];
										v50 = v49;
										v51 = v49;
										if (v49 < 0)
											v51 = -v49;
										v52 = v326[5];
										v53 = v52;
										v54 = v52;
										if (v52 < 0)
											v54 = -v52;
										if (LODWORD(num) + v48 <= v51 + v54)
										{
											if (v326[3] < 0)
												v50 = -v50;
											if (v52 < 0)
												v53 = -v52;
											LODWORD(num) = v50 + v53;
										}
										else
										{
											if (v326[2] >= 0)
												v55 = v331;
											else
												v55 = -v331;
											v56 = v329;
											if (v326[4] < 0)
												v56 = -v329;
											LODWORD(num) = v55 + v56;
										}
										v57 = this->pTargetEntity;
										v58 = &v57->__parent.m_pCoords->rwMat.right;
										v59 = v58 + 4;
										if (!v58)
											v59 = (CVector *)&v57->__parent.placement;
										num = (double)SLODWORD(num) + (double)SLODWORD(num);
										v60 = num;
										v61 = scaleVector2(&v330, num, &out);
										vectorAdd(&a1, v59, v61);
										v62 = &v57->__parent.m_pCoords->rwMat.right;
										if (v62)
											v63 = v62 + 4;
										else
											v63 = (CVector *)&v57->__parent.placement;
										if (CWorld::GetIsLineOfSightClear(v63, &a1, 1, 0, 0, 0, 0, 0, 1))
											goto LABEL_728;
										v64 = this->pTargetEntity;
										v65 = &v64->__parent.m_pCoords->rwMat.right;
										v66 = v65 + 4;
										if (!v65)
											v66 = (CVector *)&v64->__parent.placement;
										v67 = scaleVector2(&v330, v60, &out);
										v68 = vectorSub(&v335, v66, v67);
										a1.x = v68->x;
										a1.y = v68->y;
										a1.z = v68->z;
										v69 = v64->__parent.m_pCoords;
										v70 = v69 ? &v69->rwMat.pos : &v64->__parent.placement;
										if (CWorld::GetIsLineOfSightClear(v70, &a1, 1, 0, 0, 0, 0, 0, 1))
											LABEL_728:
										position = (RwV3D)a1;
										v36 = arrPos;
									}
								}
								v71 = &this->pToGarageWeAreIn->Position.x;
								if (v71)
								{
									v72 = v71[11];
									a1.z = 0.0;
									v73 = v72 + v71[10];
									what.z = 0.0;
									a1.x = v73 * 0.5;
									v74 = v71[13];
									what.x = a1.x;
									a1.y = (v74 + v71[12]) * 0.5;
									what.y = a1.y;
								}
								else
								{
									position.z = 0.0;
									if (!v326)
									{
										v75 = this->pTargetEntity;
										v76 = v75->__parent.m_pCoords;
										if (v76)
											v77 = (int)&v76->rwMat.pos;
										else
											v77 = (int)&v75->__parent.placement;
										v78 = v75->__parent.m_pCoords;
										if (v78)
											v79 = &v78->rwMat.pos.x;
										else
											v79 = &v75->__parent.placement.pos.x;
										v80 = *v79;
										a1.y = *(float *)(v77 + 4);
										a1.x = v80;
										a1.z = 0.0;
										what.x = v80;
										what.y = a1.y;
										what.z = 0.0;
									}
								}
								if (v36 == 1)
								{
									a1.x = position.x - what.x;
									a1.y = position.y - what.y;
									v81 = position.z;
								}
								else
								{
									a1.x = out.x - what.x;
									a1.y = out.y - what.y;
									v81 = out.z;
								}
								v82 = this->pTargetEntity;
								a1.z = v81 - what.z;
								v334 = (RwV3D)a1;
								v83 = v82->__parent.m_pCoords;
								if (v83)
									v84 = (int)&v83->rwMat.pos;
								else
									v84 = (int)&v82->__parent.placement;
								v85 = *(float *)v84;
								v86 = *(float *)(v84 + 4);
								v87 = *(float *)(v84 + 8);
								v335.x = v85;
								v335.y = v86;
								v335.z = v87;
								CWorld::FindGroundZFor3DCoord(v85, v86, v87, &a4, 0);
								v334.z = 0.0;
								CVector::Normalise(&v334);
								position.x = 0.0;
								position.y = 0.0;
								position.z = 0.0;
								out.x = 0.0;
								out.y = 0.0;
								out.z = 0.0;
								if (a2)
								{
									v88 = (CPlaceable *)a2->m_pDummyObject;
									v89 = v88->m_pCoords;
									if (v89)
										v90 = (int)&v89->rwMat.pos;
									else
										v90 = (int)&v88->placement;
									position.x = *(float *)v90;
									v91 = *(float *)(v90 + 4);
									v92 = *(float *)(v90 + 8);
									position.y = v91;
									position.z = v92;
									v93 = (float *)CPlaceable::GetMatrix(v88);
									rotation.x = *v93;
									rotation.y = v93[1];
									rotation.z = v93[2];
								}
								v94 = a3;
								if (a3)
								{
									v95 = (CPlaceable *)a3->m_pDummyObject;
									v96 = v95->m_pCoords;
									if (v96)
										v97 = (int)&v96->rwMat.pos;
									else
										v97 = (int)&v95->placement;
									out.x = *(float *)v97;
									v98 = *(float *)(v97 + 4);
									v99 = *(float *)(v97 + 8);
									out.y = v98;
									out.z = v99;
									v100 = (float *)CPlaceable::GetMatrix(v95);
									rotation.x = *v100;
									rotation.y = v100[1];
									rotation.z = v100[2];
									v94 = a3;
								}
								if (a2)
								{
									if (v94)
									{
										a1.z = out.z - position.z;
										out.x = (out.x - position.x) * 0.5;
										a1.x = out.x + position.x;
										a1.y = (out.y - position.y) * 0.5 + position.y;
										a1.z = a1.z * 0.5 + position.z;
										v330 = a1;
									}
									else
									{
										v330 = (CVector)position;
									}
								}
								else if (v94)
								{
									v330 = out;
								}
								else
								{
									v330.x = v85;
									v330.y = v86;
									v330.z = v87;
									a1.x = v335.x - what.x;
									rotation.x = a1.x;
									a1.y = v335.y - what.y;
									rotation.y = a1.y;
									a1.z = v335.z - what.z;
									rotation.z = 0.0;
									CVector::Normalise(&rotation);
								}
								v101 = VectorScale(&a1, (CVector *)&rotation, dword_8CCF1C);
								v102 = vectorAdd(&out, &v330, v101);
								v103 = v102->y;
								v104 = v102->z + flt_8CCF18;
								position.x = v102->x;
								position.y = v103;
								position.z = v104;
								a1.x = 0.0;
								a1.y = 0.0;
								a1.z = 0.0;
								CCamera::SetCamPositionForFixedMode(&position, (RwV3D *)&a1);
								v26 = v326;
								this->m_bGarageFixedCamPositionSet = 1;
							}
						}
						if (!CGarages::CameraShouldBeOutside() && !v317
							|| this->m_bGarageFixedCamPositionSet != 1
							|| this->m_bLookingAtPlayer != 1 && this->m_nWhoIsInControlOfTheCamera != 2)
						{
							if (this->m_bPlayerIsInGarage)
							{
								this->m_bJustCameOutOfGarage = 1;
								this->m_bPlayerIsInGarage = 0;
							}
							goto LABEL_176;
						}
						if (this->m_pToGarageWeAreIn || v26)
						{
							currentCamMode = MODE_FIXED;
							this->m_bPlayerIsInGarage = 1;
						}
					LABEL_177:
						int arrPos = 0;
						CCamera::GetArrPosForVehicleType(((CVehicle *)this->m_pTargetEntity)->GetVehicleAppearance(), &arrPos);
						if (this->m_nCarZoom == 0 && !this->m_bPlayerIsInGarage)
						{
							v13 = 16;
							this->m_fCarZoomBase = 0.0;
							currentCamMode = 16;
						LABEL_188:
							if (v107 == 4 && !this->m_bPlayerIsInGarage)
								this->m_fCarZoomBase = 1.0;
							if (this->m_fCarZoomTotal == 0.0)
								this->m_fCarZoomTotal = this->m_fCarZoomBase;
							v109 = this->m_fCarZoomSmoothed;
							if (this->m_bUseScriptZoomValueCar == 1)
							{
								v110 = this->m_fCarZoomValueScript;
								v112 = CTimer::ms_fTimeStep * 0.12;
								if (v113 | v114)
								{
									this->m_fCarZoomSmoothed += v112;
									if (this->m_fCarZoomSmoothed > this->m_fCarZoomValueScript)
										this->m_fCarZoomSmoothed = this->m_fCarZoomValueScript;
								}
								else
								{
									this->m_fCarZoomSmoothed -= v112;
									if (this->m_fCarZoomSmoothed < this->m_fCarZoomValueScript)
									{
										this->m_fCarZoomSmoothed = this->m_fCarZoomValueScript;
									}
								}
								WellBufferMe(
									*(float *)&v321,
									(float *)&this->Cams[this->ActiveCam].m_fCloseInCarHeightOffset,
									(float *)&this->Cams[this->ActiveCam].m_fCloseInCarHeightOffsetSpeed,
									0.1,
									0.25,
									0);
								goto LABEL_288;
							}
							if (this->m_bFailedCullZoneTestPreviously)
							{
								v321 = (CObject *)1059481190;
								v117 = CTimer::ms_fTimeStep * 0.12;
								if (v118 | v119)
								{
									v120 = v117 + this->m_fCarZoomSmoothed;
									if (v120 > -0.64999998)
										v120 = -0.64999998;
								}
								else
								{
									v120 = this->m_fCarZoomSmoothed - v117;
									if (v120 < -0.64999998)
									{
										this->m_fCarZoomSmoothed = -0.64999998;
										goto LABEL_215;
									}
								}
								this->m_fCarZoomSmoothed = v120;
								goto LABEL_215;
							}
							v121 = this->m_fCarZoomBase;
							v123 = CTimer::ms_fTimeStep * 0.12;
							if (v124 | v125)
							{
								v126 = v123 + this->m_fCarZoomSmoothed;
								if (v126 > this->m_fCarZoomBase)
									goto LABEL_211;
							}
							else
							{
								v126 = this->m_fCarZoomSmoothed - v123;
								if (v126 < this->m_fCarZoomBase)
								{
								LABEL_211:
									v126 = this->m_fCarZoomBase;
									goto LABEL_212;
								}
							}
						LABEL_212:
							this->m_fCarZoomSmoothed = v126;
							if (v107 == 3 && this->m_fCarZoomBase == 0.0)
								this->m_fCarZoomSmoothed = this->m_fCarZoomBase;
							goto LABEL_215;
						}
						switch (v107)
						{
						case 1:
							v108 = ZOOM_ONE_DISTANCE[arrPos];
							break;
						case 2:
							v108 = ZOOM_TWO_DISTANCE[arrPos];
							break;
						case 3:
							v108 = *(float *)&ZOOM_THREE_DISTANCE[arrPos];
							break;
						default:
						LABEL_187:
							v13 = currentCamMode;
							goto LABEL_188;
						}
						this->m_fCarZoomBase = v108;
						goto LABEL_187;
					}
					if ((this->m_bLookingAtPlayer == 1 || this->m_nWhoIsInControlOfTheCamera == 2)
						&& !CGameLogic::IsCoopGameGoingOn())
					{
						if (CPad::GetPad(0)->CycleCameraModeUpJustDown() == 0)
							this->m_nCarZoom++;
						else
							this->m_nCarZoom--;

						if (this->m_nCarZoom > 5)
						{
							this->m_nCarZoom = 0;
						}

						if (this->m_nCarZoom < 0)
							this->m_nCarZoom = 5;

						if (this->m_nCarZoom == 4)
						{
							this->m_nCarZoom = CPad::GetPad(0)->CycleCameraModeUpJustDown() != 0 ? 3 : 5;
						}
						if (this->m_nCarZoom == 0 && this->m_bDisableFirstPersonInCar)
						{
							this->m_nCarZoom = CPad::GetPad(0)->CycleCameraModeUpJustDown() != 0 ? 5 : 1;
						}
					}
				}
			}
		LABEL_46:
			if (!this->m_bFailedCullZoneTestPreviously)
				goto LABEL_50;
			goto LABEL_47;
		}
		v13 = MODE_BEHINDCAR;
		currentCamMode = MODE_BEHINDCAR;
	}
LABEL_288:
	if (this->m_bCooperativeCamMode)
	{
		if (CGameLogic::IsCoopGameGoingOn())
		{
			CVehicle *pVeh1 = CWorld::Players[0].m_pPed->m_pVehicle;
			CVehicle *pVeh2 = CWorld::Players[1].m_pPed->m_pVehicle;
			CPed* pPed1 = CWorld::Players[0].m_pPed;
			CPed* pPed2 = CWorld::Players[1].m_pPed;
			if (pPed1->bInVehicle && pPed2->bInVehicle && pVeh1 && pVeh2)
			{
				this->m_pTargetEntity = pVeh1;
				if (pVeh1 == pVeh2)
				{
					if (this->m_bAllowShootingWith2PlayersInCar)
						currentCamMode = this->m_nModeForTwoPlayersSameCarShootingAllowed;
					else
						currentCamMode = this->m_nModeForTwoPlayersSameCarShootingNotAllowed;
				}
				else
				{
					currentCamMode = this->m_nModeForTwoPlayersSeparateCars;
				}
			}
			else
			{
				currentCamMode = this->m_nModeForTwoPlayersNotBothInCar;
			}
		}
	}
	int v167 = 0;
	int a4 = 0;
	if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_ARRESTED)
	{
		byte_B7013C = 1;
	}
	else if (byte_B7013C)
	{
		a4 = 1;
		byte_B7013C = 0;
	}
	if (dword_B70138 == 63 || v166 != 63)
	{
		v167 = 0;
	}
	else if (this->m_nCarZoom || this->m_pTargetEntity->_GetEntityType() != ENTITY_TYPE_VEHICLE)
	{
		v167 = 1;
	}
	dword_B70138 = v166;
	if (v167 == 1)
	{
		currentCamMode = 32;
		dword_B70134 = 32;
		this->_GetActiveCam()->m_bResetStatics = true;
	}
	else
	{
		if (v166 == 63)
			currentCamMode = dword_B70134;
	}

	if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_DEAD)
	{
		this->m_bObbeCinematicCarCamOn = 0;
		if (this->_GetActiveCam()->m_nMode == MODE_PED_DEAD_BABY)
		{
			currentCamMode = MODE_PED_DEAD_BABY;
		}
		else if (this->_GetActiveCam()->m_nMode != MODE_ARRESTCAM_ONE)
		{
			if (this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_PED)
			{
				CPed * pPed = (CPed *)this->m_pTargetEntity;
				CPedIntelligence * pIntelligence = pPed->m_pIntelligence;
				int i;
				for (i = 0; i < ARRAYSIZE(pIntelligence->m_pedScanner.m_apEntities); ++i)
				{
					CPed * pScannedPed = (CPed *)pIntelligence->m_pedScanner.m_apEntities[i];
					if (!pScannedPed)
						continue;

					CTaskSimpleArrestPed * pArrestTask = pScannedPed->m_pIntelligence->m_TaskMgr.FindActiveTaskByType(TASK_SIMPLE_ARREST_PED);
					if (pArrestTask && pArrestTask->m_pArrestingPlayer == FindPlayerPed())
					{
						CVector vecCopToPed = pScannedPed->GetPosition() - pPed->GetPosition();
						if (vecCopToPed.Magnitude() < 4.0f)
						{
							currentCamMode = MODE_ARRESTCAM_ONE;
							this->_GetActiveCam()->m_bResetStatics = true;
							break;
						}
					}
				}
				if (i == ARRAYSIZE(pIntelligence->m_pedScanner.m_apEntities))
				{
					currentCamMode = MODE_PED_DEAD_BABY;
					this->_GetActiveCam()->m_bResetStatics = true;
				}
			}
			else
			{
				currentCamMode = MODE_PED_DEAD_BABY;
				this->_GetActiveCam()->m_bResetStatics = true;
			}
		}
	}

	if (this->m_bRestoreByJumpCut == 1)
	{
		if (v13 != MODE_FOLLOWPED
			&& v13 != 34
			&& v13 != 7
			&& v13 != 8
			&& v13 != 51
			&& v13 != 46
			&& v13 != 11
			&& v13 != 28
			&& v13 != 35
			&& v13 != 18
			&& v13 != MODE_BEHINDCAR
			&& !CCamera::m_bUseMouse3rdPerson)
		{
			this->SetCameraDirectlyBehindForFollowPed_CamOnAString();
		}

		this->_GetActiveCam()->m_nMode = (eCamMode)this->m_nModeToGoTo;
		currentCamMode = this->m_nModeToGoTo;

		this->m_bJust_Switched = 1;
		this->_GetActiveCam()->m_bResetStatics = 1;

		this->_GetActiveCam()->m_vecCamFixedModeVector = this->m_vecFixedModeVector;

		if (this->_GetActiveCam()->m_pCamTargetEntity)
			this->_GetActiveCam()->m_pCamTargetEntity->CleanUpOldReference(&this->_GetActiveCam()->m_pCamTargetEntity);

		this->_GetActiveCam()->m_pCamTargetEntity = this->m_pTargetEntity;
		this->_GetActiveCam()->m_pCamTargetEntity->RegisterReference(&this->_GetActiveCam()->m_pCamTargetEntity);

		this->_GetActiveCam()->m_vecCamFixedModeSource = this->m_vecFixedModeSource;
		this->_GetActiveCam()->m_vecCamFixedModeUpOffSet = this->m_vecFixedModeUpOffSet;

		this->_GetActiveCam()->m_bCamLookingAtVector = false;

		this->_GetActiveCam()->m_vecLastAboveWaterCamPosition = this->_GetNextCam()->m_vecLastAboveWaterCamPosition;

		this->m_bRestoreByJumpCut = 0;
		this->_GetActiveCam()->m_bResetStatics = 1;
		this->m_fCarZoomSmoothed = this->m_fCarZoomBase;
		this->m_fPedZoomSmoothed = this->m_fPedZoomBase;
		this->m_bTransitionState = 0;
		this->m_bDoingSpecialInterPolation = 0;
	}
	
	if (byte_BA6728)
		currentCamMode = MODE_MODELVIEW;

	int v318 = 1;
	
	if (this->m_pTargetEntity)
	{
		if (this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE)
		{
			if (this->m_nCarZoom == 5)
				this->m_bObbeCinematicCarCamOn = 1;
		}
		else if (this->m_nPedZoom == 5)
		{
			this->m_bObbeCinematicPedCamOn = 1;
		}
	}

	if (FindPlayerVehicle(-1, 0) && FindPlayerVehicle(-1, 0)->m_nVehicleClass == 6)
		this->m_bObbeCinematicCarCamOn = 1;
	int v202 = currentCamMode;
	if (this->m_pTargetEntity 
		&& this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_PED
		&& FindPlayerPed()
		&& (FindPlayerPed()->m_nPedState == PEDSTATE_ARRESTED || FindPlayerPed()->m_nPedState == PEDSTATE_DEAD))
	{
		this->m_bObbeCinematicPedCamOn = 0;
		v318 = 0;
		if (FindPlayerPed()->m_nPedState == PEDSTATE_ARRESTED)
		{
			v202 = MODE_ARRESTCAM_ONE;
				currentCamMode = MODE_ARRESTCAM_ONE;
		}
		else if (FindPlayerPed()->m_nPedState == PEDSTATE_DEAD)
		{
			v202 = MODE_PED_DEAD_BABY;
			currentCamMode = MODE_PED_DEAD_BABY;
		}
	}

	if (this->m_bTargetJustBeenOnTrain == 1
		|| v202 == 29
		|| v202 == 23
		|| v202 == 28
		|| v202 == 11
		|| v202 == 7
		|| v202 == 35
		|| v202 == 8
		|| v202 == 51
		|| v202 == 32
		|| v202 == 33
		|| v202 == 34
		|| v202 == 36
		|| v202 == 39
		|| v202 == 40
		|| v202 == 52
		|| v202 == 42
		|| v202 == 43
		|| v202 == 41
		|| v202 == 45
		|| v202 == 46
		|| this->m_nWhoIsInControlOfTheCamera == 1
		|| this->m_bJustCameOutOfGarage
		|| this->m_bPlayerIsInGarage
		|| this->_GetActiveCam()->m_nMode == MODE_PED_DEAD_BABY)
	{
		v318 = 0;
	}

	if (this->m_bCinemaCamera)
	{
		this->m_bObbeCinematicCarCamOn = 1;
		v318 = 1;
	}

	if (!this->m_bObbeCinematicPedCamOn || v318 != 1)
	{
		if (this->m_bObbeCinematicCarCamOn && v318 == 1)
		{
			//CPostEffects::m_bSpeedFXUserFlagCurrentFrame = 0;

			if (this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE)
			{
				CVehicle * pVehicle = (CVehicle *)this->m_pTargetEntity; // v204
				
				if (pVehicle->m_nVehicleSubClass == VEHICLE_PLANE)
				{
					this->ProcessObbeCinemaCameraPlane();
				}
				else if (pVehicle->GetVehicleAppearance() == VEHICLE_APPEARANCE_HELI)
				{
					this->ProcessObbeCinemaCameraHeli();
				}
				else if (pVehicle->m_nVehicleSubClass == VEHICLE_BOAT)
				{
					this->ProcessObbeCinemaCameraBoat();
				}
				else if (pVehicle->m_nVehicleSubClass == VEHICLE_TRAIN)
						this->ProcessObbeCinemaCameraTrain();
				else
					this->ProcessObbeCinemaCameraCar();
				v202 = currentCamMode;
			}
		}
		else
		{
			if (this->m_bPlayerIsInGarage && v203)
				v316 = 1;
			v318 = 0;
			bDidWeProcessAnyCinemaCam = 0;
		}
	}
	if (this->m_bLookingAtPlayer == 1)
	{
		if (v202 != MODE_TOPDOWN
			&& v202 != MODE_1STPERSON
			&& v202 != MODE_TOP_DOWN_PED)
		{
			if (v202 != MODE_CAM_ON_A_STRING && v202 != MODE_BEHINDBOAT)
			{
				if (v202 == MODE_FIXED)
				{
					if (this->_GetActiveCam()->m_nMode == MODE_TOPDOWN)
						v316 = 1;
					goto LABEL_472;
				}
			LABEL_436:
				if (v202 == MODE_AIMWEAPON
				|| v202 == MODE_AIMWEAPON_FROMCAR
				|| v202 == MODE_AIMWEAPON_ATTACHED)
				{
					v206 = this->pTargetEntity;
					if (v206)
					{
						if ((v206->info & 7) == 3)
						{
							v207 = 0;
							if (CPedIntelligence::GetTaskJetPack((CPedIntelligence *)v206[20].flags1))
								v207 = 1;
							if (currentCamMode == MODE_AIMWEAPON)
							{
								v208 = (int)v2 + 568 * this->ActiveCam;
								if (*(_WORD *)(v208 + 384) == 4 && !v207)
								{
									v209 = this->pTargetEntity;
									v210 = v209[32].flags1;
									if (v210)
									{
										v211 = &v209->__parent.m_pCoords->rwMat.right;
										v212 = v211 + 4;
										if (!v211)
											v212 = (CVector *)&v209->__parent.placement;
										v213 = *(CVector **)(v210 + 20);
										if (v213)
											v214 = v213 + 4;
										else
											v214 = (CVector *)(v210 + 4);
										vectorSub(&a1, v214, v212);
										v215 = atan2(-a1.x, a1.y);
									}
									else
									{
										v215 = CPlaceable::getRotation((_CPlaceable *)this->pTargetEntity);
									}
									v216 = v215 - 1.5707964;
									if (v216 <= *(float *)(v208 + 560) + 3.1415927)
									{
										if (v216 < *(float *)(v208 + 560) - 3.1415927)
											v216 = v216 + 6.2831855;
									}
									else
									{
										v216 = v216 - 6.2831855;
									}
									if (MAX_ANGLE_BEFORE_AIMWEAPON_JUMPCUT * 0.017453292 < fabs(v216 - *(float *)(v208 + 560))
										|| ((v217 = &v209->__parent.m_pCoords->rwMat.right) == 0 ? (v218 = (CVector *)&v209->__parent.placement) : (v218 = v217 + 4),
											v219 = vectorSub(&a1, v218, &this->m_cameraMatrix.rwMat.pos),
											(flt_B6F0FC + 2.0) * 1.5 < CVector::Magnitude(v219)))
									{
										v316 = 1;
									}
									if (CCamera::m_bUseMouse3rdPerson)
										v316 = 0;
								LABEL_472:
									v220 = this->_GetActiveCam()->m_nMode;
									v202 = currentCamMode;
									if ((v220 != MODE_TWOPLAYER
										|| currentCamMode == MODE_TWOPLAYER_IN_CAR_AND_SHOOTING)
										&& (v220 != MODE_TWOPLAYER_IN_CAR_AND_SHOOTING
											|| currentCamMode == MODE_TWOPLAYER))
									{
									LABEL_477:
										if (v202 == 1)
										{
											v222 = v220 == 37;
										}
										else
										{
											if (v202 != 37)
											{
												if ((v202 == 16
													|| v202 == 7
													|| v202 == 34
													|| v202 == 8
													|| v202 == 51
													|| v202 == 39
													|| v202 == 40
													|| v202 == 52
													|| v202 == 42
													|| v202 == 43
													|| v202 == 41
													|| v202 == 45
													|| v202 == 32
													|| v202 == 33
													|| v202 == 46)
													&& (this->pTargetEntity->info & 7) == 3)
												{
													goto LABEL_582;
												}
												switch (v202)
												{
												case 15:
													if (this->m_bPlayerIsInGarage == 1)
													{
														if (v220 != 7
															&& v220 != 45
															&& v220 != 8
															&& v220 != 51
															&& v220 != 34
															&& v220 != 37
															&& !v317
															&& v220 != 16
															&& v220 != 39
															&& v220 != 40
															&& v220 != 52
															&& v220 != 42
															&& v220 != 43
															&& v220 != 41
															&& v220 != 46)
														{
															goto LABEL_583;
														}
														v223 = this->pTargetEntity;
														if (!v223)
															goto LABEL_583;
														v224 = (v223->info & 7) == 2;
														goto LABEL_581;
													}
													break;
												case 4:
													v225 = 0;
													if (v220 == 53)
													{
														v226 = this->pTargetEntity;
														if (!(unsigned __int8)CPed::CanWeRunAndFireWithWeapon((CPed *)v315)
															|| (v227 = this->pTargetEntity, LODWORD(v227[20].__parent.placement.pos.z) & 0x4000000))
														{
															v202 = currentCamMode;
														}
														else
														{
															v228 = CPlaceable::getRotation((_CPlaceable *)this->pTargetEntity) - 1.5707964;
															v229 = this->ActiveCam;
															v230 = this->Cams[v229].Beta + 3.1415927;
															v231 = &this->Cams[v229].Beta;
															if (v228 <= v230)
															{
																if (v228 < *v231 - 3.1415927)
																	v228 = v228 + 6.2831855;
															}
															else
															{
																v228 = v228 - 6.2831855;
															}
															if (MAX_ANGLE_BEFORE_AIMWEAPON_JUMPCUT * 0.017453292 < fabs(v228 - *v231)
																|| !(LOBYTE(v227[20].__parent.placement.pos.z) & 1))
															{
																v225 = 1;
															}
															v202 = currentCamMode;
															if (CCamera::m_bUseMouse3rdPerson)
															{
																v225 = 0;
																this->m_bJustCameOutOfGarage = 1;
															}
														}
													}
													v232 = this->ActiveCam;
													v233 = (int)v2 + v232 * 568;
													v234 = this->Cams[v232].mode;
													if ((v234 == 16
														|| v234 == 7
														|| v234 == 34
														|| v234 == 8
														|| v234 == 51
														|| v234 == 29
														|| v234 == 32
														|| v234 == 33
														|| v234 == 30
														|| v234 == 39
														|| v234 == 40
														|| v234 == 52
														|| v234 == 42
														|| v234 == 43
														|| v234 == 41
														|| v234 == 45
														|| v234 == 1
														|| v234 == 37
														|| v234 == 46
														|| v225
														|| a4)
														&& !this->m_bJustCameOutOfGarage)
													{
														if (v234 == 7
															|| v234 == 8
															|| v234 == 51
															|| v234 == 34
															|| v234 == 16
															|| v234 == 39
															|| v234 == 40
															|| v234 == 52
															|| v234 == 42
															|| v234 == 43
															|| v234 == 41
															|| v234 == 45
															|| v234 == 46)
														{
															v235 = CGeneral::GetATanOfXY(*(float *)(v233 + 772), *(float *)(v233 + 776)) - 1.5707964;
															*(float *)&this->pTargetEntity[24].m_pRWObject = v235;
															*(float *)&this->pTargetEntity[24].flags1 = v235;
														}
														v236 = (int)v2 + 568 * this->ActiveCam;
														this->m_bUseTransitionBeta = 1;
														v316 = 1;
														if (*(_WORD *)(v236 + 384) == 37)
														{
															v237 = FindPlayerPed(-1);
															v238 = v237->__parent.__parent.__parent.m_pCoords;
															if (v238)
																v239 = &v238->rwMat.pos;
															else
																v239 = (CVector *)&v237->__parent.__parent.__parent.placement;
															vectorSub(&a1, (CVector *)&this->Cams[this->ActiveCam].Source, v239);
															a1.z = 0.0;
															CVector::Normalise((RwV3D *)&a1);
															a1.x = 0.001;
															a1.y = 1.0;
															v240 = CGeneral::GetATanOfXY(0.001, 1.0);
														}
														else
														{
															v240 = CGeneral::GetATanOfXY(*(float *)(v236 + 772), *(float *)(v236 + 776)) + 3.1415927;
														}
														v202 = currentCamMode;
														this->Cams[this->ActiveCam].m_fTransitionBeta = v240;
													}
													goto LABEL_583;
												case 38:
												case 32:
												case 33:
												case 29:
													goto LABEL_582;
												}
											LABEL_580:
												v224 = v220 == 29;
											LABEL_581:
												if (v224)
													LABEL_582 :
													v316 = 1;
											LABEL_583:
												v241 = this->ActiveCam;
												v242 = this->Cams[v241].mode;
												v243 = (int)v2 + v241 * 568;
												if (v202 != v242 && !*(_DWORD *)(v243 + 912))
													v316 = 1;
												v244 = this->m_bPlayerIsInGarage;
												if (v244)
												{
													v245 = this->pToGarageWeAreIn;
													if (v245)
													{
														v246 = v245->Type;
														if (v246 == 2 || v246 == 3 || v246 == 4)
														{
															v247 = this->pTargetEntity;
															if ((v247->info & 7) == 2 && v247->m_wModelIndex == 423 && v202 != v242)
																v316 = 1;
														}
													}
													v248 = *(_DWORD *)(v243 + 912);
													if (v248)
													{
														v249 = *(_DWORD *)(v248 + 20);
														v250 = (float *)(v249 ? v249 + 48 : v248 + 4);
														v251 = *v250 - this->m_vecFixedModeSource.x;
														v252 = v250[1] - this->m_vecFixedModeSource.y;
														v253 = v250[2];
														v254 = *(_DWORD *)(v248 + 20);
														v255 = v253 - this->m_vecFixedModeSource.z;
														v256 = (float *)(v254 ? v254 + 48 : v248 + 4);
														v257 = (*v256 - *(float *)(v243 + 784)) * v251
															+ (v256[2] - *(float *)(v243 + 792)) * v255
															+ (v256[1] - *(float *)(v243 + 788)) * v252;
														if (v259 | v260)
															v316 = 1;
													}
												}
												if (v202 == v242)
													goto LABEL_607;
												if (this->m_bTransitionState)
												{
													if (v316 != 1)
													{
													LABEL_607:
														v261 = this->m_bTransitionState;
														if (v261 && v202 != v242)
														{
															if (!this->m_bWaitForInterpolToFinish && this->m_bLookingAtPlayer)
															{
																CVector vecPlayerToCam = FindPlayerPed()->GetPosition() - this->m_mCameraMatrix.pos;//a1

																if (this->m_pTargetEntity 
																	&& this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_PED
																	&& vecPlayerToCam.Magnitude() > 17.5)
																{
																	v202 = currentCamMode;
																	if (currentCamMode == MODE_SYPHON || currentCamMode == MODE_SYPHON_CRIM_IN_FRONT)
																		this->m_bWaitForInterpolToFinish = 1;
																}
																else
																{
																	v202 = currentCamMode;
																}
															}
															if (this->m_bWaitForInterpolToFinish != 1)
															{
																this->m_bDoingSpecialInterPolation = 1;
																this->StartTransition(v202);
															}
														}
														else if (v202 == 15 && this->m_pTargetEntity != *(CEntity **)(v243 + 912) && v244)
														{
															if (v261)
																this->m_bDoingSpecialInterPolation = 1;
															this->StartTransition(15);
														}
														goto LABEL_690;
													}
												}
												else if (v316 != 1)
												{
													if (!this->m_bWaitForInterpolToFinish)
														this->StartTransition(v202);
													goto LABEL_690;
												}
												if ((!v244 || this->m_bJustCameOutOfGarage)
													&& v202 != 4
													&& v202 != 34
													&& v202 != 7
													&& v202 != 8
													&& v202 != 51
													&& v202 != 46
													&& v202 != 11
													&& v202 != 16
													&& v202 != 28
													&& v202 != 35
													&& !CCamera::m_bUseMouse3rdPerson)
												{
													this->SetCameraDirectlyBehindForFollowPed_CamOnAString();
													v202 = currentCamMode;
												}
												this->_GetActiveCam()->m_nMode = (eCamMode)v202;
												this->m_bJust_Switched = 1;

												this->_GetActiveCam()->m_vecCamFixedModeVector = this->m_vecFixedModeVector;

												if (this->_GetActiveCam()->m_pCamTargetEntity)
													this->_GetActiveCam()->m_pCamTargetEntity->CleanUpOldReference(&this->_GetActiveCam()->m_pCamTargetEntity);
												
												this->_GetActiveCam()->m_pCamTargetEntity = this->m_pTargetEntity;
												this->_GetActiveCam()->m_pCamTargetEntity->RegisterReference(&this->_GetActiveCam()->m_pCamTargetEntity);

												this->_GetActiveCam()->m_vecCamFixedModeSource = this->m_vecFixedModeSource;
												this->_GetActiveCam()->m_vecCamFixedModeUpOffSet = this->m_vecFixedModeUpOffSet;
												this->_GetActiveCam()->m_bCamLookingAtVector = this->m_bLookingAtVector;
												this->_GetActiveCam()->m_vecLastAboveWaterCamPosition = this->_GetNextCam()->m_vecLastAboveWaterCamPosition;

												this->m_fCarZoomSmoothed = this->m_fCarZoomBase;
												this->m_fPedZoomSmoothed = this->m_fPedZoomBase;
												this->m_bTransitionState = 0;
												this->m_bDoingSpecialInterPolation = 0;
												this->m_bStartInterScript = 0;
												this->_GetActiveCam()->m_bResetStatics = 1;
												goto LABEL_690;
											}
											v222 = v220 == 1;
										}
										if (!v222 && v220 != 29)
											goto LABEL_583;
										v316 = 0;
										goto LABEL_580;
									}
								LABEL_476:
									v316 = 1;
									goto LABEL_477;
								}
							}
							v202 = currentCamMode;
							v316 = 1;
						}
					}
				}
				bool v221;
				if (v202 == MODE_TWOPLAYER)
				{
					v221 = this->_GetActiveCam()->m_nMode == MODE_TWOPLAYER_IN_CAR_AND_SHOOTING;
				}
				else
				{
					if (v202 != MODE_TWOPLAYER_IN_CAR_AND_SHOOTING)
						goto LABEL_472;

					v221 = this->_GetActiveCam()->m_nMode == MODE_TWOPLAYER;
				}
				if (!v221)
					goto LABEL_476;
				goto LABEL_472;
			}
			if (this->_GetActiveCam()->m_nMode != MODE_TOPDOWN
				&& this->_GetActiveCam()->m_nMode != MODE_1STPERSON
				&& this->_GetActiveCam()->m_nMode != MODE_TOP_DOWN_PED)
				goto LABEL_436;
		}
		v316 = 1;
		goto LABEL_436;
	}
	int v286 = 0;
	a4 = 0;
	if (this->m_bEnable1rstPersonCamCntrlsScript == 1 || this->m_bAllow1rstPersonWeaponsCamera == 1)
	{
		if (v202 == 16)
		{
			if (this->_GetActiveCam()->m_nMode != MODE_1STPERSON)
				v286 = 1;
		}
		else
		{
			int v287 = this->m_PlayerWeaponMode.m_nMode;
			if ((v287 == 7 
				|| v287 == 16 
				|| v287 == 8 
				|| v287 == 51)
				&& CPad::GetPad(0)->GetTarget()
				&& this->m_bAllow1rstPersonWeaponsCamera)
			{
				v286 = 1;
				a4 = 1;
			}
			else if (this->_GetActiveCam()->m_nMode != this->m_nModeToGoTo)
			{
				this->m_bStartInterScript = 1;
				this->m_nTypeOfSwitch = 2;
				CPad::GetPad(0)->b1stPlayerMode = false;
			}
			v202 = currentCamMode;
		}
	}
	if (this->m_bTransitionState)
	{
		if (this->m_bStartInterScript != 1)
		{
		LABEL_680:
			if (!v286)
				goto LABEL_690;
		LABEL_681:
			this->m_bTransitionState = 0;
			this->m_bDoingSpecialInterPolation = 0;
			if (this->m_bEnable1rstPersonCamCntrlsScript && v202 == 16)
			{
				this->_GetActiveCam()->m_nMode = MODE_1STPERSON;
			}
			else if (a4)
			{
				this->_GetActiveCam()->m_nMode = (eCamMode)this->m_PlayerWeaponMode.m_nMode;
			}
			else
			{
				this->_GetActiveCam()->m_nMode = (eCamMode)this->m_nModeToGoTo;
			}

			this->m_bJust_Switched = 1;
			this->m_aCams[this->m_nActiveCam].m_bResetStatics = 1;

			this->m_aCams[this->m_nActiveCam].m_vecCamFixedModeVector = this->m_vecFixedModeVector;

			if (this->_GetActiveCam()->m_pCamTargetEntity)
				this->_GetActiveCam()->m_pCamTargetEntity->CleanUpOldReference(&this->_GetActiveCam()->m_pCamTargetEntity);

			this->_GetActiveCam()->m_pCamTargetEntity = this->m_pTargetEntity;
			this->_GetActiveCam()->m_pCamTargetEntity->RegisterReference(&this->_GetActiveCam()->m_pCamTargetEntity);

			this->m_aCams[this->m_nActiveCam].m_vecCamFixedModeSource = this->m_vecFixedModeSource;
			this->m_aCams[this->m_nActiveCam].m_vecCamFixedModeUpOffSet = this->m_vecFixedModeUpOffSet;

			this->m_aCams[this->m_nActiveCam].m_bCamLookingAtVector = this->m_bLookingAtVector;

			this->m_aCams[this->m_nActiveCam].m_vecLastAboveWaterCamPosition = this->m_aCams[(this->m_nActiveCam + 1) % 2].m_vecLastAboveWaterCamPosition;

			this->m_bJust_Switched = true;
			this->m_fCarZoomSmoothed = this->m_fCarZoomBase;
			this->m_fPedZoomSmoothed = this->m_fPedZoomBase;

			goto LABEL_690;
		}
		if (this->m_nTypeOfSwitch == 1)
		{
			currentCamMode = this->m_nModeToGoTo;
			this->m_bDoingSpecialInterPolation = 1;
			this->StartTransition(currentCamMode);
			goto LABEL_690;
		}
	LABEL_678:
		if (this->m_bStartInterScript == 1 && this->m_nTypeOfSwitch == 2)
			goto LABEL_681;
		goto LABEL_680;
	}
	if (this->m_bStartInterScript != 1 || this->m_nTypeOfSwitch != 1)
		goto LABEL_678;
	currentCamMode = this->m_nModeToGoTo;
	this->StartTransition(currentCamMode);
LABEL_690:
	this->m_bStartInterScript = 0;
	if (!this->_GetActiveCam()->m_pCamTargetEntity)
	{
		this->_GetActiveCam()->m_pCamTargetEntity = this->m_pTargetEntity;
		this->_GetActiveCam()->m_pCamTargetEntity->RegisterReference(&this->_GetActiveCam()->m_pCamTargetEntity);
	}
	int v304 = this->_GetActiveCam()->m_nMode;
	if (v304 == MODE_FLYBY
		|| this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_PED
		&& (v304 == MODE_1STPERSON
			|| v304 == MODE_SNIPER
			|| v304 == MODE_M16_1STPERSON
			|| v304 == MODE_CAMERA
			|| v304 == MODE_HELICANNON_1STPERSON
			|| v304 == MODE_ROCKETLAUNCHER
			|| v304 == MODE_ROCKETLAUNCHER_HS))
	{
		if (FindPlayerPed(-1)->m_bIsVisible)
		{
			FindPlayerPed(-1)->m_bIsVisible = false;
			CTaskSimpleHoldEntity * pTaskHold = FindPlayerPed(-1)->m_pIntelligence->GetTaskHold(false);
			if (pTaskHold && pTaskHold->m_pEntity)
			{
				pTaskHold->m_pEntity->m_bIsVisible = false;
			}
		}
	}
	else
	{
		FindPlayerPed(-1)->m_bIsVisible = true;
	}
	if (this->_GetActiveCam()->m_nMode == MODE_FIXED)
	{
		v310 = FindPlayerPed(-1);
		v310->__parent.__parent.flags1 ^= 
			(unsigned __int8)((v310->__parent.__parent.flags1 ^ (gPlayerPedVisible << 7)) & 0x80);
	}
	bool v311 = 0;
	if (!v318 && this->m_nWhoIsInControlOfTheCamera == 2)
	{
		this->RestoreWithJumpCut();
		v311 = 1;
		this->m_bCamDirectlyBehind = 1;
		CPlayerPed * pPlayerPed = FindPlayerPed();
		if (pPlayerPed)
			this->m_fPedOrientForBehindOrInFront = CGeneral::GetATanOfXY(
				pPlayerPed->GetMatrix()->up.x,
				pPlayerPed->GetMatrix()->up.y);
	}

	if (v336 != this->_GetActiveCam()->m_nMode
		|| v311 
		|| this->_GetActiveCam()->m_nMode == MODE_FOLLOWPED
		|| this->_GetActiveCam()->m_nMode == MODE_CAM_ON_A_STRING)
	{
		if (CPad::GetPad(0)->CycleCameraModeJustDown())
		{
			if (CReplay::Mode != 1 
				&& (this->m_bLookingAtPlayer == 1 || this->m_nWhoIsInControlOfTheCamera == 2))
			{
				if (!this->m_bWideScreenOn)
				{
					if (this->m_nWhoIsInControlOfTheCamera != 2
						|| byte_B6EC34 == 1 && !CPad::GetPad(0)->DisablePlayerControls)
					{
						AudioEngine.ReportFrontendAudioEvent(32, 0.0, 1.0);
					}
				}
			}
		}
	}
#endif
}

void CCamera::RestoreWithJumpCut()
{
	this->m_bRestoreByJumpCut = 1;
	this->m_bLookingAtPlayer = 1;
	this->m_bLookingAtVector = 0;
	this->m_nTypeOfSwitch = 2;
	this->m_nWhoIsInControlOfTheCamera = 0;
	this->m_fPositionAlongSpline = 0.0;
	this->m_bStartingSpline = 0;
	this->m_bUseNearClipScript = 0;
	this->m_nModeObbeCamIsInForCar = MODE_PILLOWS_PAPS;
	this->m_bScriptParametersSetForInterPol = 0;
	this->m_bCameraJustRestored = 1;
	this->m_bEnable1rstPersonCamCntrlsScript = 0;
	this->m_bAllow1rstPersonWeaponsCamera = 0;

	if (FindPlayerVehicle(-1, 0))
	{
		this->m_nModeToGoTo = MODE_CAM_ON_A_STRING;
		if (this->m_pTargetEntity)
			this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);
		this->m_pTargetEntity = FindPlayerVehicle(-1, 0);
	}
	else
	{
		this->m_nModeToGoTo = MODE_FOLLOWPED;
		if (this->m_pTargetEntity)
			this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);
		this->m_pTargetEntity = CWorld::Players[CWorld::PlayerInFocus].m_pPed;
	}
	this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);

	if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_ENTER_CAR)
		this->m_nModeToGoTo = MODE_CAM_ON_A_STRING;

	if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_CARJACK
		|| CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_OPEN_DOOR)
		this->m_nModeToGoTo = MODE_CAM_ON_A_STRING;

	if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_EXIT_CAR)
	{
		this->m_nModeToGoTo = MODE_FOLLOWPED;
		if (this->m_pTargetEntity)
			this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);
		this->m_pTargetEntity = CWorld::Players[CWorld::PlayerInFocus].m_pPed;
		this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
	}
	if (!this->m_bCooperativeCamMode || !CGameLogic::IsCoopGameGoingOn())
	{
		this->m_bUseScriptZoomValuePed = 0;
		this->m_bUseScriptZoomValueCar = 0;
		return;
	}
	CPed *pPedOne = CWorld::Players[0].m_pPed;
	CPed *pPedTwo = CWorld::Players[1].m_pPed;

	if (this->m_pTargetEntity)
	{
		this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);
	}
	CVehicle * pVehOne = pPedOne->m_pVehicle;
	CVehicle * pVehTwo = pPedTwo->m_pVehicle;

	if (pPedOne->bInVehicle && pPedTwo->bInVehicle && pVehOne && pVehTwo)
	{
		if (pVehOne == pVehTwo)
		{
			if (this->m_bAllowShootingWith2PlayersInCar)
				this->m_nModeToGoTo = this->m_nModeForTwoPlayersSameCarShootingAllowed;
			else
				this->m_nModeToGoTo = this->m_nModeForTwoPlayersSameCarShootingNotAllowed;
			this->m_pTargetEntity = pVehOne;
			this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
			this->m_bUseScriptZoomValuePed = 0;
			this->m_bUseScriptZoomValueCar = 0;
		}
		else
		{
			this->m_nModeToGoTo = this->m_nModeForTwoPlayersSeparateCars;
			this->m_pTargetEntity = pVehOne;
			this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
			this->m_bUseScriptZoomValuePed = 0;
			this->m_bUseScriptZoomValueCar = 0;
		}
	}
	else
	{
		this->m_nModeToGoTo = this->m_nModeForTwoPlayersNotBothInCar;
		this->m_pTargetEntity = pPedOne;
		this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
		this->m_bUseScriptZoomValuePed = 0;
		this->m_bUseScriptZoomValueCar = 0;
	}
}

void CCamera::AvoidTheGeometry(const CVector *arg0, const CVector *a3, CVector *what, float FOV)
{
//	v62.x = 0.0;
	//v62.y = 0.0;
	
	//v7 = ;
	//v62.z = 0.0;
	//v57 = 0.0;
	
	this->m_vecClearGeometryVec = CVector(0, 0, 0);

	//v11 = 0;
	float v6 = a3->x - arg0->x;
	//x = v6;
	float v61 = a3->z - arg0->z;
	float v12 = a3->y - arg0->y;
	
	//v13 = v12 * v12;
	
	float v52 = sqrt(v61 * v61 + v6 * v6 + v12 * v12);
	float a2 = sqrt(v12 * v12 + v6 * v6);
	float v14;
	if (v6 != 0.0 || v12 != 0.0)
		v14 = CGeneral::GetATanOfXY(v6, v12);
	else
		v14 = CGeneral::GetATanOfXY(this->m_mCameraMatrix.up.x, this->m_mCameraMatrix.up.y);
	
	float a1 = v14;
	float v15;
	if (a2 == 0.0 && v61 == 0.0)
		v15 = 0.f;
	else
		v15 = CGeneral::GetATanOfXY(a2, v61);

	CVector v62;
	v62.x = cos(a1) * cos(v15);
	v62.y = sin(a1) * cos(v15);
	v62.z = sin(v15);

	*what = *a3 - v62 * v52;

	v62.Normalise();

	CWorld::pIgnoreEntity = this->m_pTargetEntity;

	CEntity * pEntity = nullptr;
	CColPoint colPoint;
	if (CWorld::ProcessLineOfSight(*a3, *what, colPoint, pEntity, 
		1, 0, 0, 1, 0, 0, 1, 0))
		// check: buildings, objects, camera ignore test
	{
		
		*what = colPoint.m_vecPoint;//v21,v22,v23; x,y,v61
		CVector saved(*what);

		if (bAvoidTest1)
		{
			if (CWorld::ProcessLineOfSight(*what, *a3, colPoint, pEntity, 
				0, 1, 1, 1, 0, 0, 1, 0))
				// check: vehicles, peds, objects, camera ignore test
			{
				CVector vecRay = *what - colPoint.m_vecPoint;
				if (vecRay.Magnitude() >= Scene.m_pRwCamera->nearPlane)
				{
					CVector vecOldRay = *what - saved;
					if (vecOldRay.Magnitude() < Scene.m_pRwCamera->nearPlane)
					{
						*what = saved;
					}
				}
				else
				{
					*what = colPoint.m_vecPoint;
				}
			}
		}
	}
	CWorld::pIgnoreEntity = 0;;

	CVector vecRayToWhat = *a3 - *what;//x,y,v61

	if (FindPlayerPed(-1))
	{
		float v32 = vecRayToWhat.Magnitude() - flt_8CC38C;
		if (v32 < Scene.m_pRwCamera->nearPlane)
		{
			if (v32 <= fCloseNearClipLimit)
			{
				RwCameraSetNearClipPlane(Scene.m_pRwCamera, fCloseNearClipLimit);
			}
			else
			{
				RwCameraSetNearClipPlane(Scene.m_pRwCamera, v32);
			}
		}
	}
	float a1a = 0.0;
	float a2a = Scene.m_pRwCamera->nearPlane * (tan(DEG_TO_RAD(FOV) / 2) * CDraw::ms_fAspectRatio * fAvoidTweakFOV);
	CVector v58 = v62 * Scene.m_pRwCamera->nearPlane + *what;
	if (CWorld::TestSphereAgainstWorld(v58, a2a, 0, 1, 0, 0, 1, 0, 1))
	{
		CVector colPointPos = gaTempSphereColPoints[0].m_vecPoint;
		CVector xyz = colPointPos - v58;//x,y,v61
		CVector vecRay = colPointPos - *what;
		float v35 = DotProduct(vecRay, v62);
		if (fCloseNearClipLimit >= v35 ||
			v35 >= 0.9f)
		{
			if (v35 < fCloseNearClipLimit)
				RwCameraSetNearClipPlane(Scene.m_pRwCamera, fCloseNearClipLimit);
		}
		else if (v35 < Scene.m_pRwCamera->nearPlane)
		{
			RwCameraSetNearClipPlane(Scene.m_pRwCamera, v35);
		}
		float v53 = a2a - xyz.Magnitude();
		xyz.Normalise();
		v58 = gaTempSphereColPoints[0].m_vecNormal;
		v58.Normalise();
		if (-DotProduct(xyz, v58) < 0.0)
		{
			v58 = -v58;
		}
		a1a = 1.0;
		float v39 = -DotProduct(xyz * v53, v58);
		this->m_vecClearGeometryVec = v58 * v39;
		if (this->m_pTargetEntity
		 && this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_PED
				&& fCloseNearClipLimit + fCloseNearClipLimit > Scene.m_pRwCamera->nearPlane)
			{
				if (DotProduct(v58, this->m_pTargetEntity->GetMatrix()->up) >= 0.0
					&& DotProduct(v58, this->m_pTargetEntity->GetMatrix()->up) > 0.5)
				{
						if (this->m_fAvoidTheGeometryProbsTimer > 0.0)
							this->m_fAvoidTheGeometryProbsTimer = 0.0;
						this->m_fAvoidTheGeometryProbsTimer -= CTimer::ms_fTimeStep;
				}
				else
				{
					if (this->m_fAvoidTheGeometryProbsTimer < 0.0)
						this->m_fAvoidTheGeometryProbsTimer = 0.0;
					this->m_fAvoidTheGeometryProbsTimer += CTimer::ms_fTimeStep;
				}

				if (!this->m_nAvoidTheGeometryProbsDirn)
				{
					CVector vecTargetToSomething = this->m_pTargetEntity->GetPosition() - *what;
					if (CrossProduct(vecTargetToSomething, v58).z <= 0.0)
						this->m_nAvoidTheGeometryProbsDirn = 1;
					else
						this->m_nAvoidTheGeometryProbsDirn = -1;
				}
			}
	}

	this->m_fAvoidTheGeometryProbsTimer *= pow(fAvoidProbTimerDamp, CTimer::ms_fTimeStep);
	WellBufferMe(a1a, &flt_B6EC38, &flt_B6EC3C, 0.2, 0.05f, 0);
	this->m_vecClearGeometryVec *= flt_B6EC38;
	this->m_bMoveCamToAvoidGeom = 1;
}

// module:
void CCamera::Process()
{
	ResetMadeInvisibleObjects();

	int v3 = 0;
	float FOVDuringInter = 0.0;
	CVector a3, v161, v163,v169;

	this->m_bJust_Switched = 0;

	this->m_vecRealPreviousCameraPosition = this->GetPosition();

	if (this->m_bLookingAtPlayer || this->m_bTargetJustBeenOnTrain || this->m_nWhoIsInControlOfTheCamera == 2)
		this->UpdateTargetEntity();

	if (!this->m_pTargetEntity)
	{
		this->m_pTargetEntity = (CEntity *)FindPlayerPed(-1);
		this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
	}

	if (!this->_GetActiveCam()->m_pCamTargetEntity)
	{
		this->_GetActiveCam()->m_pCamTargetEntity = this->m_pTargetEntity;
		this->_GetActiveCam()->m_pCamTargetEntity->RegisterReference(&this->_GetActiveCam()->m_pCamTargetEntity);
	}

	if (!this->_GetNextCam()->m_pCamTargetEntity)
	{
		this->_GetNextCam()->m_pCamTargetEntity = this->m_pTargetEntity;
		this->_GetNextCam()->m_pCamTargetEntity->RegisterReference(&this->_GetNextCam()->m_pCamTargetEntity);
	}

	this->CamControl();
	TheCamera.ProcessVectorMoveLinear();
	TheCamera.ProcessVectorTrackLinear();
	TheCamera.ProcessFOVLerp();

	if (this->m_bFading)
		this->ProcessFade();

	if (this->m_bMusicFading)
		this->ProcessMusicFade();

	if (this->m_bWideScreenOn)
		this->ProcessWideScreenOn();

	RwCameraSetNearClipPlane(Scene.m_pRwCamera, 0.3f);

	float fAnglePreProcess = 0.0f;
	if (this->_GetActiveCam()->m_vecFront.x != 0.0
		|| this->_GetActiveCam()->m_vecFront.y != 0.0)
		fAnglePreProcess = CGeneral::GetATanOfXY(this->_GetActiveCam()->m_vecFront.x, this->_GetActiveCam()->m_vecFront.y);

	this->_GetActiveCam()->Process();

	float fAnglePostProcess = 0.0f;
	if (this->_GetActiveCam()->m_vecFront.x != 0.0
		|| this->_GetActiveCam()->m_vecFront.y != 0.0)
		fAnglePostProcess = CGeneral::GetATanOfXY(this->_GetActiveCam()->m_vecFront.x, this->_GetActiveCam()->m_vecFront.y);

	if (this->m_bTransitionState
		&& CTimer::m_snTimeInMilliseconds > this->m_nTimeTransitionStart + this->m_nTransitionDuration)
	{
		this->m_bTransitionState = 0;
		this->m_bDoingSpecialInterPolation = 0;
		this->m_bWaitForInterpolToFinish = 0;
	}

	if (this->m_bUseNearClipScript)
		RwCameraSetNearClipPlane(Scene.m_pRwCamera, this->m_fNearClipScript);

	float fDeltaAngle = fAnglePostProcess - fAnglePreProcess;
	MakeAngleLessThan180(fDeltaAngle);

	if (fabs(fDeltaAngle) > 0.3f)
		this->m_bJust_Switched = 1;

	if (this->_GetActiveCam()->m_nDirectionWasLooking != CAMERA_DIR_LOOK_FORWARD
		&& this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE)
		v3 = 1;

	float fCurrentShakeTime = CTimer::m_snTimeInMilliseconds;
	if (fCurrentShakeTime <= this->m_fEndShakeTime)
	{
		float fShakeIntensity = Unlerp(this->m_fStartShakeTime, fCurrentShakeTime, this->m_fEndShakeTime);
		this->ProcessShake(fShakeIntensity);
	}

	if (this->m_bTransitionState && !v3)
	{
		float v19 = ClampMax(CTimer::m_snTimeInMilliseconds - this->m_nTimeTransitionStart, this->m_nTransitionDuration);

		float size = v19 / this->m_nTransitionDuration;
		float v21 = Clamp(0.0f, v19 / this->m_nTransitionDurationTargetCoors, 1.0f);

		CVector vecTargetCoorsForFudgeInter;//TargetDuringInter,v166,v167

		if (v21 > this->m_fFractionInterToStopMovingTarget)
		{
			if (v21 > this->m_fFractionInterToStopMovingTarget)
			{
				float v29;
				if (this->m_fFractionInterToStopCatchUpTarget == 0.0)
					v29 = 1.0;
				else
					v29 = (v21 - this->m_fFractionInterToStopMovingTarget) / this->m_fFractionInterToStopCatchUpTarget;

				float fInterpolProgress  = 0.5 - cos(v29 * rwPI) * 0.5; // TrueBeta

				if (this->m_fFractionInterToStopMovingTarget == 0.0)
				{
					this->m_vecTargetWhenInterPol = this->m_vecStartingTargetForInterPol;
				}

				CVector vecInterpol = this->_GetActiveCam()->m_vecTargetCoorsForFudgeInter - this->m_vecTargetWhenInterPol;

				vecTargetCoorsForFudgeInter = this->m_vecTargetWhenInterPol + vecInterpol * fInterpolProgress;
			}
		}
		else
		{
			float v22;
			if (this->m_fFractionInterToStopMovingTarget == 0.0)
				v22 = 0.0;
			else
				v22 = (this->m_fFractionInterToStopMovingTarget - v21) / this->m_fFractionInterToStopMovingTarget;
			
			float fInterpolProgress = 0.5 - cos(v22 * rwPI) * 0.5;
			
			vecTargetCoorsForFudgeInter = this->m_vecStartingTargetForInterPol + fInterpolProgress * this->m_vecTargetSpeedAtStartInter;
		}
		if (size > (double)this->m_fFractionInterToStopMoving)
		{
			if (size > (double)this->m_fFractionInterToStopMoving && size <= 1.0)
			{
				float v63;
				if (this->m_fFractionInterToStopCatchUp == 0.0)
					v63 = 1.0;
				else
					v63 = (size - this->m_fFractionInterToStopMoving) / this->m_fFractionInterToStopCatchUp;

				float fProgress = 0.5 - cos(v63 * rwPI) * 0.5;

				a3 = this->m_vecSourceWhenInterPol + (this->_GetActiveCam()->m_vecSource - this->m_vecSourceWhenInterPol) * fProgress;

				if (this->m_bLookingAtPlayer)
				{
					CVector vecRay = a3 - vecTargetCoorsForFudgeInter;

					if (vecRay.Magnitude2D() < flt_8CCF20)
					{
						float fAngle = CGeneral::GetATanOfXY(vecRay.x, vecRay.y);
						a3.x = cos(fAngle) * flt_8CCF20 + vecTargetCoorsForFudgeInter.x;
						a3.y = sin(fAngle) * flt_8CCF20 + vecTargetCoorsForFudgeInter.y;
					}
				}
				FOVDuringInter = (this->_GetActiveCam()->m_fFOV - this->m_fFOVWhenInterPol) * size + this->m_fFOVWhenInterPol;
				v161 = this->m_vecUpWhenInterPol + (this->_GetActiveCam()->m_vecUp - this->m_vecUpWhenInterPol) * size;
				v163 = vecTargetCoorsForFudgeInter - a3;
				//x = TargetDuringInter - a3.x;
				//v163.x = x;
				//y = v166 - a3.y;
				//v163.y = y;
				//z = v167 - a3.z;
				//v163.z = z;
				this->StoreValuesDuringInterPol(
					&a3,
					&vecTargetCoorsForFudgeInter,
					&v161,
					&FOVDuringInter);

				v163.Normalise();

				if (this->m_bLookingAtPlayer == 1)
				{
					/*x = 0.0;
					y = 0.0;
					z = 1.0;*/
					v161 = CVector(0.0,0,1);
				}

				if (this->_GetActiveCam()->m_nMode == MODE_TOPDOWN
					|| this->_GetActiveCam()->m_nMode == MODE_TOP_DOWN_PED)
				{
					v163.Normalise();
					/*x = -1.0;
					y = 0.0;
					z = 0.0;*/
					v169 = CVector(-1.0,0,0);
					v161 = CrossProduct(v163, v169);
				}
				else
				{
					v163.Normalise();
					v161.Normalise();
					v169 = CrossProduct(v163, v161);
					v169.Normalise();
					v161 = CrossProduct(v169, v163);
				}
				v161.Normalise();
				FOVDuringInter = this->m_fFOVWhenInterPol;
			}
		}
		else
		{
			float v36;
			if (this->m_fFractionInterToStopMoving == 0.0)
				v36 = 0.0;
			else
				v36 = (this->m_fFractionInterToStopMoving - size) / this->m_fFractionInterToStopMoving;

			float v39 = 0.5 - cos(v36 * rwPI) * 0.5;

			this->m_vecSourceWhenInterPol = this->m_vecStartingSourceForInterPol + v39 * this->m_vecSourceSpeedAtStartInter;

			if (this->m_bLookingAtPlayer)
			{
				CVector vecDelta = this->m_vecSourceWhenInterPol - vecTargetCoorsForFudgeInter;
				if (vecDelta.Magnitude2D() < flt_8CCF20)
				{
					float v44 = CGeneral::GetATanOfXY(vecDelta.x, vecDelta.y);
					this->m_vecSourceWhenInterPol.x = cos(v44) * flt_8CCF20 + vecTargetCoorsForFudgeInter.x;
					this->m_vecSourceWhenInterPol.y = sin(v44) * flt_8CCF20 + vecTargetCoorsForFudgeInter.y;
				}
			}

			this->m_vecUpWhenInterPol = this->m_vecStartingUpForInterPol + size * this->m_vecUpSpeedAtStartInter;
			a3 = this->m_vecSourceWhenInterPol;
			
			this->m_fFOVWhenInterPol = size * this->m_fFOVSpeedAtStartInter + this->m_fStartingFOVForInterPol;

			v163 = vecTargetCoorsForFudgeInter - a3;

			this->StoreValuesDuringInterPol(
				&a3,
				&this->m_vecTargetWhenInterPol,
				&this->m_vecUpWhenInterPol,
				&this->m_fFOVWhenInterPol);

			v163.Normalise();
			if (this->m_bLookingAtPlayer == 1)
			{
				//x = 0.0;
				//y = 0.0;
				//z = 1.0;
				v161 = CVector(0, 0, 1);
			}
			else
			{
				v161 = this->m_vecUpWhenInterPol;
			}
			v161.Normalise();
			if (this->_GetActiveCam()->m_nMode == MODE_TOPDOWN
				|| this->_GetActiveCam()->m_nMode == MODE_TOP_DOWN_PED)
			{
				v163.Normalise();
				v169 = CVector(-1.0f, 0.0f, 0.0f);
				v161 = CrossProduct(v163, v169);
			}
			else
			{
				v163.Normalise();
				v161.Normalise();
				v169 = CrossProduct(v163, v161);
				v169.Normalise();
				v161 = CrossProduct(v169, v163);
			}
			v161.Normalise();
			FOVDuringInter = this->m_fFOVWhenInterPol;
		}

		CVector vecDelta = a3 - vecTargetCoorsForFudgeInter;

		float fAlpha = CGeneral::GetATanOfXY(vecDelta.Magnitude2D(), vecDelta.z);
		float fBeta = CGeneral::GetATanOfXY(vecDelta.x, vecDelta.y);

		this->_GetActiveCam()->KeepTrackOfTheSpeed(
			a3,
			vecTargetCoorsForFudgeInter,
			v161,
			fAlpha,
			fBeta,
			FOVDuringInter);
	}
	else
	{

		a3 = this->_GetActiveCam()->m_vecSource;
		v161 = this->_GetActiveCam()->m_vecUp;

		if (this->m_bMoveCamToAvoidGeom)
		{
			a3 += this->m_vecClearGeometryVec;
			v163 = this->_GetActiveCam()->m_vecTargetCoorsForFudgeInter - a3;
			v163.Normalise();
			CVector x = CrossProduct(v163, v161);
			x.Normalise();
			v161 = CrossProduct(x, v163);
			v161.Normalise();
		}
		else
		{
			v163 = this->_GetActiveCam()->m_vecFront;
			v161 = this->_GetActiveCam()->m_vecUp;
		}

		byte_B70143 = 0;
		FOVDuringInter = this->_GetActiveCam()->m_fFOV;
	}

	if (this->m_bTransitionState
		&& !this->m_bLookingAtVector
		&& this->m_bLookingAtPlayer == 1
		&& !CCullZones::CamStairsForPlayer()
		&& !this->m_bPlayerIsInGarage)
	{
		CEntity * pEntity = 0;
		CColPoint colPoint;
		if (CWorld::ProcessLineOfSight(
			this->m_pTargetEntity->GetPosition(),
			a3,
			colPoint,
			pEntity,
			1,
			0,
			0,
			1,
			0,
			1,
			1,
			0))
		{
			a3 = colPoint.m_vecPoint;
			RwCameraSetNearClipPlane(Scene.m_pRwCamera, 0.05f);
		}
	}
#if 0
	if (CMBlur::Drunkness > 0.0)
	{
		v91 = CMBlur::Drunkness * -0.02;

		v92 = DEG_TO_RAD(flt_B6EC30);
		v93 = cos(v92);
		a3.x += v93 * v91;
		v94 = sin(v92);
		a3.z += v94 * v91;
		v161.Normalise();

		v95 = CMBlur::Drunkness * 0.05f;
		v161.x += v93 * v95;
		v161.y += v94 * v95;

		v161.Normalise();
		v163.Normalise();

		v96 = CMBlur::Drunkness * -0.1;
		v163.x += v93 * v96;
		v163.y += v94 * v96;
		v163.Normalise();

		v169 = CrossProduct(v163, v161);
		v169.Normalise();

		v161 = CrossProduct(v169, v163);
		v161.Normalise();

		flt_B6EC30 += 5.0f;
	}
#endif
	this->m_mCameraMatrix.right = CrossProduct(v161, v163);
	this->m_mCameraMatrix.at = v161;
	this->m_mCameraMatrix.up = v163;
	this->m_mCameraMatrix.pos = a3;

	float fShakeProgress = CTimer::m_snTimeInMilliseconds - this->m_nCamShakeStart;

	float fShake = Clamp(0.f, this->m_fCamShakeForce - fShakeProgress * 0.00028f, 2.f);
	unsigned int v115 = rand();
	float fPosShakeOffset = fShake * 0.1f;
	this->m_mCameraMatrix.pos.x += ((v115 & 0xF) - 7) * fPosShakeOffset;
	this->m_mCameraMatrix.pos.y += (((v115 >> 4) & 0xF) - 7) * fPosShakeOffset;
	this->m_mCameraMatrix.pos.z += (((v115 >> 8) & 0xF) - 7) * fPosShakeOffset;

	if (fPosShakeOffset > 0.0 && this->m_nBlurType != 1)
	{
		this->m_nMotionBlurAddAlpha = ClampMax(25 - (unsigned int)(fShake * -255.0), 150u);
	}

	if (this->_GetActiveCam()->m_nMode == MODE_1STPERSON
		&& FindPlayerVehicle(-1, 0)
		&& FindPlayerVehicle(-1, 0)->GetMatrix()->at.z < 0.2)
	{
		this->m_nBlurRed = 255;
		this->m_nBlurGreen = 255;
		this->m_nBlurBlue = 255;
		this->m_nBlurType = 1;
		this->m_nMotionBlur = 240;
		byte_B70142 = 1;
	}
	else if (byte_B70142)
	{
		byte_B70142 = 0;
	}
	CDraw::SetFOV(FOVDuringInter);
	this->CalculateDerivedValues(0, 1);
	this->CopyCameraMatrixToRWCam(0);

	this->m_vecGameCamPos = this->m_mCameraMatrix.pos;

	this->UpdateSoundDistances();

	if (!CCutsceneMgr::ms_running || CCutsceneMgr::ms_useLodMultiplier)
		this->m_fLODDistMultiplier = 70.0 / CDraw::ms_fFOV;
	else
		this->m_fLODDistMultiplier = 1.0;

	this->m_fGenerationDistMultiplier = this->m_fLODDistMultiplier;
	this->m_fLODDistMultiplier *= CRenderer::ms_lodDistScale;

	float fFarPlane = Scene.m_pRwCamera->farPlane * 100.0;
	float fFarClip = fFarPlane * 0.01f;

	RwCameraSetFarClipPlane(Scene.m_pRwCamera, fFarClip);

	CDraw::ms_fNearClipZ = this->m_pRwCamera->nearPlane;
	CDraw::ms_fFarClipZ = this->m_pRwCamera->farPlane;

	if (this->m_bJustInitalised == 1 || this->m_bJust_Switched == 1)
	{
		this->m_vecPreviousCameraPosition = this->m_mCameraMatrix.pos;
		this->m_bJustInitalised = 0;
	}

	CVector vecDeltaCamPos = this->m_mCameraMatrix.pos - this->m_vecPreviousCameraPosition;

	this->m_nNumFramesSoFar++;
	this->m_fCameraSpeedSoFar += vecDeltaCamPos.Magnitude();
	if (this->m_nNumFramesSoFar == this->m_nWorkOutSpeedThisNumFrames)
	{
		this->m_fCameraSpeedSoFar = 0.0;
		this->m_nNumFramesSoFar = 0;
		this->m_fCameraAverageSpeed = this->m_fCameraSpeedSoFar / this->m_nWorkOutSpeedThisNumFrames;
	}

	float fNewOrientation = this->m_fOrientation + rwPI;

	this->m_vecPreviousCameraPosition = this->m_mCameraMatrix.pos;

	if (this->_GetActiveCam()->m_nDirectionWasLooking != CAMERA_DIR_LOOK_FORWARD
		&& this->_GetActiveCam()->m_nMode != MODE_TOP_DOWN_PED)
	{
		this->_GetActiveCam()->m_vecSource = this->_GetActiveCam()->m_vecSourceBeforeLookBehind;
		this->m_fOrientation = fNewOrientation;
	}

	if (this->m_bTransitionState
		&& this->_GetNextCam()->m_pCamTargetEntity
		&& this->m_pTargetEntity
		&& this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_PED
		&& this->_GetNextCam()->m_pCamTargetEntity->_GetEntityType() != ENTITY_TYPE_VEHICLE
		&& this->_GetActiveCam()->m_nMode != MODE_TOP_DOWN_PED
		&& this->_GetNextCam()->m_nDirectionWasLooking != CAMERA_DIR_LOOK_FORWARD)
	{
		this->_GetNextCam()->m_vecSource = this->_GetActiveCam()->m_vecSourceBeforeLookBehind;
		this->m_fOrientation = fNewOrientation;
	}

	this->m_bCameraJustRestored = 0;
	this->m_bMoveCamToAvoidGeom = 0;

	CVector vecSampleWaterPos = this->GetPosition() + this->GetTopDirection() * 0.4f;//x,y,z
	float fWaterLevel;

	if (CWaterLevel::GetWaterLevel(vecSampleWaterPos.x, vecSampleWaterPos.y, vecSampleWaterPos.z, &fWaterLevel, true, false)
		&& vecSampleWaterPos.z - 0.6f <= fWaterLevel)
	{
		CWeather::WaterDepth = ClampMin(fWaterLevel - vecSampleWaterPos.z, 0.f);

		if (vecSampleWaterPos.z + 0.6f >= fWaterLevel)
			CWeather::UnderWaterness = 1.0 - (vecSampleWaterPos.z - (fWaterLevel - 0.6f)) * 0.83333331;
		else
			CWeather::UnderWaterness = 1.0;
	}
	else
	{
		CWeather::UnderWaterness = 0.0;
	}
}


void CCamera::UpdateTargetEntity()
{
	CPhysical * pPhysical = (CPhysical*)this->m_pTargetEntity;
	
	this->m_bPlayerWasOnBike = 0;
	bool v25 = false;

	if (this->m_pTargetEntity
		&& this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE
		&& pPhysical->m_vecMoveSpeed.MagnitudeSquared() > 0.3f)
	{
		this->m_bPlayerWasOnBike = 1;
	}

	bool v4 = false;

	if (this->m_nWhoIsInControlOfTheCamera == 2)
	{
		v4 = 1;
		if (this->m_nModeObbeCamIsInForCar == 8 || this->m_nModeObbeCamIsInForCar == 7)
		{
			if (FindPlayerPed(-1)->m_nPedState != PEDSTATE_ARRESTED)
				v4 = 0;
			if (!FindPlayerVehicle(-1, 0))
			{
				if (this->m_pTargetEntity)
					this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);

				this->m_pTargetEntity = (CEntity *)FindPlayerPed(-1);
				this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
			}
		}
	}
	if ((this->m_bLookingAtPlayer == 1 || v4) 
		&& !this->m_bTransitionState 
		|| !this->m_pTargetEntity
		|| this->m_bTargetJustBeenOnTrain == 1)
	{
		if (FindPlayerVehicle(-1, 0)
			&& (CGameLogic::IsCoopGameGoingOn()
				|| (FindPlayerPed(-1)->m_pIntelligence->m_TaskMgr.GetSimplestActiveTask() == nullptr)
				|| (FindPlayerPed(-1)->m_pIntelligence->m_TaskMgr.GetSimplestActiveTask()->GetId() != 1022)))
		{
			if (this->m_pTargetEntity)
				this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);

			this->m_pTargetEntity = (CEntity *)FindPlayerVehicle(-1, 0);
			this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
		}
		else
		{
			if (this->m_pTargetEntity)
				this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);

			this->m_pTargetEntity = (CEntity *)FindPlayerPed(-1);
			this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);

			if (FindPlayerPed(-1)->m_nPedState != PEDSTATE_ENTER_CAR
				&& FindPlayerPed(-1)->m_nPedState != PEDSTATE_CARJACK
				&& FindPlayerPed(-1)->m_nPedState != PEDSTATE_OPEN_DOOR)
			{
				if (this->m_pTargetEntity != this->_GetActiveCam()->m_pCamTargetEntity)
				{
					if (this->_GetActiveCam()->m_pCamTargetEntity)
						this->_GetActiveCam()->m_pCamTargetEntity->CleanUpOldReference(&this->_GetActiveCam()->m_pCamTargetEntity);

					this->_GetActiveCam()->m_pCamTargetEntity = this->m_pTargetEntity;
					this->_GetActiveCam()->m_pCamTargetEntity->RegisterReference(&this->_GetActiveCam()->m_pCamTargetEntity);
				}
			}
			else
			{
				v25 = 1;
			}
		}
		CPed * v17 = CWorld::Players[CWorld::PlayerInFocus].m_pPed;
		bool v18 = 1;
		if (v17)
		{
			if (v17->m_pVehicle)
			{
				if (FindPlayerPed(-1)->m_pVehicle->CanPedOpenLocks(FindPlayerPed(-1)))
					v18 = 0;
			}
		}

		if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_ENTER_CAR
			&& !v18 
			&& !v25 && this->m_nCarZoom)
		{
			if (this->m_pTargetEntity)
				this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);

			this->m_pTargetEntity = (CEntity *)CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_pVehicle;
			
			if (!this->m_pTargetEntity)
				this->m_pTargetEntity = (CEntity *)FindPlayerPed(-1);

			this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
		}
		ePedState pedState = CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState;
		if ((pedState == PEDSTATE_CARJACK || pedState == PEDSTATE_OPEN_DOOR)
			&& !v18)
		{
			if (!v25 && this->m_nCarZoom)
			{
				if (this->m_pTargetEntity)
					this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);

				this->m_pTargetEntity = (CEntity *)CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_pVehicle;
				this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
			}
			if (!CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_pVehicle)
			{
				if (this->m_pTargetEntity)
					this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);
				
				this->m_pTargetEntity = (CEntity *)FindPlayerPed(-1);
				this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
			}
		}
		if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_EXIT_CAR)
		{
			if (this->m_pTargetEntity)
				this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);
			
			this->m_pTargetEntity = (CEntity *)FindPlayerPed(-1);
			this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
		}
		if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->m_nPedState == PEDSTATE_DRAGGED_FROM_CAR)
		{
			if (this->m_pTargetEntity)
				this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);

			this->m_pTargetEntity = (CEntity *)FindPlayerPed(-1);
			this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
		}
		if (this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE
			&& !this->m_nCarZoom 
			&& FindPlayerPed(-1)->m_nPedState == PEDSTATE_ARRESTED)
		{
			if (this->m_pTargetEntity)
				this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);

			this->m_pTargetEntity = (CEntity *)FindPlayerPed(-1);
			this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
		}
	}
}

void CCamera::AddShakeSimple(float fDuration, int nType, float fIntensity)
{
	this->m_fShakeIntensity = fIntensity;
	this->m_nShakeType = nType;
	float fCurrentTime = CTimer::m_snTimeInMilliseconds;
	this->m_fStartShakeTime = fCurrentTime;
	this->m_fEndShakeTime = fCurrentTime + fDuration;
}

void CCamera::AllowShootingWith2PlayersInCar(bool bAllowShootingWith2PlayersInCar)
{
	this->m_bAllowShootingWith2PlayersInCar = bAllowShootingWith2PlayersInCar;
}

void CCamera::InitCameraVehicleTweaks()
{
	this->m_fCurrentTweakDistance = 1.0;
	this->m_fCurrentTweakAltitude = 1.0;
	this->m_fCurrentTweakAngle = 0;
	this->m_nCurrentTweakModelIndex = -1;

	if (this->m_bCameraVehicleTweaksInitialized == false)
	{
		this->m_aCamTweak[0].m_nModelIndex = -1;
		this->m_aCamTweak[0].m_fDistance = 1.0;
		this->m_aCamTweak[0].m_fAltitude = 1.0;
		this->m_aCamTweak[0].m_fAngle = 0.0;
		this->m_aCamTweak[1].m_nModelIndex = -1;
		this->m_aCamTweak[1].m_fDistance = 1.0;
		this->m_aCamTweak[1].m_fAltitude = 1.0;
		this->m_aCamTweak[1].m_fAngle = 0.0;
		this->m_aCamTweak[2].m_nModelIndex = -1;
		this->m_aCamTweak[2].m_fDistance = 1.0;
		this->m_aCamTweak[2].m_fAltitude = 1.0;
		this->m_aCamTweak[2].m_fAngle = 0.0;
		this->m_aCamTweak[3].m_nModelIndex = -1;
		this->m_aCamTweak[3].m_fDistance = 1.0;
		this->m_aCamTweak[3].m_fAltitude = 1.0;
		this->m_aCamTweak[3].m_fAngle = 0.0;
		this->m_aCamTweak[4].m_nModelIndex = -1;
		this->m_aCamTweak[4].m_fDistance = 1.0;
		this->m_aCamTweak[4].m_fAltitude = 1.0;
		this->m_aCamTweak[4].m_fAngle = 0.0;
		this->m_aCamTweak[0].m_nModelIndex = 501;
		this->m_aCamTweak[0].m_fDistance = 1.0;
		this->m_aCamTweak[0].m_fAltitude = 1.0;
		this->m_aCamTweak[0].m_fAngle = 0.178997;
		this->m_bCameraVehicleTweaksInitialized = 1;
	}
}

void CCamera::ApplyVehicleCameraTweaks(CVehicle * pVehicle)
{
	if (pVehicle->m_nModelIndex == this->m_nCurrentTweakModelIndex)
	{
		return;
	}

	this->InitCameraVehicleTweaks();
	for (int i = 0; i < ARRAYSIZE(this->m_aCamTweak); ++i)
	{
		if (this->m_aCamTweak[i].m_nModelIndex >= 0
			&& this->m_aCamTweak[i].m_nModelIndex == pVehicle->m_nModelIndex)
		{
			this->m_fCurrentTweakDistance = this->m_aCamTweak[i].m_fDistance;
			this->m_nCurrentTweakModelIndex = this->m_aCamTweak[i].m_nModelIndex;
			this->m_fCurrentTweakAltitude = this->m_aCamTweak[i].m_fAltitude;
			this->m_fCurrentTweakAngle = this->m_aCamTweak[i].m_fAngle;
			break;
		}
	}
}

void CCamera::CalculateDerivedValues(bool a2, bool a3)
{
	CMatrix matInverse(plugin::dummy);

	Invert(this->m_mCameraMatrix, matInverse);

	this->m_mMatInverse = matInverse;
	
	this->CalculateFrustumPlanes(a2);

	if (this->m_mCameraMatrix.up.x != 0.0 || this->m_mCameraMatrix.up.y != 0.0)
	{
		if (a3)
			this->m_fOrientation = atan2(this->m_mCameraMatrix.up.x, this->m_mCameraMatrix.up.y);
	}
	else
	{
		this->m_mCameraMatrix.up.x = 0.0001f;
	}
	this->m_fCamFrontXNorm = this->m_mCameraMatrix.up.x;
	this->m_fCamFrontYNorm = this->m_mCameraMatrix.up.y;
	float v7 = this->m_mCameraMatrix.up.Magnitude2D();
	if (v7 == 0.0f)
	{
		this->m_fCamFrontXNorm = 1.0;
	}
	else
	{
		this->m_fCamFrontXNorm /= v7;
		this->m_fCamFrontYNorm /= v7;
	}
}

void CCamera::CalculateFrustumPlanes(bool a2)
{
	float v2 = DEG_TO_RAD(CDraw::ms_fFOV / 2.f);

	this->m_avecFrustumNormals[0] = CVector(cos(v2), -sin(v2), 0.0f);
	this->m_avecFrustumNormals[1] = CVector(-cos(v2), -sin(v2), 0.0f);
	
	float v12 = RsGlobal.maximumHeight / RsGlobal.maximumWidth;
	float v15 = v12 * cos(v2);
	float v16 = -(v12 * sin(v2));

	this->m_avecFrustumNormals[2] = CVector(0, v16, -v15);
	this->m_avecFrustumNormals[3] = CVector(0, v16, v15);

	if (a2)
	{
		//TransformVectors(this->m_avecFrustumWorldNormals_Mirror->ToRwV3d(), 4, this->m_mCameraMatrix, &this->m_avecFrustumNormals->ToRwV3d());

		this->m_fFrustumPlaneOffsets_Mirror[0] = DotProduct(this->m_avecFrustumWorldNormals_Mirror[0], this->GetPosition());
		this->m_fFrustumPlaneOffsets_Mirror[1] = DotProduct(this->m_avecFrustumWorldNormals_Mirror[1], this->GetPosition());
		this->m_fFrustumPlaneOffsets_Mirror[2] = DotProduct(this->m_avecFrustumWorldNormals_Mirror[2], this->GetPosition());
		this->m_fFrustumPlaneOffsets_Mirror[3] = DotProduct(this->m_avecFrustumWorldNormals_Mirror[3], this->GetPosition());
	}
	else
	{
		//TransformVectors(this->m_avecFrustumWorldNormals, 4, &this->m_mCameraMatrix, &this->m_avecFrustumNormals->ToRwV3d());
		this->m_fFrustumPlaneOffsets[0] = DotProduct(this->m_avecFrustumWorldNormals[0], this->GetPosition());
		this->m_fFrustumPlaneOffsets[1] = DotProduct(this->m_avecFrustumWorldNormals[1], this->GetPosition());
		this->m_fFrustumPlaneOffsets[2] = DotProduct(this->m_avecFrustumWorldNormals[2], this->GetPosition());
		this->m_fFrustumPlaneOffsets[3] = DotProduct(this->m_avecFrustumWorldNormals[3], this->GetPosition());
	}
}

float CCamera::CalculateGroundHeight(unsigned int nFlags) 
{
	CVector currentCamPos = this->GetPosition();
	static CVector vecLastHeightCheckPos;
	static float fEntityTop;
	static float fEntityCenter;
	static float fEntityBottom;

	if (fabs(vecLastHeightCheckPos.x - currentCamPos.x) > 20.0
		|| fabs(vecLastHeightCheckPos.y - currentCamPos.y) > 20.0
		|| fabs(vecLastHeightCheckPos.z - currentCamPos.z) > 20.0)
	{
		currentCamPos.z = 1000.f;

		CEntity * pHitEntity;
		CColPoint hitPoint;

		if (CWorld::ProcessVerticalLine(currentCamPos, -1000.f, hitPoint, pHitEntity, 
			1,
			0,
			0,
			0,
			1,
			0,
			0))
		{
			fEntityCenter = hitPoint.m_vecPoint.z;

			CColModel * pColModel = CModelInfo::ms_modelInfoPtrs[pHitEntity->m_nModelIndex]->m_pColModel;		
			fEntityTop = pColModel->m_boundBox.m_vecMax.z + pHitEntity->GetPosition().z;

			CBox const & box = pColModel->m_boundBox;

			float fBoxWidth = box.m_vecMax.x - box.m_vecMin.x;
			float fBoxHeight = box.m_vecMax.y - box.m_vecMin.y;

			if (fBoxWidth > 120.0 || fBoxHeight > 120.0)
			{
				fEntityBottom = fEntityCenter;
			}
			else
			{
				fEntityBottom = pHitEntity->GetPosition().z + pColModel->m_boundBox.m_vecMin.z;
			}
			fEntityBottom = ClampMin(fEntityBottom, 0.f);
		}

		vecLastHeightCheckPos = this->GetPosition();
	}

	switch (nFlags)
	{
	case EHeightEntityTop:
		return fEntityTop;
	case EHeightEntityCenter:
		return fEntityCenter;
	case EHeightEntityBottom:
	default:
		return fEntityBottom;
	}
}

void CCamera::CalculateMirroredMatrix(CVector vecNormal, float fPosOffset, CMatrix * pOriginalMatrix, CMatrix * pMirrorMatrix)
{
	// info: https://wp.optics.arizona.edu/optomech/wp-content/uploads/sites/53/2016/08/6-Mirror-matrices.pdf
	float t1 = 2 * (DotProduct(pOriginalMatrix->pos, vecNormal) - fPosOffset);
	pMirrorMatrix->pos = pOriginalMatrix->pos - vecNormal * t1;

	float t2 = 2 * DotProduct(pOriginalMatrix->up, vecNormal);
	pMirrorMatrix->up = pOriginalMatrix->up - vecNormal * t2;

	float t3 = 2 * DotProduct(pOriginalMatrix->at, vecNormal);
	pMirrorMatrix->at = pOriginalMatrix->at - vecNormal * t3;

	pMirrorMatrix->right = CrossProduct(pMirrorMatrix->at, pMirrorMatrix->up);
}

void CCamera::DealWithMirrorBeforeConstructRenderList(bool bActivate, CVector vecNormal, float fPosOffset, CMatrix * pCachedMirrorMatrix)
{
	this->m_bMirrorActive = bActivate;


	if (bActivate)
	{
		if (pCachedMirrorMatrix)
			this->m_mMatMirror = *pCachedMirrorMatrix;
		else
			CCamera::CalculateMirroredMatrix(vecNormal, fPosOffset, &this->m_mCameraMatrix, &this->m_mMatMirror);

		Invert(this->m_mMatMirror, this->m_mMatMirrorInverse);
	}
}

void CCamera::CamShake(float fShakeMag, float x, float y, float z)
{
	CVector vecShakePos(x, y, z);
	CVector vecRayShakeToCam = this->_GetActiveCam()->m_vecSource - vecShakePos;

	float fMagXY = vecRayShakeToCam.Magnitude2D();
	float fMagTotal = sqrt(fMagXY * fMagXY + vecRayShakeToCam.z * vecRayShakeToCam.z);
	
	fMagTotal = Clamp(0.0f, fMagTotal, 100.0f);

	float fMagPercent = 1.0 - fMagTotal / 100.0f;
	float fShakeTimeProgress = CTimer::m_snTimeInMilliseconds - this->m_nCamShakeStart;
	float fCurrentShakeForce = (this->m_fCamShakeForce - fShakeTimeProgress / 1000.0f) * fMagPercent;

	fCurrentShakeForce = Clamp(0.0f, fCurrentShakeForce, 2.0f);
	
	float v13 = fMagPercent * fShakeMag * 0.35f;

	if (v13 > fCurrentShakeForce)
	{
		this->m_fCamShakeForce = v13;
		this->m_nCamShakeStart = CTimer::m_snTimeInMilliseconds;
	}
}

void CCamera::CameraPedAimModeSpecialCases(CPed * pPed)
{
	CCollision::bCamCollideWithVehicles = 1;
	CCollision::bCamCollideWithObjects = 1;
	CCollision::bCamCollideWithPeds = 1;

	if (pPed->bInVehicle
		&& pPed->m_pVehicle)
	{
		this->m_pExtraEntity[this->m_nExtraEntitiesCount++] = pPed->m_pVehicle;
	}
}

void CCamera::CameraPedModeSpecialCases()
{
	CCollision::bCamCollideWithVehicles = 1;
	CCollision::bCamCollideWithObjects = 1;
	CCollision::bCamCollideWithPeds = 1;
}

void CCamera::CameraVehicleModeSpecialCases(CVehicle * pVehicle)
{
	CCollision::bCamCollideWithObjects = 0;
	if (pVehicle->m_vecMoveSpeed.MagnitudeSquared() <= 0.4f)
	{
		CCollision::relVelCamCollisionVehiclesSqr = 0.01f;
		CCollision::bCamCollideWithVehicles = 1;
		CCollision::bCamCollideWithPeds = 1;
		CCollision::bCamCollideWithObjects = 1;
	}
	else
	{
		CCollision::relVelCamCollisionVehiclesSqr = 1.0;
		CCollision::bCamCollideWithVehicles = 1;
		CCollision::bCamCollideWithPeds = 0;
		CCollision::bCamCollideWithObjects = 0;
	}
	if (pVehicle->m_pTrailer)
		this->m_pExtraEntity[this->m_nExtraEntitiesCount++] = pVehicle->m_pTrailer;
}

void CCamera::ClearPlayerWeaponMode()
{
	this->m_PlayerWeaponMode.m_nMode = 0;
	this->m_PlayerWeaponMode.m_nMaxZoom = 1;
	this->m_PlayerWeaponMode.m_nMinZoom = -1;
	this->m_PlayerWeaponMode.m_fDuration = 0.0;
}

bool CCamera::ConsiderPedAsDucking(CPed * pPed)
{
	CTaskSimpleDuck * pTaskDuck = pPed->m_pIntelligence->GetTaskDuck(true);
	return pTaskDuck && pPed->bIsDucking && !pTaskDuck->m_bIsAborting;
}

void CCamera::CopyCameraMatrixToRWCam(bool bUpdateMatrix)
{
	RwFrame * pFrame = RwCameraGetFrame(this->m_pRwCamera);
	RwMatrix * pRwMatrix = RwFrameGetMatrix(pFrame);//v3
	
	if (!bUpdateMatrix)
		this->m_mCameraMatrixOld.UpdateMatrix(pRwMatrix);

	pRwMatrix->pos = this->m_mCameraMatrix.pos.ToRwV3d();
	pRwMatrix->right = this->m_mCameraMatrix.right.ToRwV3d();
	pRwMatrix->at = this->m_mCameraMatrix.up.ToRwV3d();
	pRwMatrix->up = this->m_mCameraMatrix.at.ToRwV3d();

	static CVector vecSavedPos(-99999.f, -99999.f, -99999.f);
	static CVector vecSavedAt(-99999.f, -99999.f, -99999.f);
	static CVector vecSavedUp(-99999.f, -99999.f, -99999.f);
	static CVector vecSavedRight(-99999.f, -99999.f, -99999.f);

#if 0
	v8 = frame->modelling.pos.y;
	v9 = vecSavedPos.z - frame->modelling.pos.z;
	if (!(v11 | v12))
	{
		pRwMatrix->pos = vecSavedPos.ToRwV3d();
	}
	v13 = frame->modelling.at.y;
	v14 = vecSavedAt.z - frame->modelling.at.z;
	if (v16 | v17)
	{
		pRwMatrix->at = vecSavedAt.ToRwV3d();
	}
	v18 = frame->modelling.up.y;
	v19 = vecSavedUp.z - frame->modelling.up.z;
	if (v21 | v22)
	{
		pRwMatrix->up = vecSavedUp.ToRwV3d();
	}
	v23 = frame->modelling.right.y;
	v24 = vecSavedRight.z - frame->modelling.right.z;
	if (v26 | v27)
	{
		pRwMatrix->right = vecSavedRight.ToRwV3d();
	}
#endif

	vecSavedPos.FromRwV3d(pRwMatrix->pos);
	vecSavedAt.FromRwV3d(pRwMatrix->at);
	vecSavedUp.FromRwV3d(pRwMatrix->up);
	vecSavedRight.FromRwV3d(pRwMatrix->right);

	RwMatrixUpdate(pRwMatrix);
	RwFrameUpdateObjects(pFrame);
	RwFrameOrthoNormalize(pFrame);

	if (this->m_bResetOldMatrix && !bUpdateMatrix)
	{
		this->m_mCameraMatrixOld.UpdateMatrix(pRwMatrix);
		this->m_bResetOldMatrix = false;
	}
}

void CCamera::ProcessVectorMoveLinear()
{
	float fCurrentTime = CTimer::m_snTimeInMilliseconds;

	if (fCurrentTime > this->m_fMoveLinearEndTime)
	{
		if (this->m_bCameraPersistPosition)
			this->m_bVecMoveLinearProcessed = 1;
	}
	else
	{
		float fProgress = Unlerp(this->m_fMoveLinearStartTime, fCurrentTime, this->m_fMoveLinearEndTime);
		this->ProcessVectorMoveLinear(fProgress);
	}
}

void CCamera::ProcessVectorMoveLinear(float fProgress)
{
	this->m_bVecMoveLinearProcessed = true;

	if (this->m_bMoveLinearWithEase)
	{
		fProgress = (sin(DEG_TO_RAD(270.0 - fProgress * 180.0)) + 1.0) * 0.5;
	}

	this->m_vecMoveLinear = 
		this->m_vecMoveLinearPosnStart + 
		(this->m_vecMoveLinearPosnEnd - this->m_vecMoveLinearPosnStart) * fProgress;
}

// module:
void CCamera::ProcessVectorTrackLinear()
{
	float fCurrentTime = CTimer::m_snTimeInMilliseconds;

	if (fCurrentTime > this->m_fTrackLinearEndTime)
	{
		if (this->m_bCameraPersistTrack)
			this->m_bVecTrackLinearProcessed = 1;
	}
	else
	{
		float fProgress = Unlerp(this->m_fTrackLinearStartTime, fCurrentTime, this->m_fTrackLinearEndTime);
		this->ProcessVectorTrackLinear(fProgress);
	}
}

void CCamera::ProcessVectorTrackLinear(float fProgress)
{
	this->m_bVecTrackLinearProcessed = 1;
	if (this->m_bTrackLinearWithEase)
	{
		fProgress = (sin(DEG_TO_RAD(270.0 - fProgress * 180.0)) + 1.0) * 0.5;
	}
	this->m_vecTrackLinear = (this->m_vecTrackLinearEndPoint - this->m_vecTrackLinearStartPoint) * fProgress
			+ this->m_vecTrackLinearStartPoint;
}

bool CCamera::ConeCastCollisionResolve(CVector *source, CVector *center,
	CVector *pVecOut, float radius, float arg5, float *pFloatOut)
{
	return true;
}

void CCamera::DeleteCutSceneCamDataMemory()
{
	for (int i = 0; i < ARRAYSIZE(this->m_aPathArray); ++i)
	{
		if (this->m_aPathArray[i].m_pArrPathData)
			delete this->m_aPathArray[i].m_pArrPathData;
	}
}

void CCamera::DrawBordersForWideScreen()
{
	CRect a2;
	a2.top = 1000000.0;
	a2.bottom = -1000000.0;
	this->GetScreenRect(&a2);

	if (this->m_nBlurType == 0 || this->m_nBlurType == 2)
		this->m_nMotionBlurAddAlpha = 80;

	RwRenderStateSet(rwRENDERSTATETEXTURERASTER, 0);

	CRect coords;
	coords.right = RsGlobal.maximumWidth + 5;
	coords.left = -5.0;
	coords.top = a2.bottom;
	coords.bottom = -5.0;
	CSprite2d::DrawRect(coords, CRGBA(0, 0, 0, 255));

	coords.top = RsGlobal.maximumHeight + 5;
	coords.bottom = a2.top;
	coords.right = RsGlobal.maximumWidth + 5;
	coords.left = -5.0;
	CSprite2d::DrawRect(coords, CRGBA(0, 0, 0, 255));
}

void CCamera::Enable1rstPersonCamCntrlsScript()
{
	this->m_bEnable1rstPersonCamCntrlsScript = 1;
}

void CCamera::Enable1rstPersonWeaponsCamera()
{
	this->m_bAllow1rstPersonWeaponsCamera = 1;
}
#if 0
void CCamera::Fade(float fFadeDuration, short FadeInOutFlag)
{
	this->m_fFadeDuration = fFadeDuration;
	this->m_bFading = 1;
	this->m_nFadeInOutFlag = FadeInOutFlag;
	this->m_nFadeStartTime = CTimer::m_snTimeInMilliseconds;
	if (!this->m_bIgnoreFadingStuffForMusic || FadeInOutFlag == 1)
	{
		this->m_bMusicFading = 1;
		v4 = fFadeDuration * 0.3f;
		this->m_nMusicFadingDirection = FadeInOutFlag;
		if (v4 <= 0.30000001)
			v5 = 0.30000001;
		else
			v5 = v4;

		if (v5 >= duration)
		{
			v4 = duration;
		}
		else if (v4 <= 0.30000001)
		{
			v4 = 0.30000001;
		}
		this->m_fTimeToFadeMusic = v4;
		if (inOut)
		{
			this->m_fTimeToWaitToFadeMusic = 0.0;
			this->m_nFadeTimeStartedMusic = CTimer::m_snTimeInMilliseconds;
		}
		else
		{
			this->m_fTimeToWaitToFadeMusic = duration - v4;
			v6 = v4 - 0.1;
			if (v6 <= 0.0)
				v6 = 0.0;
			this->m_fTimeToFadeMusic = v6;
			this->m_uiFadeTimeStartedMusic = CTimer::m_snTimeInMilliseconds;
		}
	}
}
#endif
void CCamera::Find3rdPersonCamTargetVector(float range, CVector source, CVector *pCamera, CVector *pPoint)
{

}
#if 0
float CCamera::Find3rdPersonQuickAimPitch()
{
	return -(atan2(
		tan(DEG_TO_RAD(this->FindCamFOV() / 2.0f))
		* (0.5 - CCamera::m_f3rdPersonCHairMultY + 0.5 - CCamera::m_f3rdPersonCHairMultY)
		* (1.0 / CDraw::ms_fAspectRatio),
		1.0)
		+ this->Cams[this->ActiveCam].Alpha);
}
#endif
float CCamera::FindCamFOV()
{
	return this->_GetActiveCam()->m_fFOV;
}

void CCamera::FinishCutscene()
{
	this->SetPercentAlongCutScene(100.0f);
	this->m_fPositionAlongSpline = 1.0f;
	this->m_bcutsceneFinished = true;
}

bool CCamera::GetArrPosForVehicleType(int type, int *arrPos)
{
	switch (type)
	{
	case 1:
		*arrPos = 0;
		break;
	case 2:
		*arrPos = 1;
		break;
	case 3:
		*arrPos = 2;
		break;
	case 4:
		*arrPos = 4;
		break;
	case 5:
		*arrPos = 3;
		break;
	default:
		return false;
	}
	return true;
}

unsigned int CCamera::GetCutSceneFinishTime()
{
	CCam * pActiveCam = this->_GetActiveCam();
	if (pActiveCam->m_nMode == MODE_FLYBY)
		return pActiveCam->m_nFinishTime;

	CCam * pNextCam = this->_GetNextCam();
	if (pNextCam->m_nMode == MODE_FLYBY)
		return pNextCam->m_nFinishTime;
	else
		return 0;
}

bool CCamera::GetFading()
{
	return this->m_bFading;
}

int CCamera::GetFadingDirection()
{
	if (this->m_bFading)
		return this->m_nFadeInOutFlag == 1;
	else
		return 2;
}

CVector * CCamera::GetGameCamPosition()
{
	return &this->m_vecGameCamPos;
}

signed int CCamera::GetLookDirection()
{
	CCam * pActiveCam = this->_GetActiveCam();
	eCamMode mode = pActiveCam->m_nMode;

	if (mode != MODE_CAM_ON_A_STRING
		&& mode != MODE_1STPERSON
		&& mode != MODE_BEHINDBOAT
		&& mode != MODE_FOLLOWPED
		|| pActiveCam->m_nDirectionWasLooking == CAMERA_DIR_LOOK_FORWARD)
		return CAMERA_DIR_LOOK_FORWARD;

	return pActiveCam->m_nDirectionWasLooking;
}

bool CCamera::GetLookingForwardFirstPerson()
{
	CCam * pActiveCam = this->_GetActiveCam();
	eCamMode mode = pActiveCam->m_nMode;

	return mode == MODE_1STPERSON
		&& pActiveCam->m_nDirectionWasLooking == CAMERA_DIR_LOOK_FORWARD;
}

bool CCamera::GetLookingLRBFirstPerson()
{
	CCam * pActiveCam = this->_GetActiveCam();
	eCamMode mode = pActiveCam->m_nMode;

	return mode == MODE_1STPERSON
		&& pActiveCam->m_nDirectionWasLooking != CAMERA_DIR_LOOK_FORWARD;
}

float CCamera::GetPositionAlongSpline()
{
	return this->m_fPositionAlongSpline;
}

float CCamera::GetRoughDistanceToGround()
{
	return this->_GetActiveCam()->m_vecSource.z - this->CalculateGroundHeight(EHeightEntityBottom);
}

signed int CCamera::GetScreenFadeStatus()
{
	if (this->m_fFadeAlpha == 0.0)
		return EScreenFadeBegin;
	else if (this->m_fFadeAlpha == 255.0f)
		return EScreenFadeEnd;
	else
		return EScreenFadeFading;
}

void CCamera::GetScreenRect(CRect *pRect)
{
	pRect->left = 0.0f;
	pRect->right = RsGlobal.maximumWidth;

	if (this->m_bWideScreenOn)
	{
		pRect->top = (RsGlobal.maximumHeight / 2) * this->m_fScreenReductionPercentage * 0.01f
			- RsGlobal.maximumHeight / 448.0f * 22.0;
		pRect->bottom = (double)RsGlobal.maximumHeight
			- (RsGlobal.maximumHeight / 2) * this->m_fScreenReductionPercentage * 0.01f
			- RsGlobal.maximumHeight / 448.0f * 14.0;
	}
	else
	{
		pRect->top = 0.0f;
		pRect->bottom = RsGlobal.maximumHeight;
	}
}

bool CCamera::Get_Just_Switched_Status()
{
	return this->m_bJust_Switched;
}

int CCamera::InitialiseScriptableComponents()
{
	this->m_fTrackLinearStartTime = -1.0;
	this->m_fTrackLinearEndTime = -1.0;
	this->m_fStartShakeTime = -1.0;
	this->m_fEndShakeTime = -1.0;
	this->m_fEndZoomTime = -1.0;
	this->m_fStartZoomTime = -1.0;
	this->m_fZoomInFactor = 0.0;
	this->m_fZoomOutFactor = 0.0;
	this->m_bTrackLinearWithEase = 1;
	this->m_nZoomMode = 1;
	this->m_bMoveLinearWithEase = 1;
	this->m_fMoveLinearStartTime = -1.0;
	this->m_fMoveLinearEndTime = -1.0;
	this->m_bBlockZoom = 0;
	this->m_bCameraPersistPosition = 0;
	this->m_bCameraPersistTrack = 0;
	this->m_bVecTrackLinearProcessed = 0;
	this->m_bVecMoveLinearProcessed = 0;
	this->m_bFOVLerpProcessed = 0;
	return 0;
}

bool CCamera::IsExtraEntityToIgnore(CEntity * pEntity)
{
	for (int i = 0; i < this->m_nExtraEntitiesCount; ++i)
	{
		if (this->m_pExtraEntity[i] == pEntity)
			return true;
	}
	return false;
}

bool CCamera::IsItTimeForNewcam(int CamSequence, int StartTime)
{
	return true;
}

bool CCamera::IsSphereVisible(CVector const *origin, float radius, RwMatrixTag *transformMatrix)
{
	return true;
}

bool CCamera::IsSphereVisible(CVector const *origin, float radius)
{
	return true;
}
#if 0
void CCamera::LerpFOV(float zoomInFactor, float zoomOutFactor, float fDuration, bool bEase)
{
	float fCurrentTime = CTimer::m_snTimeInMilliseconds;
	this->fZoomStartTime = fCurrentTime;
	this->fZoomInFactor = zoomInFactor;
	this->fZoomEndTime = fCurrentTime + fDuration;
	this->fZoomOutFactor = zoomOutFactor;
	this->bZoomSmooth = bEase;
}
#endif
void CCamera::LoadPathSplines(int pFile)
{

}

void CCamera::HandleCameraMotionForDuckingDuringAim(CPed * pPed, CVector * pVecSource, CVector * pVecTarget, bool arg5)
{
	CTaskSimpleDuck * pTaskDuck = pPed->m_pIntelligence->GetTaskDuck(true);
	float fOffset = 0.0f;

	if (pTaskDuck 
		&& pPed->bIsDucking
		&& false == pTaskDuck->m_bIsAborting)
		fOffset = -0.35f;
	
	if (!arg5)
		this->m_fDuckAimCamMotionFactor += (fOffset - this->m_fDuckAimCamMotionFactor) * (CTimer::ms_fTimeStep * 0.13);

	if (pVecSource)
		pVecSource->z += this->m_fDuckAimCamMotionFactor;

	if (pVecTarget)
		pVecTarget->z += this->m_fDuckAimCamMotionFactor;
}

void CCamera::Init()
{
	memset(&this->m_bAboveGroundTrainNodesLoaded, 0, 0xD60u);

	this->m_pRwCamera = 0;
	TheCamera.InitialiseScriptableComponents();
	this->m_bPlayerWasOnBike = 0;
	this->m_b1rstPersonRunCloseToAWall = 0;
	this->m_fPositionAlongSpline = 0.0;
	this->m_bCameraJustRestored = 0;
	this->m_aCams[0].Init();
	this->m_aCams[1].Init();
	this->m_aCams[2].Init();
	this->m_aCams[0].m_nMode = MODE_FOLLOWPED;
	this->m_aCams[1].m_nMode = MODE_FOLLOWPED;
	this->m_aCams[0].m_fTargetCloseInDist = 2.0837801 - 1.85;
	this->m_bEnable1rstPersonCamCntrlsScript = 0;
	this->m_bAllow1rstPersonWeaponsCamera = 0;
	this->m_bCooperativeCamMode = 0;
	this->m_bAllowShootingWith2PlayersInCar = 1;
	this->m_bVehicleSuspenHigh = 0;
	this->m_aCams[0].m_fMinRealGroundDist = 1.85;
	this->m_aCams[0].m_fTargetZoomGroundOne = -0.55f;
	this->m_aCams[0].m_fTargetZoomGroundTwo = 1.5;
	this->m_aCams[0].m_fTargetZoomGroundThree = 3.6f;
	this->m_aCams[0].m_fTargetZoomOneZExtra = 0.06f;
	this->m_aCams[0].m_fTargetZoomTwoZExtra = -0.1;
	this->m_aCams[0].m_fTargetZoomTwoInteriorZExtra = 0.0;
	this->m_aCams[0].m_fTargetZoomThreeZExtra = -0.07;
	this->m_aCams[0].m_fTargetZoomZCloseIn = 0.90040702;
	this->m_bMoveCamToAvoidGeom = 0;
	this->ClearPlayerWeaponMode();
	this->m_bInATunnelAndABigVehicle = 0;
	this->m_aCams[0].m_pCamTargetEntity = 0;
	this->m_aCams[1].m_pCamTargetEntity = 0;
	this->m_aCams[2].m_pCamTargetEntity = 0;
	this->m_aCams[0].m_fCamBufferedHeight = 0;
	this->m_aCams[0].m_fCamBufferedHeightSpeed = 0;
	this->m_aCams[1].m_fCamBufferedHeight = 0;
	this->m_aCams[1].m_fCamBufferedHeightSpeed = 0;
	this->m_aCams[0].m_bCamLookingAtVector = 0;
	this->m_aCams[1].m_bCamLookingAtVector = 0;
	this->m_aCams[2].m_bCamLookingAtVector = 0;
	this->m_aCams[0].m_fPlayerVelocity = 0.0;
	this->m_aCams[1].m_fPlayerVelocity = 0.0;
	this->m_aCams[2].m_fPlayerVelocity = 0.0;
	this->m_bHeadBob = 0;
	this->m_fFractionInterToStopMoving = 0.25;
	this->m_fFractionInterToStopCatchUp = 0.75;
	this->m_fGaitSwayBuffer = 0.85000002;
	this->m_bScriptParametersSetForInterPol = 0;
	this->m_nCamShakeStart = 0;
	this->m_fCamShakeForce = 0.0;
	this->m_nModeObbeCamIsInForCar = 30;
	this->m_bIgnoreFadingStuffForMusic = 0;
	this->m_bWaitForInterpolToFinish = 0;
	this->m_pToGarageWeAreIn = 0;
	this->m_pToGarageWeAreInForHackAvoidFirstPerson = 0;
	this->m_bPlayerIsInGarage = 0;
	this->m_bJustCameOutOfGarage = 0;
	this->m_fNearClipScript = 0.89999998;
	this->m_bUseNearClipScript = 0;
	this->m_bDoingSpecialInterPolation = 0;
	this->m_bAboveGroundTrainNodesLoaded = 0;
	this->m_bBelowGroundTrainNodesLoaded = 0;
	this->m_nModeForTwoPlayersSeparateCars = MODE_TWOPLAYER_SEPARATE_CARS;
	this->m_nModeForTwoPlayersSameCarShootingAllowed = MODE_TWOPLAYER_IN_CAR_AND_SHOOTING;
	this->m_nModeForTwoPlayersSameCarShootingNotAllowed = MODE_BEHINDCAR;
	this->m_nModeForTwoPlayersNotBothInCar = MODE_TWOPLAYER;
	this->m_bWideScreenOn = 0;
	this->m_fFOV_Wide_Screen = 0.0;
	this->m_bRestoreByJumpCut = 0;
	this->m_nCarZoom = 2;
	this->m_nPedZoom = 2;
	this->m_fCarZoomSmoothed = 0.0;
	this->m_fCarZoomTotal = 0.0;
	this->m_fCarZoomBase = 0.0;
	this->m_fPedZoomSmoothed = 0.0;
	this->m_fPedZoomTotal = 0;
	this->m_fPedZoomBase = 0.0;
	this->m_pTargetEntity = 0;

	if (FindPlayerVehicle(-1, 0))
		this->m_pTargetEntity = FindPlayerVehicle(-1, 0);
	else
		this->m_pTargetEntity = CWorld::Players[CWorld::PlayerInFocus].m_pPed;
	if (this->m_pTargetEntity)
		this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);

	this->m_bInitialNodeFound = 0;
	this->m_fScreenReductionPercentage = 0.0;
	this->m_fScreenReductionSpeed = 0.0;
	this->m_bWideScreenOn = 0;
	this->m_bWantsToSwitchWidescreenOff = 0;
	this->m_bWorldViewerBeingUsed = 0;
	this->m_fPlayerExhaustion = 1.0;
	this->m_fPedOrientForBehindOrInFront = 0.0;
	if (!FrontEndMenuManager.doGameReload)
	{
		this->m_bFading = 0;
		CDraw::FadeValue = 0;
		this->m_fFadeAlpha = 0.0;
		this->m_bMusicFading = 0;
		this->m_fTimeToFadeMusic = 0.0;
		this->m_fEffectsFaderScalingFactor = 0.0;
		CCamera::m_fMouseAccelVertical = 0.0015;
	}
	if (FrontEndMenuManager.doGameReload == 1)
		this->m_fTimeToFadeMusic = 0.0;
	this->m_bStartingSpline = 0;
	this->m_nTypeOfSwitch = 1;
	this->m_bUseScriptZoomValuePed = 0;
	this->m_bUseScriptZoomValueCar = 0;
	this->m_fPedZoomValueScript = 0.0;
	this->m_fCarZoomValueScript = 0.0;
	this->m_bUseSpecialFovTrain = 0;
	this->m_fFovForTrain = 70.0;
	this->m_nModeToGoTo = MODE_FOLLOWPED;
	this->m_bJust_Switched = 0;
	this->m_bUseTransitionBeta = 0;
	this->m_mCameraMatrix.SetScale(1.0f);
	this->m_bTargetJustBeenOnTrain = 0;
	this->m_bInitialNoNodeStaticsSet = 0;
	this->m_nLongestTimeInMill = 5000;
	this->m_nTimeLastChange = 0;
	this->m_bIdleOn = 0;
	this->m_nTimeWeLeftIdle_StillNoInput = 0;
	this->m_nTimeWeEnteredIdle = 0;
	this->m_fLODDistMultiplier = 1.0;
	this->m_bCamDirectlyBehind = 0;
	this->m_bCamDirectlyInFront = 0;
	this->m_nMotionBlur = 0;
	this->m_bGarageFixedCamPositionSet = 0;
	this->SetMotionBlur(255, 255, 255, 0, 0);
	this->m_bCullZoneChecksOn = 0;
	this->m_bFailedCullZoneTestPreviously = 0;
	this->m_nCheckCullZoneThisNumFrames = 6;
	this->m_nZoneCullFrameNumWereAt = 0;
	this->m_fCameraAverageSpeed = 0.0;
	this->m_fCameraSpeedSoFar = 0.0;
	this->m_vecPreviousCameraPosition = CVector(0.f, 0.f, 0.f);
	this->m_nWorkOutSpeedThisNumFrames = 4;
	this->m_nNumFramesSoFar = 0;
	this->m_bJustInitalised = 1;
	this->m_bTransitionState = 0;
	this->m_nTimeTransitionStart = 0;
	this->m_bLookingAtPlayer = 1;
	this->m_bLookingAtVector = 0;
	CCamera::m_f3rdPersonCHairMultX = 0.52999997;
	CCamera::m_f3rdPersonCHairMultY = 0.40000001;
	this->m_fAvoidTheGeometryProbsTimer = 0.0;
	this->m_nAvoidTheGeometryProbsDirn = 0;
	gPlayerPedVisible = 1;
	this->m_bResetOldMatrix = 1;
}

void CCamera::ProcessFOVLerp(float fProgress)
{
	this->m_bFOVLerpProcessed = true;

	if (this->m_nZoomMode)
		this->m_fFOVNew = LerpImprecise(this->m_fZoomInFactor, this->m_fZoomOutFactor,
		(sin(DEG_TO_RAD(270.0 - fProgress * 180.0)) + 1.0) * 0.5);
	else
		this->m_fFOVNew = LerpImprecise(this->m_fZoomInFactor, this->m_fZoomOutFactor, fProgress);
}

void CCamera::ProcessFOVLerp()
{
	float fCurrentTime = CTimer::m_snTimeInMilliseconds;

	if (fCurrentTime > this->m_fEndZoomTime)
	{
		if (this->m_bBlockZoom)
			this->m_bFOVLerpProcessed = 1;
	}
	else
	{
		float fProgress = Lerp(this->m_fStartZoomTime, this->m_fEndZoomTime, fCurrentTime);

		this->ProcessFOVLerp(fProgress);
	}
}

void CCamera::ProcessFade()
{
#if 0
	if (this->m_bFading == 1)
	{
		v1 = this->m_iFadingDirection;
		v2 = 0.0;
		if (v1 == 1)
		{
			if (this->m_fFadeDuration == 0.0)
				this->m_fFadeAlpha = 0.0;
			else
				this->m_fFadeAlpha = this->m_fFadeAlpha - CTimer::ms_fTimeStep * 0.02 / this->m_fFadeDuration * 255.0;
			if (this->m_fFadeAlpha > 0.0)
				goto LABEL_16;
			this->m_bFading = 0;
		}
		else
		{
			if (v1)
				goto LABEL_16;
			if (this->m_fFadeAlpha >= 255.0)
				this->m_bFading = 0;
			v2 = 255.0;
			if (this->m_fFadeDuration == 0.0)
				this->m_fFadeAlpha = 255.0;
			else
				this->m_fFadeAlpha = CTimer::ms_fTimeStep * 0.02 / this->m_fFadeDuration * 255.0 + this->m_fFadeAlpha;
			if (this->m_fFadeAlpha < 255.0)
				goto LABEL_16;
		}
		this->m_fFadeAlpha = v2;
	LABEL_16:
		CDraw::FadeValue = (unsigned __int64)this->m_fFadeAlpha;
	}
#endif
}

void CCamera::ProcessMusicFade()
{
#if 0
	v1 = this;
	result = this->m_bMusicFading;
	if (result)
	{
		if (this->m_fTimeToWaitToFadeMusic <= 0.0)
		{
			if (this->m_iMusicFadingDirection == 1)
			{
				if (this->m_fTimeToFadeMusic > 0.0)
					this->m_fEffectsFaderScalingFactor = CTimer::ms_fTimeStep * 0.02 / this->m_fTimeToFadeMusic
					+ this->m_fEffectsFaderScalingFactor;
				else
					this->m_fEffectsFaderScalingFactor = 1.0;
				if (this->m_fEffectsFaderScalingFactor >= 1.0)
				{
					this->m_bMusicFadedOut = 0;
					this->m_bMusicFading = 0;
					this->m_fEffectsFaderScalingFactor = 1.0;
				}
			}
			else if (!this->m_iFadingDirection)
			{
				if (this->m_fEffectsFaderScalingFactor <= 0.0)
				{
					this->m_bMusicFadedOut = 1;
					this->m_bMusicFading = 0;
					this->m_fEffectsFaderScalingFactor = 0.0;
				}
				if (this->m_fTimeToFadeMusic > 0.0)
					this->m_fEffectsFaderScalingFactor = this->m_fEffectsFaderScalingFactor
					- CTimer::ms_fTimeStep * 0.02 / this->m_fTimeToFadeMusic;
				else
					this->m_fEffectsFaderScalingFactor = 0.0;
				if (this->m_fEffectsFaderScalingFactor <= 0.0)
					this->m_fEffectsFaderScalingFactor = 0.0;
			}
		}
		else
		{
			this->m_fTimeToWaitToFadeMusic = this->m_fTimeToWaitToFadeMusic - CTimer::ms_fTimeStep * 0.02;
		}
		result = CAudioEngine::IsLoadingTuneActive(&AudioEngine);
		if (!result)
		{
			CAudioEngine::SetMusicFaderScalingFactor(&AudioEngine, v1->m_fEffectsFaderScalingFactor);
			result = CAudioEngine::SetEffectsFaderScalingFactor(&AudioEngine, v1->m_fEffectsFaderScalingFactor);
		}
	}
	return result;
#endif
}
#if 0
void CCamera::ProcessObbeCinemaCameraBoat()
{
	signed int v1; // edi
	CCamera *v2; // esi
	char *v3; // eax
	signed int v4; // edx
	signed int v5; // edx
	bool v6; // zf
	signed int v7; // eax
	int v8; // ecx

	v1 = 0;
	v2 = this;
	if (bDidWeProcessAnyCinemaCam)
		goto LABEL_28;
	gCurrentBoatCam = -1;
	if (gbCineyCamMessageDisplayed > 0 && !this->bCinemaCamera)
	{
		--gbCineyCamMessageDisplayed;
		v3 = CText::Get(&TheText, "CINCAM");
		CHud::SetHelpMessage(v3, 1, 0, 0);
	}
	byte_B6EC34 = 1;
	if (bDidWeProcessAnyCinemaCam)
	{
	LABEL_28:
		if (!CCamera::IsItTimeForNewcam(v2, SequenceOfBoatCams[gCurrentBoatCam], ObbeCinemaCameraBoatStartTime))
		{
		LABEL_23:
			v7 = gCurrentBoatCam;
			goto LABEL_24;
		}
	}
	gCurrentBoatCam = (gCurrentBoatCam + gCinematicModeSwitchDir) % 3;
	v4 = gCurrentBoatCam;
	if (gCurrentBoatCam >= 0)
	{
		if (gCurrentBoatCam <= 2)
			goto LABEL_12;
		v4 = 0;
	}
	else
	{
		v4 = 2;
	}
	gCurrentBoatCam = v4;
LABEL_12:
	if (CCamera::TryToStartNewCamMode(v2, SequenceOfBoatCams[v4]))
	{
	LABEL_25:
		v8 = CTimer::m_snTimeInMilliseconds;
		LODWORD(v2->m_iModeObbeCamIsInForCar) = gCurrentBoatCam;
		bDidWeProcessAnyCinemaCam = 1;
		ObbeCinemaCameraBoatStartTime = v8;
		return;
	}
	while (v1 <= 3)
	{
		gCurrentBoatCam = (gCurrentBoatCam + gCinematicModeSwitchDir) % 3;
		v5 = gCurrentBoatCam;
		if (gCurrentBoatCam < 0)
		{
			v5 = 2;
		LABEL_18:
			gCurrentBoatCam = v5;
			goto LABEL_19;
		}
		if (gCurrentBoatCam > 2)
		{
			v5 = 0;
			goto LABEL_18;
		}
	LABEL_19:
		++v1;
		if (CCamera::TryToStartNewCamMode(v2, SequenceOfBoatCams[v5]))
		{
			if (v1 < 3)
				goto LABEL_25;
			break;
		}
	}
	v6 = v2->Cams[v2->ActiveCam].mode == 18;
	v7 = 3;
	gCurrentBoatCam = 3;
	if (!v6)
	{
		CCamera::TryToStartNewCamMode(v2, 29);
		ObbeCinemaCameraBoatStartTime = CTimer::m_snTimeInMilliseconds;
		goto LABEL_23;
	}
LABEL_24:
	LODWORD(v2->m_iModeObbeCamIsInForCar) = v7;
	bDidWeProcessAnyCinemaCam = 1;
}
#endif

void CCamera::ProcessObbeCinemaCameraCar()
{
}

void CCamera::ProcessObbeCinemaCameraHeli()
{
}

unsigned int CCamera::ProcessObbeCinemaCameraPed()
{
	return true;
}

void CCamera::ProcessObbeCinemaCameraPlane()
{
}

void CCamera::ProcessObbeCinemaCameraTrain()
{
}

void CCamera::ProcessScriptedCommands()
{
	this->ProcessVectorMoveLinear();
	this->ProcessVectorTrackLinear();
	this->ProcessFOVLerp();
}

void CCamera::ProcessShake(float JiggleIntensity)
{
}

void CCamera::ProcessShake()
{
	float fCurrentTime = CTimer::m_snTimeInMilliseconds;

	if (fCurrentTime <= this->m_fEndShakeTime)
	{
		float fProgress = Lerp(this->m_fStartShakeTime, this->m_fEndShakeTime, fCurrentTime);
		this->ProcessShake(fProgress);
	}
}

void CCamera::ProcessWideScreenOn()
{
	if (this->m_bWantsToSwitchWidescreenOff)
	{
		this->m_bWantsToSwitchWidescreenOff = 0;
		this->m_bWideScreenOn = 0;
		this->m_fScreenReductionPercentage = 0.0;
		this->m_fFOV_Wide_Screen = 0.0;
		this->m_fWideScreenReductionAmount = 0.0;
	}
	else
	{
		this->m_fWideScreenReductionAmount = 1.0;
		this->m_fScreenReductionPercentage = 30.0;
		this->m_fFOV_Wide_Screen = this->FindCamFOV() * 0.3f;
	}
}

void CCamera::RenderMotionBlur()
{
#if 0
	// NOT IMPLEMENTED ON PC US 1.0
	if (this->m_BlurType)
		CMBlur::MotionBlurRender(
			this->m_pRwCamera,
			this->m_BlurRed,
			this->m_BlurGreen,
			this->m_BlurBlue,
			this->m_motionBlur,
			this->m_BlurType,
			this->m_imotionBlurAddAlpha);
#endif
}
#if 0
void CCamera::Restore()
{
	this->m_bLookingAtPlayer = 1;
	this->m_bLookingAtVector = 0;
	this->m_nTypeOfSwitch = 1;
	this->m_bUseNearClipScript = 0;
	this->m_nModeObbeCamIsInForCar = 30;
	this->m_fPositionAlongSpline = 0.0;
	this->m_bStartingSpline = 0;
	this->m_bScriptParametersSetForInterPol = 0;
	this->m_nWhoIsInControlOfTheCamera = 0;
	v2 = FindPlayerVehicle(-1, 0);
	v3 = v1->pTargetEntity;
	v4 = &v1->pTargetEntity;
	if (v2)
	{
		v1->m_iModeToGoTo = 18;
		if (v3)
			CEntity::CleanUpOldReference((CEntity *)&v1->pTargetEntity, v7);
		*v4 = FindPlayerVehicle(-1, 0);
	}
	else
	{
		v1->m_iModeToGoTo = 4;
		if (v3)
			CEntity::CleanUpOldReference((CEntity *)&v1->pTargetEntity, v7);
		*v4 = CWorld::Players[CWorld::PlayerInFocus].m_pPed;
	}
	CEntity::RegisterReference((CEntity *)&v1->pTargetEntity, v7);
	if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->__parent.pedState == 59)
		v1->m_iModeToGoTo = 18;
	v5 = CWorld::Players[CWorld::PlayerInFocus].m_pPed->__parent.pedState;
	if (v5 == 57 || v5 == 53)
		v1->m_iModeToGoTo = 18;
	if (CWorld::Players[CWorld::PlayerInFocus].m_pPed->__parent.pedState == 61)
	{
		v6 = *v4 == 0;
		v1->m_iModeToGoTo = 4;
		if (!v6)
			CEntity::CleanUpOldReference((CEntity *)&v1->pTargetEntity, v8);
		*v4 = CWorld::Players[CWorld::PlayerInFocus].m_pPed;
		CEntity::RegisterReference((CEntity *)&v1->pTargetEntity, v8);
	}
	if (v1->pAttachedEntity)
	{
		CEntity::CleanUpOldReference((CEntity *)&v1->pAttachedEntity, v8);
		v1->pAttachedEntity = 0;
	}
	v1->m_bEnable1rstPersonCamCntrlsScript = 0;
	v1->m_bAllow1rstPersonWeaponsCamera = 0;
	v1->m_bUseScriptZoomValuePed = 0;
	v1->m_bUseScriptZoomValueCar = 0;
	v1->m_fAvoidTheGeometryProbsTimer = 0.0;
	v1->m_bStartInterScript = 1;
	v1->m_bCameraJustRestored = 1;
}
#endif
void CCamera::RestoreCameraAfterMirror()
{
	this->SetMatrix(gmatSavedBeforeMirror);
	this->CopyCameraMatrixToRWCam(true);
	this->CalculateDerivedValues(false, false);
}

void CCamera::SetCamCutSceneOffSet(CVector const *cutsceneOffset)
{
	this->m_vecCutSceneOffset = *cutsceneOffset;
}

void CCamera::SetCamPositionForFixedMode(CVector const *fixedModeSource, CVector const *fixedModeUpOffset)
{
	this->m_vecFixedModeSource = *fixedModeSource;
	this->m_vecFixedModeUpOffSet = *fixedModeUpOffset;
	this->m_bGarageFixedCamPositionSet = 0;
}

void CCamera::SetCameraDirectlyBehindForFollowPed_CamOnAString()
{
	this->m_bCamDirectlyBehind = true;
	CPlayerPed * pPlayerPed = FindPlayerPed(-1);
	if (pPlayerPed)
		this->m_fPedOrientForBehindOrInFront = CGeneral::GetATanOfXY(
			pPlayerPed->_GetMatrixNoUpdate().up.x,
			pPlayerPed->_GetMatrixNoUpdate().up.y);

}

void CCamera::SetCameraDirectlyBehindForFollowPed_ForAPed_CamOnAString(CPed * pTarget)
{
	if (!pTarget)
	{
		return;
	}

	this->m_bCamDirectlyBehind = true;
	this->m_bLookingAtPlayer = false;
		
	TheCamera.m_pTargetEntity = (CEntity *)pTarget;
		
	if (TheCamera._GetActiveCam()->m_pCamTargetEntity)
		TheCamera._GetActiveCam()->m_pCamTargetEntity->CleanUpOldReference(&TheCamera._GetActiveCam()->m_pCamTargetEntity);

	TheCamera._GetActiveCam()->m_pCamTargetEntity = pTarget;

	TheCamera._GetActiveCam()->m_pCamTargetEntity->RegisterReference(&TheCamera._GetActiveCam()->m_pCamTargetEntity);

	this->m_fPedOrientForBehindOrInFront = CGeneral::GetATanOfXY(
		pTarget->_GetMatrixNoUpdate().up.x,
		pTarget->_GetMatrixNoUpdate().up.y);
}

void CCamera::SetCameraUpForMirror()
{
	gmatSavedBeforeMirror = this->m_mCameraMatrix;
	this->m_mCameraMatrix = this->m_mMatMirror;

	this->CopyCameraMatrixToRWCam(true);
	this->CalculateDerivedValues(true, false);
}

void CCamera::SetFadeColour(unsigned char Red, unsigned char Green, unsigned char Blue)
{
	this->m_bFadeTargetIsSplashScreen = 0;
	if (Red == 2 && Green == 2 && Blue == 2)
		this->m_bFadeTargetIsSplashScreen = 1;
	CDraw::FadeGreen = Green;
	CDraw::FadeRed = Red;
	CDraw::FadeBlue = Blue;
}

void CCamera::SetMotionBlur(int Red, int Green, int Blue, int value, int Blurtype)
{
	this->m_nBlurRed = Red;
	this->m_nBlurGreen = Green;
	this->m_nBlurBlue = Blue;
	this->m_nBlurType = Blurtype;
	this->m_nMotionBlur = value;
}

void CCamera::SetMotionBlurAlpha(int Alpha)
{
	this->m_nMotionBlurAddAlpha = Alpha;
}
#if 0
void CCamera::SetNearClipBasedOnPedCollision(float arg2)
{
	v2 = gpCamColVars[4];
	v3 = sqrt(arg2) / flt_B6EC68 * flt_8CCC84 * (0.30000001 - v2) + v2;
	arg2a = v3;
	if (v3 < v2)
		arg2a = gpCamColVars[4];
	RwCameraSetNearClipPlane(Scene.m_pRwCamera, arg2a);
}
#endif
void CCamera::SetNearClipScript(float NearClip)
{
	this->m_fNearClipScript = NearClip;
	this->m_bUseNearClipScript = 1;
}

void CCamera::SetNewPlayerWeaponMode(short mode, short maxZoom, short minZoom)
{
	this->m_PlayerWeaponMode.m_nMode = mode;
	this->m_PlayerWeaponMode.m_nMaxZoom = maxZoom;
	this->m_PlayerWeaponMode.m_nMinZoom = minZoom;
	this->m_PlayerWeaponMode.m_fDuration = 0.0;
}

void CCamera::SetParametersForScriptInterpolation(float InterpolationToStopMoving, 
	float InterpolationToCatchUp, unsigned int TimeForInterPolation)
{
	this->m_nScriptTimeForInterPolation = TimeForInterPolation;
	this->m_bScriptParametersSetForInterPol = 1;
	this->m_fScriptPercentageInterToStopMoving = InterpolationToStopMoving * 0.01f;
	this->m_fScriptPercentageInterToCatchUp = InterpolationToCatchUp * 0.01f;
}

void CCamera::SetPercentAlongCutScene(float percent)
{
	CCam * pActiveCam = this->_GetActiveCam();
	if (pActiveCam->m_nMode == 17)
	{
		pActiveCam->m_fTimeElapsedFloat = percent * 0.01f * pActiveCam->m_nFinishTime;
	}
	else
	{
		CCam * pNextCam = this->_GetNextCam();
		if (pNextCam->m_nMode == 17)
			pNextCam->m_fTimeElapsedFloat = percent * 0.01f * pNextCam->m_nFinishTime;
	}
}

void CCamera::SetRwCamera(RwCamera * pRwCamera)
{ 
	this->m_pRwCamera = pRwCamera;
	this->m_mViewMatrix.Attach(&pRwCamera->viewMatrix, false);
//	CMBlur::MotionBlurOpen(this->m_pRwCamera);
}

void CCamera::SetWideScreenOff()
{
	this->m_bWantsToSwitchWidescreenOff = this->m_bWideScreenOn == 1;
}

void CCamera::SetWideScreenOn()
{
	this->m_bWideScreenOn = 1;
	this->m_bWantsToSwitchWidescreenOff = 0;
}
#if 0
void CCamera::SetZoomValueCamStringScript(short zoomMode)
{
	v2 = this;
	v3 = (CVehicle *)this->Cams[0].CamTargetEntity;
	if ((v3->field_0.__parent.info & 7) == 2)
	{
		arrPos = 0;
		v4 = CVehicle::GetVehicleAppearance(v3);
		CCamera::GetArrPosForVehicleType(v4, &arrPos);
		if (zoomMode)
		{
			if (zoomMode == 1)
			{
				v2->m_fCarZoomValueScript = ZOOM_TWO_DISTANCE[arrPos];
				v2->m_bUseScriptZoomValueCar = 1;
			}
			else
			{
				if (zoomMode == 2)
					LODWORD(v2->m_fCarZoomValueScript) = ZOOM_THREE_DISTANCE[arrPos];
				v2->m_bUseScriptZoomValueCar = 1;
			}
		}
		else
		{
			v2->m_fCarZoomValueScript = ZOOM_ONE_DISTANCE[arrPos];
			v2->m_bUseScriptZoomValueCar = 1;
		}
	}
	else
	{
		CCamera::SetZoomValueFollowPedScript(v2, zoomMode);
	}
}
#endif
void CCamera::SetZoomValueFollowPedScript(short zoomMode)
{
	if (zoomMode)
	{
		if (zoomMode == 1)
		{
			this->m_fPedZoomValueScript = 1.5;
			this->m_bUseScriptZoomValuePed = 1;
		}
		else
		{
			if (zoomMode == 2)
				this->m_fPedZoomValueScript = 2.9000001;
			this->m_bUseScriptZoomValuePed = 1;
		}
	}
	else
	{
		this->m_fPedZoomValueScript = 0.25;
		this->m_bUseScriptZoomValuePed = 1;
	}
}

void CCamera::StartCooperativeCamMode()
{
	this->m_bCooperativeCamMode = 1;
	CGameLogic::n2PlayerPedInFocus = 2;
}

void CCamera::StartTransition(short currentCamMode){}

void CCamera::StartTransitionWhenNotFinishedInter(short currentCamMode)
{
	this->m_bDoingSpecialInterPolation = 1;
	this->StartTransition(currentCamMode);
}

void CCamera::StopCooperativeCamMode()
{
	this->m_bCooperativeCamMode = 0;
	CGameLogic::n2PlayerPedInFocus = 2;
}

void CCamera::StoreValuesDuringInterPol(CVector *SourceDuringInter, CVector *TargetDuringInter, CVector *UpDuringInter, float *FOVDuringInter)
{
	this->m_vecSourceDuringInter = *SourceDuringInter;
	this->m_vecTargetDuringInter = *TargetDuringInter;
	this->m_vecUpDuringInter = *UpDuringInter;
	this->m_fFOVDuringInter = *FOVDuringInter;

	CVector vecRay = this->m_vecSourceDuringInter - this->m_vecTargetDuringInter;
	this->m_fBetaDuringInterPol = CGeneral::GetATanOfXY(vecRay.x, vecRay.y);
	float fLen = vecRay.Magnitude2D();
	this->m_fAlphaDuringInterPol = CGeneral::GetATanOfXY(fLen, vecRay.z);
}

void CCamera::TakeControl(CEntity * pTarget, short ModeToGoTo, 
	short TypeOfSwitch, int WhoIsInControlOfTheCamera)
{
	bool result = 1;
	
	if (WhoIsInControlOfTheCamera == 2 && this->m_nWhoIsInControlOfTheCamera == 1)
		result = 0;

	if (this->m_bCinemaCamera || result)
	{
		this->m_nWhoIsInControlOfTheCamera = WhoIsInControlOfTheCamera;
		if (pTarget)
		{
			if (ModeToGoTo == MODE_NONE)
			{
				if (this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_PED)
				{
					ModeToGoTo = MODE_FOLLOWPED;
				}
				else if (this->m_pTargetEntity->_GetEntityType() == ENTITY_TYPE_VEHICLE)
				{
					ModeToGoTo = MODE_CAM_ON_A_STRING;
				}
			}
		}
		else if (FindPlayerVehicle(-1, 0))
		{
			pTarget = (CEntity *)FindPlayerVehicle(-1, 0);
		}
		else
		{
			pTarget = (CEntity*)CWorld::Players[CWorld::PlayerInFocus].m_pPed;
		}
		this->m_bLookingAtVector = 0;

		if (this->m_pTargetEntity)
			this->m_pTargetEntity->CleanUpOldReference(&this->m_pTargetEntity);

		this->m_pTargetEntity = pTarget;
		this->m_pTargetEntity->RegisterReference(&this->m_pTargetEntity);
		this->m_nModeToGoTo = ModeToGoTo;
		this->m_nTypeOfSwitch = TypeOfSwitch;
		this->m_bLookingAtPlayer = 0;
		this->m_bStartInterScript = 1;
	}
}
#if 0
void CCamera::TakeControlAttachToEntity(CEntity *target, CEntity *attached, 
	CVector *AttachedCamOffset, CVector *AttachedCamLookAt, 
	float AttachedCamAngle, short TypeOfSwitch, int WhoIsInControlOfTheCamera)
{
	result = (CPed *)a8;
	v10 = this;
	if (a8 != 2 || this->WhoIsInControlOfTheCamera != 1)
	{
		this->WhoIsInControlOfTheCamera = a8;
		if (!a3)
		{
			if (FindPlayerVehicle(-1, 0))
				a3 = (CEntity *)FindPlayerVehicle(-1, 0);
			else
				a3 = &CWorld::Players[CWorld::PlayerInFocus].m_pPed->__parent.__parent.__parent;
		}
		v13 = v8;
		if (a2)
		{
			if (v10->pTargetEntity)
				CEntity::CleanUpOldReference((CEntity *)&v10->pTargetEntity, v8);
			v10->pTargetEntity = a2;
			CEntity::RegisterReference((CEntity *)&v10->pTargetEntity, v13);
			v10->m_bLookingAtVector = 0;
		}
		else
		{
			v10->m_bLookingAtVector = 1;
			if ((unsigned __int8)VecNotEqual(&a5->x, &a4->x))
			{
				v10->m_vecAttachedCamLookAt = *a5;
			}
			else
			{
				v11 = &v10->m_vecAttachedCamLookAt;
				v11->x = 0.0;
				v11->y = 0.0;
				v11->z = 0.0;
			}
		}
		if (a4->x == 0.0 && a4->y == 0.0 && a4->z == 0.0)
		{
			v12 = &v10->m_vecAttachedCamOffset;
			v12->x = 0.0;
			v12->y = 0.0;
			v12->z = 2.0;
		}
		else
		{
			v10->m_vecAttachedCamOffset = *a4;
		}
		v10->m_fAttachedCamAngle = a6;
		if (v10->pAttachedEntity)
			CEntity::CleanUpOldReference((CEntity *)&v10->pAttachedEntity, v13);
		v10->pAttachedEntity = a3;
		CEntity::RegisterReference((CEntity *)&v10->pAttachedEntity, v13);
		v10->m_iModeToGoTo = 47;
		v10->m_iTypeOfSwitch = a7;
		v10->m_bLookingAtPlayer = 0;
		v10->m_bStartInterScript = 1;
	}
}
#endif
void CCamera::TakeControlNoEntity(CVector const *FixedModeVector, 
	short TypeOfSwitch, int WhoIsInControlOfTheCamera)
{
	if (WhoIsInControlOfTheCamera != 2 || this->m_nWhoIsInControlOfTheCamera != 1)
	{
		this->m_nWhoIsInControlOfTheCamera = WhoIsInControlOfTheCamera;
		this->m_bLookingAtVector = 1;
		this->m_nModeToGoTo = MODE_FIXED;
		this->m_bLookingAtPlayer = 0;
		this->m_vecFixedModeVector = *FixedModeVector;
		this->m_nTypeOfSwitch = TypeOfSwitch;
		this->m_bStartInterScript = 1;
	}
}

void CCamera::TakeControlWithSpline(short TypeOfSwitch)
{
	this->m_bLookingAtPlayer = 0;
	this->m_bLookingAtVector = 0;
	this->m_bcutsceneFinished = 0;
	this->m_nModeToGoTo = MODE_FLYBY;
	this->m_nTypeOfSwitch = TypeOfSwitch;
	this->m_bStartInterScript = 1;
}

bool CCamera::TryToStartNewCamMode(int CamSequence)
{
	return true;
}

void CCamera::UpdateAimingCoors(CVector const *AimingTargetCoors)
{
	this->m_vecAimingTargetCoors = *AimingTargetCoors;
}

void CCamera::UpdateSoundDistances()
{
#if 0
	v1 = a1;
	v2 = a1->Cams[a1->ActiveCam].mode;
	if (v2 != 16
		&& v2 != 7
		&& v2 != 39
		&& v2 != 40
		&& v2 != 52
		&& v2 != 42
		&& v2 != 43
		&& v2 != 41
		&& v2 != 45
		&& v2 != 46
		&& v2 != 34
		&& v2 != 8
		&& v2 != 51
		|| (a1->pTargetEntity->info & 7) != 3)
	{
		v3 = a1->m_cameraMatrix.rwMat.up.x * 5.0;
		v4 = a1->m_cameraMatrix.rwMat.up.y * 5.0;
		v5 = a1->m_cameraMatrix.rwMat.up.z * 5.0;
	}
	else
	{
		v3 = a1->m_cameraMatrix.rwMat.up.x * 0.5;
		v4 = a1->m_cameraMatrix.rwMat.up.y * 0.5;
		v5 = a1->m_cameraMatrix.rwMat.up.z * 0.5;
	}
	v6 = a1->__parent.m_pCoords;
	v16 = v5;
	if (v6)
		v7 = &v6->rwMat.pos.x;
	else
		v7 = &a1->__parent.placement.pos.x;
	v17 = v3 + *v7;
	v18 = v4 + v7[1];
	v8 = v16 + v7[2];
	a1a = v17;
	v19 = v8;
	v16 = v19;
	v15 = v18;
	v9 = *(_DWORD *)&CTimer::m_FrameCounter % 12u;
	if (!(*(_DWORD *)&CTimer::m_FrameCounter % 12u))
	{
		v10 = v16 + 20.0;
		a1->SoundDistUpAsReadOld = a1->SoundDistUpAsRead;
		a2 = v10;
		if (CWorld::ProcessVerticalLine((const CVector *)&a1a, a2, (CColPoint *)&a3, (CEntity **)&a4, 1, 0, 0, 0, 1, 0, 0))
			v1->SoundDistUpAsRead = a3.z - v16;
		else
			v1->SoundDistUpAsRead = 20.0;
	}

	v1->SoundDistUp = (1.0 - (v9 + 1) * 0.16666667) * v1->SoundDistUpAsReadOld
		+ (double)result * 0.16666667 * v1->SoundDistUpAsRead;
#endif
}

bool CCamera::Using1stPersonWeaponMode()
{
	unsigned short Mode = this->m_PlayerWeaponMode.m_nMode;
	return Mode == 7 
		|| Mode == 34 
		|| Mode == 8 
		|| Mode == 51 
		|| Mode == 45 
		|| Mode == 46 
		|| Mode == 65;
}

void CCamera::VectorMoveLinear(CVector *MoveLinearPosnEnd, CVector *MoveLinearPosnStart,
	float duration, bool bMoveLinearWithEase)
{
	float fCurrentTime = CTimer::m_snTimeInMilliseconds;

	this->m_fMoveLinearStartTime = fCurrentTime;
	this->m_fMoveLinearEndTime = fCurrentTime + duration;
	this->m_vecMoveLinearPosnStart = *MoveLinearPosnStart;
	this->m_vecMoveLinearPosnEnd = *MoveLinearPosnEnd;
	this->m_bMoveLinearWithEase = bMoveLinearWithEase;
}

void CCamera::VectorTrackLinear(CVector *TrackLinearStartPoint, 
	CVector *TrackLinearEndPoint, float duration, bool bEase)
{
	float fCurrentTime = CTimer::m_snTimeInMilliseconds;

	this->m_fTrackLinearStartTime = fCurrentTime;
	this->m_fTrackLinearEndTime = fCurrentTime + duration;
	this->m_vecTrackLinearStartPoint = *TrackLinearEndPoint;
	this->m_vecTrackLinearEndPoint = *TrackLinearStartPoint;
	this->m_bTrackLinearWithEase = bEase;
}

void CCamera::DontProcessObbeCinemaCamera()
{
	bDidWeProcessAnyCinemaCam = 0;
}

void CCamera::SetCamCollisionVarDataSet(int index)
{

}

void CCamera::SetColVarsAimWeapon(int aimingType)
{
	switch (aimingType)
	{
	case 0:
		CCamera::SetCamCollisionVarDataSet(0);
		break;
	case 1:
		CCamera::SetCamCollisionVarDataSet(1);
		break;
	case 2:
		CCamera::SetCamCollisionVarDataSet(2);
		break;
	case 3:
		CCamera::SetCamCollisionVarDataSet(3);
		break;
	default:
		return;
	}
}

void CCamera::SetColVarsVehicle(int vehicletype, int CamVehicleZoom)
{
	switch (vehicletype)
	{
	case 0:
	case 4:
		CCamera::SetCamCollisionVarDataSet(CamVehicleZoom + 9);
		break;
	case 1:
		CCamera::SetCamCollisionVarDataSet(CamVehicleZoom + 12);
		break;
	case 2:
		CCamera::SetCamCollisionVarDataSet(CamVehicleZoom + 15);
		break;
	case 3:
		CCamera::SetCamCollisionVarDataSet(CamVehicleZoom + 18);
		break;
	case 5:
		CCamera::SetCamCollisionVarDataSet(CamVehicleZoom + 21);
		break;
	case 6:
		CCamera::SetCamCollisionVarDataSet(CamVehicleZoom + 24);
		break;
	default:
		return;
	}
}