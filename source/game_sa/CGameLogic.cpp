#include "StdInc.h"

int &CGameLogic::n2PlayerPedInFocus = *reinterpret_cast<int *>(0x8A5E50);

bool CGameLogic::IsCoopGameGoingOn()
{
	return plugin::CallAndReturn<bool, 0x00441390>();
}